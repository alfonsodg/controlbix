# -*- coding: utf-8 -*-
{
'!langcode!': 'my-mm',
'!langname!': 'မြန်မာ',
'"update" is an optional expression like "field1=\'newvalue\'". You cannot update or delete the results of a JOIN': '"update" is an optional expression like "field1=\'newvalue\'". You cannot update or delete the results of a JOIN',
'%s %%{row} deleted': '%s %%{row} ဖျက်ပြီးပြီ',
'%s %%{row} updated': '%s %%{row} ပြင်ပြီးပြီ',
'%s selected': '%s ခု ရွေးထားသည်',
'%Y-%m-%d': '%Y-%m-%d',
'%Y-%m-%d %H:%M:%S': '%Y-%m-%d %H:%M:%S',
'(**%.0d MB**)': '(**%.0d MB**)',
'(requires internet access, experimental)': '(requires internet access, experimental)',
'(something like "it-it")': '(something like "it-it")',
'**%(items)s** %%{item(items)}, **%(bytes)s** %%{byte(bytes)}': '**%(items)s** %%{item(items)}, **%(bytes)s** %%{byte(bytes)}',
'**%(items)s** items, **%(bytes)s** %%{byte(bytes)}': '**%(items)s** items, **%(bytes)s** %%{byte(bytes)}',
'**not available** (requires the Python [[guppy http://pypi.python.org/pypi/guppy/ popup]] library)': '**not available** (requires the Python [[guppy http://pypi.python.org/pypi/guppy/ popup]] library)',
'---- Elegir Opción ----': '---- Elegir Opción ----',
'?': '?',
'@markmin\x01An error occured, please [[reload %s]] the page': 'An error occured, please [[reload %s]] the page',
'``**not available**``:red (requires the Python [[guppy http://pypi.python.org/pypi/guppy/ popup]] library)': '``**not available**``:red (requires the Python [[guppy http://pypi.python.org/pypi/guppy/ popup]] library)',
'About': 'အကြောင်း',
'Access Control': 'အသုံးပြု ခြင်းဆိုင်ရာ ထိန်းချုပ်ရန်',
'Actividad': 'Actividad',
'Actividades': 'Actividades',
'Additional code for your application': 'Additional code for your application',
'Admin language': 'Admin language',
'Administrar': 'Administrar',
'administrative interface': 'administrative interface',
'Administrative Interface': 'စီမံခန့်ခွဲရာ အင်တာဖေ့စ်',
'Administrator Password:': 'Administrator Password:',
'Ajax Recipes': 'Ajax Recipes',
'Alcances del Servicio': 'Alcances del Servicio',
'Almacen': 'Almacen',
'Alquileres': 'Alquileres',
'An error occured, please [[reload %s]] the page': 'An error occured, please [[reload %s]] the page',
'and rename it:': 'and rename it:',
'appadmin is disabled because insecure channel': 'စိတ်မချရသော လမ်းကြောင်းမှ ဝင်ရောက်သဖြင့် appadmin ကို အသုံးပြု၍ မရပါ',
'Application name:': 'Application name:',
'Aprobaciones': 'Aprobaciones',
'Aprobaciones Fechas': 'Aprobaciones Fechas',
'are not used': 'အသုံးမပြုပါ',
'are not used yet': 'အသုံးမပြုသေးပါ',
'Are you sure you want to delete this object?': 'သင် ဒီအရာ ဖျက်ရန် သေချာပါသလား။',
'Area Comercial': 'Area Comercial',
'Area de Negocio': 'Area de Negocio',
'Area de Trabajo': 'Area de Trabajo',
'Asesoría Jurídica': 'Asesoría Jurídica',
'Available Databases and Tables': 'အသုံးပြုနိုင်သော ဒေတာဘေစ့်များနှင့် ဇယားများ',
'Bitacora': 'Bitacora',
'Buy this book': 'ဒီစာအုပ်ကို ဝယ်ပါ',
'Cache': 'Cache',
'cache': 'cache',
'Cache Cleared': 'Cache Cleared',
'Cache contains items up to **%(hours)02d** %%{hour(hours)} **%(min)02d** %%{minute(min)} **%(sec)02d** %%{second(sec)} old.': 'Cache contains items up to **%(hours)02d** %%{hour(hours)} **%(min)02d** %%{minute(min)} **%(sec)02d** %%{second(sec)} old.',
'Cache Keys': 'Cache Keys',
'Calidad de Acabados': 'Calidad de Acabados',
'Calidad de Material': 'Calidad de Material',
'Calidade de Limpieza': 'Calidade de Limpieza',
'can be a git repo': 'can be a git repo',
'Cancelaciones': 'Cancelaciones',
'Cancelaciones Fechas': 'Cancelaciones Fechas',
'Cannot be empty': 'အလွတ် မဖြစ်ရပါ',
'Capacidad de Solucion y Respuesta': 'Capacidad de Solucion y Respuesta',
'Capital': 'Capital',
'Cartas de Presentación': 'Cartas de Presentación',
'Change admin password': 'Change admin password',
'Check to delete': 'ဖျက်ရန် စစ်ဆေးပါ',
'Checking for upgrades...': 'အဆင့်မြှင့်တင်မှုများအတွက် စစ်ဆေးနေသည် ...',
'Cheques por Cobrar': 'Cheques por Cobrar',
'Citas': 'Citas',
'Clean': 'ရှင်းလင်းရန်',
'Clear CACHE?': 'CACHE ကို ရှင်းလင်းမည်မှာ ဟုတ်ပါသလား။',
'Clear DISK': 'DISK ကို ရှင်းလင်းမည်။',
'Clear RAM': 'RAM ကို ရှင်းလင်းမည်။',
'Client IP': 'Client IP',
'Cliente': 'Cliente',
'Clientes': 'Clientes',
'Cobranzas a Tiempo': 'Cobranzas a Tiempo',
'Cobranzas Vencidas': 'Cobranzas Vencidas',
'collapse/expand all': 'collapse/expand all',
'Combustible': 'Combustible',
'Comercial': 'Comercial',
'Community': 'အသိုင်းအဝိုင်း',
'Competencia Tecnica Profesional': 'Competencia Tecnica Profesional',
'Compile': 'Compile',
'Components and Plugins': 'Components and Plugins',
'Configuracion': 'Configuracion',
'Contabilidad': 'Contabilidad',
'Contactos': 'Contactos',
'Contactos Clientes': 'Contactos Clientes',
'Control Calendario': 'Control Calendario',
'Controller': 'ကွန်ထရိုလာ',
'Controllers': 'ကွန်ထရိုလာများ',
'controllers': 'controllers',
'Copyright': 'မူပိုင်ခွင့်',
'Costo Proyecto': 'Costo Proyecto',
'Cotizacion': 'Cotizacion',
'Cotizaciones': 'Cotizaciones',
'Cotizaciones Aprobadas': 'Cotizaciones Aprobadas',
'Cotizaciones Enviadas': 'Cotizaciones Enviadas',
'Create': 'ဖန်တီးရန်',
'create file with filename:': 'create file with filename:',
'Create/Upload': 'Create/Upload',
'created by': 'ဖန်းတီးသူ',
'Created By': 'ပြုလုပ်ဖန်တီးသူ',
'Created On': 'ပြုလုပ်ဖန်တီးသည့်အချိန်',
'Creditos Leasing': 'Creditos Leasing',
'crontab': 'crontab',
'Créditos Leasing': 'Créditos Leasing',
'Cuentas Bancos': 'Cuentas Bancos',
'Cuentas Cobrar': 'Cuentas Cobrar',
'Cuentas Pagar': 'Cuentas Pagar',
'Cuentas por Cobrar': 'Cuentas por Cobrar',
'Cuentas por Pagar': 'Cuentas por Pagar',
'Cuentas por Pagar Vencidas': 'Cuentas por Pagar Vencidas',
'Current request': 'Current request',
'Current response': 'Current response',
'Current session': 'Current session',
'currently running': 'လက်ရှိတွင် လုပ်ဆောင်နေသည်',
'Data Grid': 'Data Grid',
'data uploaded': 'data uploaded',
'Database': 'ဒေတာဘေစ့်',
'Database %s select': 'Database %s select',
'database administration': 'ဒေတာဘေ့(စ်) စီမံခန့်ခွဲခြင်း',
'Database Administration (appadmin)': 'ဒေတာဘေစ့် စီမံခန့်ခွဲခြင်း (appadmin)',
'db': 'db',
'DB Model': 'DB Model',
'Debug': 'အမှားရှာရန်',
'Delete this file (you will be asked to confirm deletion)': 'Delete this file (you will be asked to confirm deletion)',
'Delete:': 'Delete:',
'Demo': 'အစမ်း၊ သရုပ်ပြမှုများ',
'Deploy': 'Deploy',
'Deploy on Google App Engine': 'Deploy on Google App Engine',
'Deploy to OpenShift': 'Deploy to OpenShift',
'Deployment Recipes': 'Deployment Recipes',
'Description': 'ဖော်ပြချက်',
'design': 'design',
'Direcciones': 'Direcciones',
'Direcciones Cliente': 'Direcciones Cliente',
'Dirección Fiscal GG': 'Dirección Fiscal GG',
'direction: ltr': 'direction: ltr',
'Disable': 'ပိတ်ရန်',
'DISK': 'DISK',
'Disk Cache Keys': 'Disk Cache Keys',
'Disk Cleared': 'Disk ရှင်းလင်းပြီးပြီ',
'DISK contains items up to **%(hours)02d** %%{hour(hours)} **%(min)02d** %%{minute(min)} **%(sec)02d** %%{second(sec)} old.': 'DISK contains items up to **%(hours)02d** %%{hour(hours)} **%(min)02d** %%{minute(min)} **%(sec)02d** %%{second(sec)} old.',
'Disposiciones de Desmonte': 'Disposiciones de Desmonte',
'Distrito': 'Distrito',
'Documentation': 'စာရွက်စာတမ်း အထောက်အကူများ',
"Don't know what to do?": 'ဘာလုပ်ရမည်မသိ ဖြစ်နေပါသလား။',
'done!': 'လုပ်ငန်း ဆောင်ရွက်ပြီးပြီ!',
'Download': 'Download',
'Download layouts from repository': 'Download layouts from repository',
'Download plugins from repository': 'Download plugins from repository',
'E-mail': 'အီးမေးလ်',
'Edit': 'ပြင်ဆင်ရန်',
'Edit application': 'Application ကို ပြင်ရန်',
'Edit current record': 'လက်ရှိ မှတ်တမ်းကို ပြင်ရန်',
'Email and SMS': 'အီးမေးလ်နှင့် SMS',
'Email Contacto': 'Email Contacto',
'Email Corporativo': 'Email Corporativo',
'Email personal': 'Email personal',
'Enable': 'ဖွင့်ရန်',
'Encuesta Satisfacción': 'Encuesta Satisfacción',
'enter an integer between %(min)g and %(max)g': 'enter an integer between %(min)g and %(max)g',
'Errors': 'အမှားများ',
'Estado Cartas Presentacion': 'Estado Cartas Presentacion',
'Estado Cheques': 'Estado Cheques',
'Estado Cliente': 'Estado Cliente',
'Estado Contratos': 'Estado Contratos',
'Estado Cuentas por Cobrar': 'Estado Cuentas por Cobrar',
'Estado Cuentas por Pagar': 'Estado Cuentas por Pagar',
'Estado Facturacion': 'Estado Facturacion',
'Estado Negociacion': 'Estado Negociacion',
'Estado Operacion': 'Estado Operacion',
'Estado Personal': 'Estado Personal',
'Estado Post venta': 'Estado Post venta',
'Estado Presupuesto': 'Estado Presupuesto',
'Estado Productos': 'Estado Productos',
'Estado Reporte': 'Estado Reporte',
'Estado Visita': 'Estado Visita',
'Estados': 'Estados',
'export as csv file': ' csv file အနေနဲ့ ထုတ်ပေးရန်',
'exposes': 'exposes',
'extends': 'extends',
'Facturacion': 'Facturacion',
'Facturación': 'Facturación',
'FAQ': 'ဖြစ်လေ့ရှိသော ပြဿနာများ',
'Fecha Actualizacion': 'Fecha Actualizacion',
'filter': 'filter',
'Finanzas': 'Finanzas',
'First name': 'အမည်၏ ပထမဆုံး စာလုံး',
'Forms and Validators': 'Forms and Validators',
'Free Applications': 'အခမဲ့ Applications',
'Gastos': 'Gastos',
'Gastos Generales': 'Gastos Generales',
'General': 'General',
'graph model': 'graph model',
'Graph Model': 'Graph Model',
'Group ID': 'Group ID',
'Groups': 'အဖွဲ့များ',
'Grupos': 'Grupos',
'Hello World': 'မင်္ဂလာပါ ကမ္ဘာကြီး။',
'Help': 'အကူအညီ',
'Hit Ratio: **%(ratio)s%%** (**%(hits)s** %%{hit(hits)} and **%(misses)s** %%{miss(misses)})': 'Hit Ratio: **%(ratio)s%%** (**%(hits)s** %%{hit(hits)} and **%(misses)s** %%{miss(misses)})',
'Hit Ratio: **%(ratio)s%%** (**%(hits)s** %%{hit(hits)} and **%(misses)s** %%{miss(misses})': 'Hit Ratio: **%(ratio)s%%** (**%(hits)s** %%{hit(hits)} and **%(misses)s** %%{miss(misses})',
'Home': 'မူလသို့',
'How did you get here?': 'သင် ဘယ်လို ရောက်လာခဲ့သလဲ။',
'import': 'သွင်းယူရန်',
'Import/Export': 'သွင်းယူရန်/ထုတ်ယူရန်',
'includes': 'includes',
'INDICE': 'INDICE',
'Install': 'Install',
'Installed applications': 'ထည့်သွင်းပြီး application များ',
'Intereses': 'Intereses',
'Internal State': 'Internal State',
'Introduction': 'မိတ်ဆက်',
'Invalid email': 'အီးမေးလ် ဖြည့်သွင်းမှုမှားနေသည်',
'Invalid Query': 'Invalid Query',
'invalid request': 'invalid request',
'Inventario de productos': 'Inventario de productos',
'Inventario de Productos': 'Inventario de Productos',
'Inversiones': 'Inversiones',
'Is Active': 'Is Active',
'Key': 'Key',
'Language': 'ဘာသာစကား',
'languages': 'ဘာသာစကားများ',
'Languages': 'ဘာသာစကားများ',
'Last name': 'မျိုးနွယ်အမည်',
'Layout': 'အပြင်အဆင်',
'Layout Plugins': 'Layout Plugins',
'Layouts': 'အပြင်အဆင်များ',
'Live Chat': 'တိုက်ရိုက် ဆက်သွယ် ပြောကြားရန်',
'Log In': 'Log In',
'Login': 'ဝင်ရောက်အသုံးပြုရန်',
'Login to the Administrative Interface': 'Login to the Administrative Interface',
'Logout': 'ထွက်ရန်',
'Lost Password': 'စကားဝှက် မသိတော့ပါ',
'Lost password?': 'စကားဝှက် မသိတော့ဘူးလား။',
'Manage': 'စီမံခန့်ခွဲရန်',
'Manage %(action)s': '%(action)s ကို စီမံရန်',
'Manage Access Control': 'အသုံးပြုခြင်းဆိုင်ရာ ထိန်းချုပ်မှု စီမံခန့်ခွဲရန်',
'Manage Cache': 'Manage Cache',
'Mantenimiento': 'Mantenimiento',
'Marca GG': 'Marca GG',
'Marcas': 'Marcas',
'Medio Captación': 'Medio Captación',
'Memberships': 'အသင်းဝင်များ',
'Membresías': 'Membresías',
'Menu Model': 'Menu Model',
'Models': 'Models',
'models': 'models',
'Modified By': 'ပြင်ဆင်မွမ်းမံသူ',
'Modified On': 'ပြင်ဆင်မွမ်းမံသည့် အချိန်',
'Modules': 'Modules',
'modules': 'modules',
'Moneda': 'Moneda',
'My Sites': 'ကျွန်ုပ်၏ Site များ',
'Name': 'အမည်',
'Negociaciones': 'Negociaciones',
'Negociación': 'Negociación',
'Negociación Avanzada': 'Negociación Avanzada',
'Negocios': 'Negocios',
'New application wizard': 'New application wizard',
'New Record': 'မှတ်တမ်း အသစ်',
'new record inserted': 'မှတ်တမ်း အသစ် ဖြည့်သွင်းပြီးပြီ',
'New simple application': 'ရိုးရိုး application အသစ်',
'next %s rows': 'နောက်အတန်း %s တန်း',
'Nivel de Comunicacion y Cortesia': 'Nivel de Comunicacion y Cortesia',
'Nivel de Satisfaccion': 'Nivel de Satisfaccion',
'No databases in this application': 'ဒီ application တွင် မည်သည့် ဒေတာဘေစ့်မှ မရှိပါ',
'no package selected': 'no package selected',
'Nota': 'Nota',
'Number of entries: **%s**': 'Number of entries: **%s**',
'Numero Ticket Gasto': 'Numero Ticket Gasto',
'Object or table name': 'Object or table name',
'Online examples': 'အွန်လိုင်း နမူနာများ',
'Operaciones': 'Operaciones',
'or alternatively': 'or alternatively',
'Or Get from URL:': 'Or Get from URL:',
'or import from csv file': 'or import from csv file',
'Origin': 'မူလ အစ',
'Other Plugins': 'အခြား Plugins',
'Other Recipes': 'အခြား Recipes',
'Otros Gastos': 'Otros Gastos',
'Overview': 'အပေါ်ယံရှုမြင်ခြင်း',
'Overwrite installed app': 'Overwrite installed app',
'Pack all': 'အားလုံးကို ထုပ်ပိုးရန်',
'Pack custom': 'ရွေးချယ်ထုပ်ပိုးရန်',
'Password': 'စကားဝှက်',
"Password fields don't match": 'စကားဝှက်များ ကိုက်ညီမှု မရှိပါ',
'Permission': 'ခွင့်ပြုချက်',
'Permissions': 'ခွင့်ပြုချက်များ',
'Personal': 'Personal',
'Personal Administrativo': 'Personal Administrativo',
'Personal Disponible': 'Personal Disponible',
'Personal Operaciones': 'Personal Operaciones',
'Plano': 'Plano',
'Plazos de Pago': 'Plazos de Pago',
'please input your password again': 'ကျေးဇူးပြု၍ စကားဝှက်ကို ထပ်မံ ဖြည့်သွင်းပေးပါ',
'plugins': 'plugins',
'Plugins': 'Plugins',
'Plural-Forms:': 'Plural-Forms:',
'Posicion / Rol': 'Posicion / Rol',
'Post Venta': 'Post Venta',
'Powered by': 'အားဖြည့်စွမ်းအားပေးသူ',
'Preface': 'နိဒါန်း',
'Prestamo Bancario': 'Prestamo Bancario',
'Prestamo Socios': 'Prestamo Socios',
'Presupuesto': 'Presupuesto',
'Presupuestos': 'Presupuestos',
'previous %s rows': 'previous %s rows',
'Prioridades': 'Prioridades',
'Private files': 'Private files',
'private files': 'private files',
'Procesos': 'Procesos',
'Proveedor Operaciones': 'Proveedor Operaciones',
'Proveedores': 'Proveedores',
'Provincia': 'Provincia',
'Proyectos': 'Proyectos',
'Préstamo Bancario': 'Préstamo Bancario',
'Préstamo Socios': 'Préstamo Socios',
'Publicidad': 'Publicidad',
'pygraphviz library not found': 'pygraphviz library ကို မတွေ့ပါ',
'Python': 'Python',
'Query:': 'Query:',
'Quick Examples': 'အမြန် အသုံးပြုနိုင်သော နမူနာများ',
'RAM': 'RAM',
'RAM Cache Keys': 'RAM Cache Keys',
'Ram Cleared': 'Ram ရှင်းလင်းပြီးပြီ',
'RAM contains items up to **%(hours)02d** %%{hour(hours)} **%(min)02d** %%{minute(min)} **%(sec)02d** %%{second(sec)} old.': 'RAM contains items up to **%(hours)02d** %%{hour(hours)} **%(min)02d** %%{minute(min)} **%(sec)02d** %%{second(sec)} old.',
'Razon Social GG': 'Razon Social GG',
'Razon social/ Nombre completo': 'Razon social/ Nombre completo',
'Razones Sociales': 'Razones Sociales',
'Recipes': 'Recipes',
'Record': 'မှတ်တမ်း',
'record does not exist': 'မှတ်တမ်း မရှိပါ',
'Record ID': 'Record ID',
'Record id': 'Record id',
'Redes Sociales': 'Redes Sociales',
'Register': 'မှတ်ပုံတင်ရန်',
'Registration identifier': 'Registration identifier',
'Registration key': 'Registration key',
'Reload routes': 'Reload routes',
'Remember me (for 30 days)': 'Remember me (for 30 days)',
'Remuneraciones': 'Remuneraciones',
'Reporte Diario': 'Reporte Diario',
'Reportes': 'Reportes',
'Reportes Finanzas': 'Reportes Finanzas',
'Reportes Periodos': 'Reportes Periodos',
'Reportes Procesos': 'Reportes Procesos',
'Reportes Tecnicos': 'Reportes Tecnicos',
'Requerimientos': 'Requerimientos',
'Request reset password': 'စကားဝှက် အသစ် တောင်းဆိုရန်',
'Reset Password key': 'Reset Password key',
'Resultados': 'Resultados',
'Role': 'Role',
'Roles': 'Roles',
'Rows in Table': 'Rows in Table',
'Rows selected': 'ရွေးထားသော အတန်းများ',
'Ruc / Dni/ Ce': 'Ruc / Dni/ Ce',
'RUC GG': 'RUC GG',
"Run tests in this file (to run all files, you may also use the button labelled 'test')": "Run tests in this file (to run all files, you may also use the button labelled 'test')",
'Running on %s': 'Running on %s',
'Saldos Cuentas Bancos': 'Saldos Cuentas Bancos',
'Save model as...': 'Save model as...',
'Seguros': 'Seguros',
'Semantic': 'Semantic',
'Services': 'Services',
'shell': 'shell',
'Sign Up': 'Sign Up',
'Site': 'Site',
'Size of cache:': 'Size of cache:',
'Start wizard': 'Start wizard',
'state': 'state',
'Static': 'Static',
'static': 'static',
'Statistics': 'ကိန်းဂဏန်း အချက်အလက်များ',
'Stylesheet': 'Stylesheet',
'Submit': 'Submit',
'submit': 'ပြုလုပ်ပါ',
'Successfuly! Here the link: ': 'Successfuly! Here the link: ',
'Support': 'အထောက်အပံ့',
'Suscripciones': 'Suscripciones',
'Table': 'ဇယား',
'Tarea Personal': 'Tarea Personal',
'Tareas': 'Tareas',
'Telefono Corporativo': 'Telefono Corporativo',
'Telefono personal': 'Telefono personal',
'Teléfonos': 'Teléfonos',
'Temporal': 'Temporal',
'Temporal2': 'Temporal2',
'test': 'test',
'The "query" is a condition like "db.table1.field1==\'value\'". Something like "db.table1.field1==db.table2.field2" results in a SQL JOIN.': 'The "query" is a condition like "db.table1.field1==\'value\'". Something like "db.table1.field1==db.table2.field2" results in a SQL JOIN.',
'The application logic, each URL path is mapped in one exposed function in the controller': 'The application logic, each URL path is mapped in one exposed function in the controller',
'The Core': 'The Core',
'The data representation, define database tables and sets': 'The data representation, define database tables and sets',
'The output of the file is a dictionary that was rendered by the view %s': 'The output of the file is a dictionary that was rendered by the view %s',
'The presentations layer, views are also known as templates': 'The presentations layer, views are also known as templates',
'The Views': 'The Views',
'There are no plugins': 'There are no plugins',
'There are no private files': 'There are no private files',
'These files are not served, they are only available from within your app': 'These files are not served, they are only available from within your app',
'These files are served without processing, your images go here': 'These files are served without processing, your images go here',
'This App': 'ဒီ App',
'This email already has an account': 'ဒီအီးမေးလ်တွင် အကောင့် ရှိပြီး ဖြစ်ပါသည်',
'Ticket de Atención': 'Ticket de Atención',
'Tiempo de Decisión': 'Tiempo de Decisión',
'Tiempo de Garantía': 'Tiempo de Garantía',
'Time in Cache (h:m:s)': 'Time in Cache (h:m:s)',
'Timestamp': 'Timestamp',
'Tipo Cuenta': 'Tipo Cuenta',
'Tipo de Centro Costo': 'Tipo de Centro Costo',
'Tipo de Cliente': 'Tipo de Cliente',
'Tipo de Comprobante': 'Tipo de Comprobante',
'Tipo de Contacto': 'Tipo de Contacto',
'Tipo de Cotizacion': 'Tipo de Cotizacion',
'Tipo de Inversion': 'Tipo de Inversion',
'Tipo de Pago': 'Tipo de Pago',
'Tipo de Recibo': 'Tipo de Recibo',
'Tipo de Trabajo': 'Tipo de Trabajo',
'Tipo de Transaccion': 'Tipo de Transaccion',
'Tipo de Visita': 'Tipo de Visita',
'Tipo Ejecucion': 'Tipo Ejecucion',
'Tipo Herrramientas epp': 'Tipo Herrramientas epp',
'Tipo Horario': 'Tipo Horario',
'Tipo Personal': 'Tipo Personal',
'Tipo Producto': 'Tipo Producto',
'Tipos de Gastos': 'Tipos de Gastos',
'Tipos de Relaciones': 'Tipos de Relaciones',
'Tipos de Servicios': 'Tipos de Servicios',
'Tipos Documentos': 'Tipos Documentos',
'To create a plugin, name a file/folder plugin_[name]': 'To create a plugin, name a file/folder plugin_[name]',
'Traceback': 'Traceback',
'Translation strings for the application': 'Translation strings for the application',
'Try the mobile interface': 'Try the mobile interface',
'Twitter': 'Twitter',
'unable to parse csv file': 'unable to parse csv file',
'Unidades de Medida': 'Unidades de Medida',
'Uninstall': 'Uninstall',
'update all languages': 'update all languages',
'Update:': 'Update:',
'Upload': 'Upload',
'Upload a package:': 'Upload a package:',
'Upload and install packed application': 'Upload and install packed application',
'upload file:': 'upload file:',
'upload plugin file:': 'upload plugin file:',
'Use (...)&(...) for AND, (...)|(...) for OR, and ~(...)  for NOT to build more complex queries.': 'Use (...)&(...) for AND, (...)|(...) for OR, and ~(...)  for NOT to build more complex queries.',
'User': 'အသုံးပြုသူ',
'User ID': 'User ID',
'Users': 'အသုံးပြုသူများ',
'Usuarios': 'Usuarios',
'Ventas': 'Ventas',
'Ventas Fechas': 'Ventas Fechas',
'Verify Password': 'စကားဝှက်ကို အတည်ပြုပါ',
'Version': 'Version',
'Versioning': 'Versioning',
'Viaticos': 'Viaticos',
'Videos': 'ဗွီဒီယိုများ',
'View': 'ဗျူး',
'Views': 'ဗျူးများ',
'views': 'views',
'Visitas': 'Visitas',
'Viáticos': 'Viáticos',
'Web Framework': 'Web Framework',
'Welcome': 'ကြိုဆိုပါ၏',
'Welcome to web2py!': 'web2py မှ ကြိုဆိုပါသည်။',
'Which called the function %s located in the file %s': 'Which called the function %s located in the file %s',
'Working...': 'ဆောင်ရွက်နေပါသည် ။ ။ ။',
'You are successfully running web2py': 'သင်သည် web2py ကို အောင်မြင်စွာ လည်ပတ်မောင်းနှင်စေပါသည်။',
'You can modify this application and adapt it to your needs': 'သင် ဒီ application ကို ပြုပြင်မွမ်းမံနိုင်ပါသည်။ ထို့အပြင် သင့်လိုအပ်ချက်များနှင့် ကိုက်ညီစေရန် ပြုလုပ်နိုင်ပါသည်။',
'You visited the url %s': 'သင် လည်ပတ်ခဲ့သော URL %s',
'စကားဝှက် အသစ် တောင်းဆိုရန်': 'စကားဝှက် အသစ် တောင်းဆိုရန်',
'မှတ်ပုံတင်ရန်': 'မှတ်ပုံတင်ရန်',
'ဝင်ရောက်အသုံးပြုရန်': 'ဝင်ရောက်အသုံးပြုရန်',
}
