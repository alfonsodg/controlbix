# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()


def marcas():
    response.view = 'corporate/default.html'
    form = SQLFORM.smartgrid(db.marcas_definiciones,maxtextlength=200, formstyle='bootstrap', linked_tables=[
        'contactos_marcas',
        'servicios_productos',
        'convenios_contratos',
        'suscripciones',
        'comercial',
        'marketing',
        'dd_negocios',
        'administracion_documentos',
        'politicas_protocolos'
    ], csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Marcas')

def documentos_marcas():
    form = SQLFORM.grid(db.documentos_marcas, csv=False, orderby=~db.documentos_marcas.id)
    return dict(form=form, page_name="Marcas")


def proveedores():
    response.view = 'corporate/default.html'
    form = SQLFORM.smartgrid(db.proveedores,maxtextlength=200, formstyle='bootstrap', linked_tables=[
        'contactos_proveedores',
        'cuentas_bancarias_proveedores',
        'documentos_proveedores',
        'asignacion_proveedor'
    ], csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name='Proveedores')

def personal():
    response.view = 'corporate/default.html'
    form = SQLFORM.smartgrid(db.personal,maxtextlength=200, formstyle='bootstrap',linked_tables=[
        'documentos_personal',
        'personal_asignaciones',
        'tareos_personal'
    ], csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name='Personal')

def inventario_productos():
    response.view = 'corporate/default.html'
    form = SQLFORM.smartgrid(db.inventario_productos, maxtextlength=200, formstyle='bootstrap', linked_tables=[
        'asignacion_personal_inventario',
        'proveedor_asignacion',
        'almacenes'
    ], csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name='Inventario de Productos')

def inventarios():
    response.view = 'corporate/default.html'
    form = SQLFORM.smartgrid(db.inventarios, linked_tables=[
        'inventarios_asignaciones',
        'inventarios_proveedor',
        'inventarios_almacenes'
    ], csv=False)
    return dict(form=form,page_name='Inventario de Productos')


def control_calendario():
    date_start = now.month
    new_data = []
    # data = db(db.requerimientos.fecha.month() == date_start).select(db.requerimientos.id, db.requerimientos.requerimiento,db.requerimientos.fecha
    #                                                         )
    # new_data = []
    # for row in data:
    #     nrow = {'id': int(row.id), 'title': '{}'.format(row.requerimiento),
    #             'start': row.fecha.isoformat()}
    #     new_data.append(nrow)
    # data = db(db.cotizaciones_enviadas.fecha.month() == date_start).select(db.cotizaciones_enviadas.id, db.cotizaciones_enviadas.nota,db.cotizaciones_enviadas.fecha
    #                                                         )
    # for row in data:
    #     nrow = {'id': int(row.id), 'title': '{}'.format(row.nota),
    #             'start': row.fecha.isoformat()}
    #     new_data.append(nrow)
    data = db(db.negociaciones_aprobadas.fecha.month() == date_start).select(db.negociaciones_aprobadas.id, db.negociaciones_aprobadas.cotizacion,db.negociaciones_aprobadas.fecha
                                                            )
    for row in data:
        nrow = {'id': int(row.id), 'title': '{}'.format(row.cotizacion),
                'start': row.fecha.isoformat()}
        new_data.append(nrow)
    data = db(db.proyectos_entregados.fecha.month() == date_start).select(db.proyectos_entregados.id, db.proyectos_entregados.proyecto,db.proyectos_entregados.fecha
                                                            )
    for row in data:
        nrow = {'id': int(row.id), 'title': '{}'.format(row.proyecto),
                'start': row.fecha.isoformat()}
        new_data.append(nrow)
    return dict(events=new_data)

def registros():
    fecha=request.vars.fecha
    return dict(dia=fecha)

def requerimientos():
    fecha = request.vars.fecha
    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
    response.view = 'corporate/default.html'
    db.requerimientos.fecha.default = fecha
    db.requerimientos.fecha.writable = False
    query = (db.requerimientos.fecha == fecha)
    form = SQLFORM.grid(query, csv=False, orderby=~db.requerimientos.id)
    return dict(form=form, page_name='Requerimientos %s' % fecha)

def cotizaciones_enviadas():
    fecha = request.vars.fecha
    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
    response.view = 'corporate/default.html'
    db.cotizaciones_enviadas.fecha.default = fecha
    db.cotizaciones_enviadas.fecha.writable = False
    db.cotizaciones_enviadas.cotizacion.writable = False
    db.cotizaciones_enviadas.marca.writable = False
    query = (db.cotizaciones_enviadas.fecha == fecha)
    form = SQLFORM.grid(query, csv=False, orderby=~db.cotizaciones_enviadas.id)
    return dict(form=form, page_name='Cotizaciones Enviadas %s' % fecha)

def negociaciones_aprobadas():
    fecha = request.vars.fecha
    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
    response.view = 'corporate/default.html'
    db.negociaciones_aprobadas.fecha.default = fecha
    db.negociaciones_aprobadas.fecha.writable = False
    db.negociaciones_aprobadas.cotizacion.writable = False
    db.negociaciones_aprobadas.marca.writable = False
    query = (db.negociaciones_aprobadas.fecha == fecha)
    form = SQLFORM.grid(query, csv=False, orderby=~db.negociaciones_aprobadas.id)
    return dict(form=form, page_name='Negociaciones Aprobadas %s' % fecha)

def proyectos_entregados():
    fecha = request.vars.fecha
    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
    response.view = 'corporate/default.html'
    db.proyectos_entregados.fecha.default = fecha
    db.proyectos_entregados.fecha.writable = False
    db.proyectos_entregados.cotizacion.writable = False
    db.proyectos_entregados.proyecto.writable = False
    db.proyectos_entregados.marca.writable = False
    query = (db.proyectos_entregados.fecha == fecha)
    form = SQLFORM.grid(query, csv=False, orderby=~db.proyectos_entregados.id)
    return dict(form=form, page_name='Proyectos Entregados %s' % fecha)

def personal_operaciones():
    fecha = request.vars.fecha
    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
    response.view = 'corporate/default.html'
    db.personal_operaciones.fecha.default = fecha
    db.personal_operaciones.fecha.writable = False
    query = (db.personal_operaciones.fecha == fecha)
    form = SQLFORM.grid(query, csv=False, orderby=~db.personal_operaciones.id)
    return dict(form=form, page_name='Personal Operaciones %s' % fecha)

def proveedor_operaciones():
    fecha = request.vars.fecha
    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
    response.view = 'corporate/default.html'
    db.proveedor_operaciones.fecha.default = fecha
    db.proveedor_operaciones.fecha.writable = False
    query = (db.proveedor_operaciones.fecha == fecha)
    form = SQLFORM.grid(query, csv=False, orderby=~db.proveedor_operaciones.id)
    return dict(form=form, page_name='Proveedor Operaciones %s' % fecha)

def reporte_diario():
    fecha = request.vars.fecha
    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
    response.view = 'corporate/default.html'
    db.reporte_diario.fecha.default = fecha
    db.reporte_diario.fecha.writable = False
    query = (db.reporte_diario.fecha == fecha)
    form = SQLFORM.grid(query, csv=False, orderby=~db.reporte_diario.id)
    return dict(form=form, page_name='Reporte Diario %s' % fecha)

def marcas_datagrid():
    data_vars = {
        'main':{
            'table': 'marcas_definiciones',
            'content': 'marca',
            'format': 'marca',
            'label': 'Marca...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'marca',
        },
        'aux_1':{
            'table':'contactos_marcas',
            'label': 'Contactos',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'servicios_productos',
            'label': 'Servicios Productos',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'redes_sociales',
            'label': 'Redes Sociales',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_4':{
            'table':'administracion_documentos',
            'label': 'Administracion Documentos',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'file'],
                [120, 'text']
            ]
        }
    }
    relations = {
        'documento': {'tipos_documentos':'nombre'},
        'tipo_cuenta': {'tipos_cuentas':'nombre'},
        'marca': {'marcas_definiciones':'marca'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    grid_4_heads = grid_headers(data_vars['aux_4']['table'], data_vars['aux_4']['data_type'], relations)
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads,grid_4_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)



def proveedores_datagrid():
    data_vars = {
        'main':{
            'table': 'proveedores',
            'content': 'proveedor',
            'format': 'proveedor',
            'label': 'Proveedor...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'proveedor',
        },
        'aux_1':{
            'table':'contactos_proveedores',
            'label': 'Contactos',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'cuentas_bancarias_proveedores',
            'label': 'Cuentas Bancarias',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'documentos_proveedores',
            'label': 'Documentos',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'file']
            ]
        },
        'aux_4':{
            'table':'asignacion_proveedor',
            'label': 'Asignacion Proveedor',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date']
            ]
        }
    }
    relations = {
        'proveedor': {'proveedores':'proveedor'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_cuenta': {'tipos_cuentas':'nombre'},
        'moneda': {'monedas':'nombre'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'},
        'producto': {'inventario_productos':'producto'},
        'tipo_producto': {'tipos_productos':'nombre'},
        'estado_producto': {'estados_productos':'nombre'},

    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    grid_4_heads = grid_headers(data_vars['aux_4']['table'], data_vars['aux_4']['data_type'], relations)
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads,grid_4_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def personal_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Documentos',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'file'],
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Asignaciones',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'tareos_personal',
            'label': 'Tareo personal',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'checkbox'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        # 'aux_4':{
        #     'table':'herramienta_epp',
        #     'label': 'Herramienta epp',
        #     'data_type':[
        #         [10, 'text'],
        #         [120, 'select'],
        #         [120, 'select'],
        #         [120, 'text']
        #     ]
        # },
    }
    relations = {
        'persona': {'personal':'persona'},
        'epp_herramienta': {'epps_herramientas':'nombre'},
        #'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'tipo_producto': {'tipos_productos':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'producto': {'inventario_productos':'producto'},
        'posicion_rol': {'posiciones_roles':'nombre'},
        'tipo_horario': {'tipos_horarios':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def marcas_aux_datagrid():
    data_vars = {
        'main':{
            'table': 'marcas_definiciones',
            'content': 'marca',
            'format': 'marca',
            'label': 'Marca...',
            'aux_base': '',
            'relation': 'marcas:nombre:nombre',
            'join': 'marca',
            'inner': 'marca_definicion',
        },
        'aux_1':{
            'table':'contactos_marcas',
            'label': 'Contactos',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'web_redes_sociales',
            'label': 'Web / Redes Sociales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'documentos_marcas',
            'label': 'Documentos',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        }

    }
    relations = {
        'red_social': {'redes_sociales':'nombre'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_cuenta': {'tipos_cuentas':'nombre'},
        'marca_definicion': {'marcas_definiciones':'marca-marcas-nombre'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def cliente_datagrid():
    data_vars = {
        'main':{
            'table': 'clientes',
            'content': 'cliente',
            'format': 'cliente',
            'label': 'Cliente ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'cliente',
        },


        'aux_1':{
            'table':'contactos',
            'label': 'Contactos Clientes',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'clientes_direcciones',
            'label': 'Direccion',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'file'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'comercial_cliente',
            'label': 'Comercial',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text']
            ]
        },
        'aux_4':{
            'table':'visitas',
            'label': 'Visita',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_5':{
            'table':'reportes',
            'label': 'Reporte',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                #[120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text']
            ]
        },
        'aux_6':{
            'table':'presupuestos',
            'label': 'Presupuesto',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_7':{
            'table':'negociaciones',
            'label': 'Negociacion',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [150, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_8':{
            'table':'operaciones',
            'label': 'Operaciones',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text']
            ]
        },

        'aux_9':{
            'table':'post_venta',
            'label': 'Post Venta',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text']
            ]
        },

        'aux_10':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'text']

            ]
        },

    }
    relations = {
        'area_trabajo': {'areas_trabajo':'nombre'},
        'direccion_cliente': {'clientes_direcciones':'direccion_cliente'},
        'numero_ticket': {'tickets':'id'},
        'marca_gg': {'marcas_definiciones':'marca'},
        'cliente': {'clientes':'cliente'},
        'posicion_rol': {'posiciones_roles':'nombre'},
        'distrito': {'distritos':'nombre'},
        'provincia': {'provincias':'nombre'},
        'estado_carta_presentacion': {'estados_cartas_presentacion':'nombre'},
        'tipo_servicio': {'tipos_servicios':'nombre'},
        'estado_visita': {'estados_visitas':'nombre'},
        'tipo_visita': {'tipos_visitas':'nombre'},
        'persona': {'personal':'persona'},
        'tipo_contacto': {'tipos_contactos':'nombre'},
        'estado_reporte': {'estados_reportes':'nombre'},
        'estado_presupuesto': {'estados_presupuestos':'nombre'},
        'contacto_visita': {'contactos':'contacto_cliente'},
        'numero_cotizacion': {'cotizaciones':'numero_cotizacion'},
        'estado_negociacion': {'estados_negociacion':'nombre'},
        'tiempo_decision': {'tiempos_decision':'nombre'},
        'interes': {'intereses':'nombre'},
        'estado_operacion': {'estados_operaciones':'nombre'},
        'numero_proyecto': {'proyectos':'numero_proyecto'},
        'razon_social': {'razones_sociales':'nombre'},
        'servicio_producto': {'servicios_productos':'servicio_producto'},
        'estado_post_venta': {'estados_post_venta':'nombre'},
        'encuesta_satisfaccion': {'encuestas_satisfaccion':'nombre'},
        'tipo_ejecucion': {'tipos_ejecuciones':'nombre'}


    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    grid_4_heads = grid_headers(data_vars['aux_4']['table'], data_vars['aux_4']['data_type'], relations)
    grid_5_heads = [],
    grid_6_heads = grid_headers(data_vars['aux_6']['table'], data_vars['aux_6']['data_type'], relations)
    grid_7_heads = grid_headers(data_vars['aux_7']['table'], data_vars['aux_7']['data_type'], relations)
    grid_8_heads = grid_headers(data_vars['aux_8']['table'], data_vars['aux_8']['data_type'], relations)
    grid_9_heads = grid_headers(data_vars['aux_9']['table'], data_vars['aux_9']['data_type'], relations)
    grid_10_heads = []
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads, grid_4_heads,
                  grid_5_heads, grid_6_heads, grid_7_heads, grid_8_heads, grid_9_heads, grid_10_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def contactos_datagrid():
    data_vars = {
        'main':{
            'table': 'contactos_clientes',
            'content': 'contacto',
            'format': 'contacto',
            'label': 'Contacto ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': '',
        },
        'aux_1':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
    }
   #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    grid_1_heads = []
    grid_2_heads = []
    grid_3_heads = []
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    #grid_heads = []
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)



def direcciones_datagrid():
    data_vars = {
        'main':{
            'table': 'direcciones',
            'content': 'direccion',
            'format': 'direccion',
            'label': 'Direccion ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': '',
        },
        'aux_1':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
    }
   #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    grid_1_heads = []
    grid_2_heads = []
    grid_3_heads = []
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    #grid_heads = []
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


#              'table': 'cuentas_por_pagar',
#             'content': 'numero_ticket_gasto',
#             'format': 'numero_ticket_gasto',
#             'label': 'Numero Tcket_gasto ...',
#             'aux_base': '',
#             'relation': 'gastos_generales:numero_ticket_gasto:numero_ticket_gasto',
#             'join': 'numero_ticket_gasto',
#             'inner': 'numero_ticket_gasto',


#'numero_ticket': {'tickets':'id'},
def cotizacion_datagrid():
    data_vars = {
        'main':{
            'table': 'cotizaciones',
            'content': 'numero_ticket',
            'format': '',
            'label': 'Numero de Ticket ...',
            'aux_base': '',
            'relation': 'tickets:tickets:id:id',
            'join': 'numero_ticket',
            'inner': 'numero_ticket',
        },
        'aux_1':{
            'table':'propuesta_tecnica',
            'label': 'Propuesta Tecnica',
            'data_type':[

                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'propuesta_economica',
            'label': 'Propuesta Economica',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'numero_cotizacion': {'cotizaciones':'numero_cotizacion'},



    }
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = []
    grid_heads = [grid_1_heads, grid_2_heads]
    #grid_heads = []
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def proyectos_datagrid():
    data_vars = {
        'main':{
            'table': 'proyectos',
            'content': 'numero_proyecto',
            'format': 'numero_proyecto',
            'label': 'Numero de Proyecto ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'numero_proyecto',

        },
        'aux_1':{
            'table':'mano_de_obra',
            'label': 'Personales',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']

            ]
        },
        'aux_2':{
            'table':'productos',
            'label': 'Productos',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'costo_proyectos',
            'label': 'Costo Proyectos',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },

    }
    relations = {
        'numero_proyecto': {'proyectos':'numero_proyecto'},
        'producto': {'inventario_productos':'producto'},
        'persona': {'personal':'persona'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'posicion': {'posiciones_roles':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'},
        'unidad_medida': {'unidades_medidas':'nombre'},
        'descripcion_item': {'items_productos':'descripcion_item'},
    }

   #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    grid_heads = [grid_1_heads,grid_2_heads,grid_3_heads]
    #grid_heads = []
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def facturacion_datagrid():
    data_vars = {
        'main':{
            'table': 'facturacion',
            'content': 'numero_comprobante',
            'format': 'numero_comprobante',
            'label': 'Numero de comprobante ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'numero_comprobante',
        },
        'aux_1':{
            'table':'facturacion_items',
            'label': 'Items de Facturacion',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {

        'numero_comprobante': {'facturacion':'numero_comprobante'},

    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)

    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)




def cuentas_por_cobrar_datagrid():
    data_vars = {
        'main':{
            'table': 'cuentas_por_cobrar',
            'content': 'numero_comprobante',
            'format': 'numero_comprobante',
            'label': 'Numero Comprobante ...',
            'aux_base': '',
            'relation': 'facturacion:numero_comprobante:numero_comprobante',
            'join': 'numero_comprobante',
            'inner': 'numero_comprobante',
# 'table': 'costos_proyectos',
#             'content': 'numero_proyecto',
#             'format': 'id',
#             'label': 'Numero de Proyecto ...',
#             'aux_base': '',
#             'relation': 'proyectos:numero_proyecto:numero_proyecto',
#             'join': 'numero_proyecto',
#             'inner': 'numero_proyecto',

            # 'table': 'resultados',
            # 'content': 'razon_social_gg',
            # 'format': 'razon_social_gg',
            # 'label': 'Razon social ...',
            # 'aux_base': '',
            # 'relation': 'razones_zociales_gg:razon_social_gg:razon_social_gg',
            # 'join': 'razon_social_gg',
            # 'inner': 'razon_social_gg',
        },
        'aux_1':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'date'],
                [120, 'text'],
                [120, 'text']


            ]
        },
        'aux_2':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {



    }
    grid_3_heads = []
    grid_2_heads = []
    grid_1_heads = []
    grid_heads = [grid_1_heads,grid_2_heads,grid_3_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def cuentas_por_pagar_datagrid():
    data_vars = {
        'main':{
            'table': 'cuentas_por_pagar',
            'content': 'numero_ticket_gasto',
            'format': 'numero_ticket_gasto',
            'label': 'Numero Tcket_gasto ...',
            'aux_base': '',
            'relation': 'gastos_generales:numero_ticket_gasto:numero_ticket_gasto',
            'join': 'numero_ticket_gasto',
            'inner': 'numero_ticket_gasto',

        },
        'aux_1':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'date'],
                [120, 'text'],
            ]
        },
        'aux_2':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'date'],
                [120, 'text'],




            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {

    }
    grid_1_heads =  []
    grid_2_heads = []
    grid_3_heads = []

    grid_heads = [grid_1_heads,grid_2_heads,grid_3_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def cuentas_bancos_datagrid():
    data_vars = {
        'main':{
            'table': 'cuentas_bancarias',
            'content': 'numero_cuenta',
            'format': 'numero_cuenta',
            'label': 'Numero Cuenta ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'numero_cuenta',

        },
        'aux_1':{
            'table':'transacciones',
            'label': 'Transacciones',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'',
            'label': 'Cuenta Dos',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'',
            'label': 'Cuenta Tres',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_4':{
            'table':'',
            'label': 'Cuenta Cuatro',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'date'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
    }
    relations = {
        'numero_cuenta': {'cuentas_bancarias':'numero_cuenta'},
        'razon_social_gg': {'razones_zociales_gg':'razon_social_gg'},
        'tipo_transaccion': {'tipos_transacciones':'nombre'},
        'tipo_cuenta': {'tipos_cuentas':'nombre'},
        'moneda': {'monedas':'nombre'},
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = []
    grid_3_heads = []
    grid_4_heads = []

    grid_heads = [grid_1_heads,grid_2_heads,grid_3_heads,grid_4_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def costo_proyecto_datagrid():
    data_vars = {
        'main':{
            'table': 'costos_proyectos',
            'content': 'numero_proyecto',
            'format': 'id',
            'label': 'Numero de Proyecto ...',
            'aux_base': '',
            'relation': 'proyectos:numero_proyecto:numero_proyecto',
            'join': 'numero_proyecto',
            'inner': 'numero_proyecto',
        },
        'aux_1':{
            'table':'proyectos_mano_obra',
            'label': 'Item mano de Obra',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'proyectos_materiales_equipos',
            'label': 'Item materiales y equipos',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {

        'numero_proyecto': {'proyectos':'numero_proyecto'},
        'posicion_rol': {'posiciones_roles':'nombre'},
        'persona': {'personal':'persona'},
        'item': {'items_productos':'numero_item'},
        'unidad_medida': {'unidades_medidas':'nombre'},
    }

    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)

    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def contabilidad_datagrid():
    data_vars = {
        'main':{
            'table': 'gastos',
            'content': 'fecha',
            'format': 'id',
            'label': 'Fecha de mes de trabajo ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': '',

               #'relation': 'marcas:nombre:nombre',
               #'join': 'marca',
               #'inner': 'marca_definicion',
        },
        'aux_1':{
            'table':'',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
    }
   #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    grid_1_heads = []
    grid_2_heads = []
    grid_3_heads = []
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    #grid_heads = []
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def area_comercial_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Fecha de mes de trabajo ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def publicidad_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def finanzas_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def viaticos_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def combustibles_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def seguros_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def alquileres_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def mantenimiento_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def creditos_leasing_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def otros_gastos_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def prestamos_bancarios_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def prestamos_socios_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def capital_datagrid():
    data_vars = {
        'main':{
            'table': 'personal',
            'content': 'persona',
            'format': 'persona',
            'label': 'Persona...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'persona',
        },
        'aux_1':{
            'table':'documentos_personal',
            'label': 'Soluciones Integrales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'personal_asignaciones',
            'label': 'Geoperuvian Group',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'personal_asignaciones',
            'label': 'Geoenvi',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date']
            ]
        },

    }
    relations = {
        'persona': {'personal':'persona'},
        'documento': {'tipos_documentos':'nombre'},
        'tipo_personal': {'tipos_personal':'nombre'},
        'item': {'items_productos':'descripcion_item'},
        'area_trabajo': {'areas_trabajo':'nombre'},
        'posicion_rol': {'posiciones_roles':'nombre'}
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    #grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    #grid_3_heads = []
    #print grid_3_heads
    #grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def resultados_datagrid():
    data_vars = {
        'main':{
            'table': 'resultados',
            'content': 'razon_social_gg',
            'format': 'razon_social_gg',
            'label': 'Razon social ...',
            'aux_base': '',
            'relation': 'razones_zociales_gg:razon_social_gg:razon_social_gg',
            'join': 'razon_social_gg',
            'inner': 'razon_social_gg',
        },

        'aux_1':{
            'table':'registros_ventas',
            'label': 'Registros ventas',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'registros_cancelaciones',
            'label': 'Registros cancelaciones',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'date'],
                [120, 'text']
            ]
        },
    }
    relations = {
         # 'marca_definicion': {'marcas_definiciones':'marca-marcas-nombre'},
         # #'marca': {'cuentas_por_cobrar_y_pagar':'marca-marcas_definiciones-marca'},
          'razon_social_gg': {'resultados':'razon_social_gg-razones_zociales_gg-razon_social_gg'},
          'numero_cotizacion': {'cotizaciones':'numero_cotizacion'},
          'cliente': {'clientes':'cliente'},
          'posicion_rol': {'posiciones_roles':'nombre'},
          'marca_gg': {'marcas_definiciones':'marca'},
          'tipo_cliente': {'tipos_clientes':'nombre'},
         }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = []
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)



def inventario_productos_datagrid():
    data_vars = {
        'main':{
            'table': 'inventario_productos',
            'content': 'producto',
            'format': 'producto',
            'label': 'Producto ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'producto',
        },
        'aux_1':{
            'table':'asignacion_personal_inventario',
            'label': 'ASIGNACIONES PERSONAL',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'date']
            ]
        },
        'aux_2':{
            'table':'proveedor_asignacion',
            'label': 'ASIGNACIONES PROVEEDOR',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'date']
            ]
        },
        'aux_3':{
            'table':'almacenes',
            'label': 'ALMACEN',
            'data_type':[
                [10, 'text'],
                [120, 'date'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'date'],
            ]
        },

    }

    relations = {
          'producto': {'inventario_productos':'producto'},
          'personal': {'personal':'persona'},
          'proveedor': {'proveedores':'proveedor'},
          'marca': {'marcas_definiciones':'marca'},
          'tipo_producto': {'tipos_productos':'nombre'},
          'unidad_medida': {'unidades_medidas':'nombre'},
    }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_3_heads = grid_headers(data_vars['aux_3']['table'], data_vars['aux_3']['data_type'], relations)
    grid_heads = [grid_1_heads, grid_2_heads, grid_3_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)


def cuentas_por_cobrar_y_pagar_datagrid():
    data_vars = {
        'main':{
            'table': 'cuentas_por_cobrar_y_pagar',
            'content': 'marca',
            'format': 'marca',
            'label': 'Marca ...',
            'aux_base': '',
            'relation': 'marcas_definiciones:marca:marca',
            'join': 'marca',
            'inner': 'marca',
        },

        'aux_1':{
            'table':'cobranzas',
            'label': 'Cobranzas',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'date'],
                [120, 'date'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'pagos',
            'label': 'Pagos',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'date'],
                [120, 'select'],
                [120, 'date'],
                [120, 'checkbox'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'date'],
                [120, 'date'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
    }

    relations = {
#'marca_definicion': {'marcas_definiciones':'marca-marcas-nombre'},
            'marca': {'cuentas_por_cobrar_y_pagar':'marca-marcas_definiciones-marca'},
            #'marca': {'cuentas_por_cobrar_y_pagar':'marca'},
            'contacto_cliente': {'contactos':'contacto_cliente'},
            'numero_cotizacion': {'cotizaciones':'numero_cotizacion'},
            'estado_cuenta_cobrar': {'estados_cuentas_cobrar':'nombre'},
            'estado_vencimiento': {'estados_vencimientos':'nombre'},
            'tipo_recibo': {'tipos_recibos':'nombre'},
            'tipo_centro_costo': {'tipos_centros_costos':'nombre'},
            'moneda': {'monedas':'nombre'},
            'cliente': {'clientes':'cliente'},
            'tipo_pago': {'tipos_pagos':'nombre'},
            'estado_cuenta_pagar': {'estados_cuentas_pagar':'nombre'},
         }
    grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
    grid_2_heads = grid_headers(data_vars['aux_2']['table'], data_vars['aux_2']['data_type'], relations)
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

# def gastos_inversiones_datagrid():
#     data_vars = {
#         'main':{
#
#             'table': 'gastos_inversiones',
#             'content': 'razon_social_gg',
#             'format': 'razon_social_gg',
#             'label': 'Razon Social  ...',
#             'aux_base': '',
#             'relation': 'razones_zociales_gg:razon_social_gg:razon_social_gg',
#             'join': 'razon_social_gg',
#             'inner': 'razon_social_gg',
#
#         },
#         'aux_1':{
#             'table':'gastos_generales',
#             'label': 'Gastos Generales',
#             'data_type':[
#                 [10, 'text'],
#                 [120, 'select'],
#                 [120, 'text'],
#                 [120, 'text'],
#                 [120, 'select'],
#                 [120, 'text'],
#                 [120, 'select'],
#                 [120, 'text'],
#                 [120, 'text'],
#                 [120, 'text']
#             ]
#         },
#         'aux_2':{
#             'table':'inversione',
#             'label': 'Inversiones',
#             'data_type':[
#                  [10, 'text'],
#                 [120, 'select'],
#                 [120, 'select'],
#                 [120, 'text'],
#                 [120, 'text'],
#                 [120, 'text'],
#                 [120, 'text'],
#                 [120, 'text']
#             ]
#         },
#         'aux_3':{
#             'table':'',
#             'label': '',
#             'data_type':[
#                 [10, 'text'],
#                 [120, 'select'],
#                 [120, 'select'],
#                 [120, 'text'],
#                 [120, 'select'],
#                 [120, 'select'],
#                 [120, 'text'],
#                 [120, 'text'],
#                 [120, 'text']
#             ]
#         }
#     }
#     relations = {
#             #'razon_social_gg': {'resultados':'razon_social_gg-razones_zociales_gg-razon_social_gg'},
#             'razon_social_gg': {'gastos_inversiones':'razon_social_gg-razones_zociales_gg-razon_social_gg'},
#             'tipo_centro_costo': {'tipos_centros_costos':'nombre'},
#             'tipo_pago': {'tipos_pagos':'nombre'},
#             'proveedor': {'proveedores':'proveedor'},
#             'marca': {'marcas':'nombre'},
#             'tipo_inversion': {'tipos_inversiones':'nombre'},
#          }
#     grid_1_heads = grid_headers(data_vars['aux_1']['table'], data_vars['aux_1']['data_type'], relations)
#     grid_2_heads = []
#     grid_3_heads = []
#     grid_heads = [grid_1_heads, grid_2_heads]
#     aux_tables = aux_table(relations)
#     return dict(data_vars=data_vars,
#                 grid_heads=grid_heads,
#                 aux_tables=aux_tables)

def gastos_generales_datagrid():
    data_vars = {
        'main':{

            'table': 'gastos_generales',
            'content': 'numero_ticket_gasto',
            'format': 'numero_ticket_gasto',
            'label': 'Numero Ticket gasto ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': '',

        },
        'aux_1':{
            'table':'',
            'label': 'Gastos Generales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'',
            'label': 'Inversiones',
            'data_type':[
                 [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        }
    }
    relations = {

         }
    grid_1_heads = []
    grid_2_heads = []
    grid_3_heads = []
    grid_heads = [grid_1_heads, grid_2_heads,grid_3_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)

def inversiones_datagrid():
    data_vars = {
        'main':{

            'table': 'inversiones',
            'content': 'numero_ticket_inversion',
            'format': 'numero_ticket_inversion',
            'label': 'Numero Ticket Inversion ...',
            'aux_base': '',
            'relation': '',
            'join': '',
            'inner': 'numero_ticket_inversion',

        },
        'aux_1':{
            'table':'',
            'label': 'Gastos Generales',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_2':{
            'table':'',
            'label': 'Inversiones',
            'data_type':[
                 [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        },
        'aux_3':{
            'table':'',
            'label': '',
            'data_type':[
                [10, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'select'],
                [120, 'select'],
                [120, 'text'],
                [120, 'text'],
                [120, 'text']
            ]
        }
    }
    relations = {

         }
    grid_1_heads = []
    grid_2_heads = []
    grid_3_heads = []
    grid_heads = [grid_1_heads, grid_2_heads]
    aux_tables = aux_table(relations)
    return dict(data_vars=data_vars,
                grid_heads=grid_heads,
                aux_tables=aux_tables)