# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def marcas():
    name = 'Marcas'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.marcas)
    return dict(form=form, page_name=name)

def epps_herramientas():
    name = 'Tipo Herrramientas epp'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.epps_herramientas)
    return dict(form=form, page_name=name)

def razones_sociales():
    name = 'Razones Sociales'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.razones_sociales)
    return dict(form=form, page_name=name)

def prioridades():
    name = 'Razones Sociales'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.prioridades)
    return dict(form=form, page_name=name)

def areas_trabajo():
    name = 'Areas de Trabajo'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.areas_trabajo)
    return dict(form=form, page_name=name)

def tipos_servicios():
    name = 'Tipos de Servicios'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_servicios)
    return dict(form=form, page_name=name)

def medios_captacion():
    name = 'Medios de Captación'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.medios_captacion)
    return dict(form=form, page_name=name)

def tipos_visitas():
    name = 'Tipos de Visitas'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_visitas)
    return dict(form=form, page_name=name)

def tipos_personal():
    name = 'Tipos de Personal'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_personal)
    return dict(form=form, page_name=name)

def distritos():
    name = 'Distritos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.distritos)
    return dict(form=form, page_name=name)

def provincias():
    name = 'Provincias'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.provincias)
    return dict(form=form, page_name=name)

def tipos_cuentas():
    name = 'Tipos de Cuentas'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_cuentas)
    return dict(form=form, page_name=name)

def redes_sociales():
    name = 'Redes Sociales'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.redes_sociales)
    return dict(form=form, page_name=name)

def tipos_documentos():
    name = 'Tipos de Documentos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_documentos)
    return dict(form=form, page_name=name)

def areas_negocios():
    name = 'Areas de Negocios'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.areas_negocios)
    return dict(form=form, page_name=name)

def tipos_cotizaciones():
    name = 'Tipos de Cotizaciones'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_cotizaciones)
    return dict(form=form, page_name=name)

def monedas():
    name = 'Monedas'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.monedas)
    return dict(form=form, page_name=name)

def intereses():
    name = 'Interes'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.intereses)
    return dict(form=form, page_name=name)

def tiempos_decision():
    name = 'Tiempos de Decisión'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tiempos_decision)
    return dict(form=form, page_name=name)

def alcances_servicios():
    name = 'Alcances de Servicios'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.alcances_servicios)
    return dict(form=form, page_name=name)

def calidades_acabados():
    name = 'Calidades de Acabados'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.calidades_acabados)
    return dict(form=form, page_name=name)

def calidades_materiales():
    name = 'Calidades de Materiales'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.calidades_materiales)
    return dict(form=form, page_name=name)

def calidades_limpieza():
    name = 'Calidades de Limpieza'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.calidades_limpieza)
    return dict(form=form, page_name=name)

def disposiciones_desmonte():
    name = 'Disposicion'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.disposiciones_desmonte)
    return dict(form=form, page_name=name)

def tiempos_garantia():
    name = 'Tiempos de Garantía'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tiempos_garantia)
    return dict(form=form, page_name=name)

def tipos_transacciones():
    name = 'Tipos de Transacciones'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_transacciones)
    return dict(form=form, page_name=name)

def tipos_trabajos():
    name = 'Tipos de Trabajos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_trabajos)
    return dict(form=form, page_name=name)

def tipos_horarios():
    name = 'Tipos de Horarios'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_horarios)
    return dict(form=form, page_name=name)

def tipos_comprobantes():
    name = 'Tipos de Comprobantes'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_comprobantes)
    return dict(form=form, page_name=name)

def tipos_gastos():
    name = 'Tipos de Gastos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_gastos)
    return dict(form=form, page_name=name)

def encuestas_satisfaccion():
    name = 'Encuestas de Satisfacción'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.encuestas_satisfaccion)
    return dict(form=form, page_name=name)

def unidades_medidas():
    name = 'Unidades de Medida'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.unidades_medidas)
    return dict(form=form, page_name=name)

def posiciones_roles():
    name = 'Posiciones / Roles'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.posiciones_roles)
    return dict(form=form, page_name=name)

def plazos_pagos():
    name = 'Plazos de Pago'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.plazos_pagos)
    return dict(form=form, page_name=name)

def tipos_productos():
    name = 'Tipos de Productos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_productos)
    return dict(form=form, page_name=name)

def estados_clientes():
    name = 'Estados de Clientes'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_clientes)
    return dict(form=form, page_name=name)

def estados_cartas_presentacion():
    name = 'Estados de Carta de Presentación'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_cartas_presentacion)
    return dict(form=form, page_name=name)

def estados_visitas():
    name = 'Estados Visitas'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_visitas)
    return dict(form=form, page_name=name)

def estados_reportes():
    name = 'Estados Reportes'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_reportes)
    return dict(form=form, page_name=name)

def estados_presupuestos():
    name = 'Estados Presupuestos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_presupuestos)
    return dict(form=form, page_name=name)

def estados_negociacion():
    name = 'Estados Negociación'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_negociacion)
    return dict(form=form, page_name=name)

def estados_operaciones():
    name = 'Estados Operaciones'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_operaciones)
    return dict(form=form, page_name=name)

def estados_facturacion():
    name = 'Estados Facturación'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_facturacion)
    return dict(form=form, page_name=name)

def estados_cuentas_cobrar():
    name = 'Estados Cuentas por Cobrar'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_cuentas_cobrar)
    return dict(form=form, page_name=name)

def estados_cuentas_pagar():
    name = 'Estados Cuentas por Pagar'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_cuentas_pagar)
    return dict(form=form, page_name=name)

def estados_productos():
    name = 'Estados Productos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_productos)
    return dict(form=form, page_name=name)

def estados_post_venta():
    name = 'Estados de Post Venta'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_post_venta)
    return dict(form=form, page_name=name)

def estados_personal():
    name = 'Estados de Personal'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_personal)
    return dict(form=form, page_name=name)

def estados_contratos():
    name = 'Estados de Contratos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_contratos)
    return dict(form=form, page_name=name)


def estados_cheques():
    name = 'Estados Cheques'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.estados_cheques)
    return dict(form=form, page_name=name)

def capitales():
    name = 'Capitales'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.capitales)
    return dict(form=form, page_name=name)

def razones_zociales():
    name = 'Razon Social gg'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.razones_zociales_gg)
    return dict(form=form, page_name=name)

def tipos_relaciones():
    name = 'Tipos de Relaciones'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_relaciones)
    return dict(form=form, page_name=name)

def tipos_clientes():
    name = 'Tipos de clientes'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_clientes)
    return dict(form=form, page_name=name)

def tipos_recibos():
    name = 'Tipos de recibos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_recibos)
    return dict(form=form, page_name=name)

def tipos_centros_costos():
    name = 'Tipos de centros de costos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_centros_costos)
    return dict(form=form, page_name=name)

def tipos_pagos():
    name = 'Tipos de pagos'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_pagos)
    return dict(form=form, page_name=name)

def tipos_inversiones():
    name = 'Tipos de inversiones'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_inversiones)
    return dict(form=form, page_name=name)

def tipos_ejecuciones():
    name = 'Tipos de Ejecuciones'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_ejecuciones)
    return dict(form=form, page_name=name)

def tipos_contacto():
    name = 'Tipo Contacto'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.tipos_contactos)
    return dict(form=form, page_name=name)

def t_documentos_marcas():
    name = 'Marcas'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.documentos_marcas)
    return dict(form=form, page_name=name)

def t_redes_sociales():
    name = 'Marcas'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.redes_sociales)
    return dict(form=form, page_name=name)

def actividades():
    name = 'Actividades'
    response.view = 'config/default.html'
    form = SQLFORM.grid(db.actividades)
    return dict(form=form, page_name=name)