# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def comercial():
    response.view = 'processes/default.html'
    form = SQLFORM.grid(db.comercial_cliente,maxtextlength=200, formstyle='bootstrap', csv=False, orderby=~db.comercial_cliente.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name="Comercial")

def visitas():
    response.view = 'processes/default.html'
    form = SQLFORM.grid(db.visitas,maxtextlength=200, formstyle='bootstrap',
                        fields=[
                            db.visitas.id,
                            db.visitas.fecha_registro,
                            db.visitas.numero_ticket,
                            db.visitas.referencia,
                            db.visitas.fecha_registro_ticket,
                            db.visitas.cliente,
                            db.visitas.marca_gg,
                            db.visitas.contacto_visita,
                            db.visitas.servicio_producto,
                            db.visitas.estado_visita,
                            db.visitas.tipo_visita,
                            db.visitas.fecha_visita,
                            db.visitas.hora_visita,
                            db.visitas.direccion_cliente,
                            db.visitas.distrito,
                            db.visitas.referencia_visita,
                            db.visitas.personal_visita,
                            db.visitas.nota,
                            ],
    csv=False, orderby=~db.visitas.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name="Visitas")

def reportes():
    response.view = 'processes/default.html'
    form = SQLFORM.grid(db.reportes,maxtextlength=200, formstyle='bootstrap', csv=False, orderby=~db.reportes.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name="Reportes")

def presupuestos():
    response.view = 'processes/default.html'
    form = SQLFORM.grid(db.presupuestos,maxtextlength=200, formstyle='bootstrap', csv=False, orderby=~db.presupuestos.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name="Presupuestos")

def negociaciones():
    response.view = 'processes/default.html'
    form = SQLFORM.grid(db.negociaciones,maxtextlength=200, formstyle='bootstrap', csv=False, orderby=~db.negociaciones.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name="Negociaciones")

def operaciones():
    response.view = 'processes/default.html'
    form = SQLFORM.grid(db.operaciones,maxtextlength=200, formstyle='bootstrap', csv=False, orderby=~db.operaciones.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name="Operaciones")

def post_venta():
    response.view = 'processes/default.html'
    form = SQLFORM.grid(db.post_venta,maxtextlength=200, formstyle='bootstrap', csv=False, orderby=~db.post_venta.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name="Post Venta")