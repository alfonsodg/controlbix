# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def facturacion():
    response.view = 'finances/default.html'
    form = SQLFORM.smartgrid(db.facturacion,maxtextlength=200, formstyle='bootstrap',linked_tables=[
        'facturacion_items'
    ], csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name="Facturación")

# def cuentas_por_cobrar_y_pagar():
#     response.view = 'finances/default.html'
#     form = SQLFORM.smartgrid(db.cuentas_por_cobrar_y_pagar, linked_tables=[
#         'cobranzas',
#         'pagos'
#     ])
#     return dict(form=form, page_name="Cuentas por cobrar")

def cuentas_bancarias():
    response.view = 'customers/default.html'
    #db.marcas_definiciones.marca.represent = 'aaa  '
    form = SQLFORM.smartgrid(db.cuentas_bancarias,maxtextlength=200, formstyle='bootstrap', linked_tables=[
        'transacciones'
    ], csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Cuentas Bancarias')

def gastos_inversiones():
    response.view = 'finances/default.html'
    form = SQLFORM.smartgrid(db.gastos_inversiones,maxtextlength=200, formstyle='bootstrap', linked_tables=[
        'gastos_generales',
        'inversione'
    ], csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form,page_name="Gastos Generales e Inversiones")


def cuentas_por_cobrar():
    response.view = 'finances/default.html'
    form = SQLFORM.grid(db.cuentas_por_cobrar,maxtextlength=200, formstyle='bootstrap', csv=False, orderby=~db.cuentas_por_cobrar.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Cuentas por Cobrar')

def cuentas_por_pagar():
    response.view = 'finances/default.html'
    form = SQLFORM.grid(db.cuentas_por_pagar, maxtextlength=200, formstyle='bootstrap',
                        fields=[
                                db.cuentas_por_pagar.id,
                                db.cuentas_por_pagar.fecha_registro,
                                db.cuentas_por_pagar.numero_ticket_gasto,
                                db.cuentas_por_pagar.personal_por_pagar,
                                db.cuentas_por_pagar.proveedor_por_pagar,
                                #db.cuentas_por_pagar.referencia,
                                db.cuentas_por_pagar.marca_gg,
                                db.cuentas_por_pagar.razon_social_gg,
                                db.cuentas_por_pagar.monto_detraccion,
                                db.cuentas_por_pagar.estado_cuenta_pagar,
                                db.cuentas_por_pagar.monto_parcial_a_cuenta,
                                db.cuentas_por_pagar.monto_saldo_por_pagar,
                                db.cuentas_por_pagar.tipo_pago,
                                db.cuentas_por_pagar.fecha_de_pago,
                                db.cuentas_por_pagar.fecha_pago_realizado,
                                db.cuentas_por_pagar.nota,
                                ],
                        csv=False
    , orderby=~db.cuentas_por_pagar.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Cuentas por Pagar')

def gastos_generales():
    response.view = 'finances/default.html'
    form = SQLFORM.grid(db.gastos_generales, maxtextlength=200, formstyle='bootstrap',csv=False, orderby=~db.gastos_generales.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Gastos Generales')

def inversiones():
    response.view = 'finances/default.html'
    form = SQLFORM.grid(db.inversiones, maxtextlength=200, formstyle='bootstrap',csv=False, orderby=~db.inversiones.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Inversiones')