# -*- coding: utf-8 -*-
import calendar


### required - do no delete
def user(): return dict(form=auth())


def download(): return response.download(request, db)


def call(): return service()


### end requires
def index():
    return dict()


def error():
    return dict()


def format_table_aux(data, total):
    tmp = []
    for elem in data:
        # print dir(elem)
        content = TR([TD(elem), TD(data[elem])])
        tmp.append(content)
    content = TR([TD(STRONG('Total General')), TD(STRONG(total))])
    tmp.append(content)

    return tmp


def format_table(data):
    # print data
    tmp = []
    cols = data.colnames
    # print 'AAA:',dir(data)
    # print cols
    for elem in data:
        # print dir(elem)
        # print elem
        # for col in cols:
        #    tmp = col.replace('"','').split('.')
        #    contentelem[tmp[0]][tmp[1]]
        # print col
        # print col.split(".")
        # print elem[col]
        content = TR([TD(elem[col.replace('"', '').split('.')[0]][col.replace('"', '').split('.')[1]]) for col in cols])

        tmp.append(content)
    return tmp


@auth.requires_login()
def personal_operaciones():
    estado_personal_1 = db.estados_personal(db.estados_personal.nombre == 'Operaciones')
    data = db(db.personal.estado_personal == estado_personal_1).select(
        db.personal.persona,
        db.personal.telefono_celular,
        db.estados_contratos.nombre,
        left=(

            db.estados_contratos.on(db.estados_contratos.id == db.personal.estado_contrato),
            # db.personal.on(db.personal.estado_personal.id==db.estados_personal.nombre),

            db.estados_personal.on(db.estados_personal.id == db.personal.estado_personal),
            # db.estados_presupuestos.on(db.estados_presupuestos.id==db.presupuestos.estado_presupuesto),
        )
    )
    data = format_table(data)
    return dict(data=data)


def personal_disponible():
    estado_personal = db.estados_personal(db.estados_personal.nombre == 'Disponible')
    data = db(db.personal.estado_personal == estado_personal).select(
        db.personal.persona,
        db.posiciones_roles.nombre,
        db.tipos_personal.nombre,
        db.personal.ocupacion,
        db.personal.especialidad,
        db.personal.pretencion_diaria,
        db.personal.pretension_mensual,
        db.personal.telefono_celular,
        left=(

            db.posiciones_roles.on(db.posiciones_roles.id == db.personal.posicion_rol),
            db.tipos_personal.on(db.tipos_personal.id == db.personal.tipo_personal),
            db.estados_personal.on(db.estados_personal.id == db.personal.estado_personal),

        )
    )
    data = format_table(data)
    return dict(data=data)


def cartas_presentacion():
    estado_carta = db.estados_cartas_presentacion(db.estados_cartas_presentacion.nombre == 'Elaboracion')
    data = db(db.comercial_cliente.estado_carta_presentacion == estado_carta).select(
        db.clientes.cliente,
        db.marcas_definiciones.marca,
        db.contactos.contacto_cliente,
        left=(
            db.clientes.on(db.clientes.id == db.comercial_cliente.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.comercial_cliente.marca_gg),
            db.contactos.on(db.contactos.id == db.comercial_cliente.contacto_cliente),
        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def citas():
    estado_visita_1 = db.estados_visitas(db.estados_visitas.nombre == 'Conversacion')
    estado_visita_2 = db.estados_visitas(db.estados_visitas.nombre == 'Postergado')
    estado_visita_3 = db.estados_visitas(db.estados_visitas.nombre == 'Nueva Visita')
    data = db(
        ((db.visitas.estado_visita == estado_visita_1) |
         (db.visitas.estado_visita == estado_visita_2) |
         (db.visitas.estado_visita == estado_visita_3))
    ).select(
        db.tickets.id,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.contactos.contacto_cliente,
        db.tipos_visitas.nombre,
        db.estados_visitas.nombre,
        left=(
            db.tickets.on(db.tickets.id == db.visitas.numero_ticket),
            db.clientes.on(db.clientes.id == db.visitas.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.tickets.marca_gg),
            db.contactos.on(db.contactos.id == db.visitas.contacto_visita),
            db.tipos_visitas.on(db.tipos_visitas.id == db.visitas.tipo_visita),
            db.estados_visitas.on(db.estados_visitas.id == db.visitas.estado_visita),

        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def visitas():
    estado_visita = db.estados_visitas(db.estados_visitas.nombre == 'Programado')
    data = db(
        (db.visitas.estado_visita == estado_visita)
    ).select(
        db.tickets.id,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.contactos.contacto_cliente,
        # db.servicios_productos.servicio_producto,
        db.tipos_visitas.nombre,
        db.visitas.fecha_visita,
        db.visitas.hora_visita,

        # db.clientes_direcciones.direccion_cliente,
        db.distritos.nombre,
        db.personal.persona,
        # db.estados_visitas.nombre,
        left=(
            db.tickets.on(db.tickets.id == db.visitas.numero_ticket),
            db.clientes.on(db.clientes.id == db.visitas.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.tickets.marca_gg),
            db.contactos.on(db.contactos.id == db.visitas.contacto_visita),
            # db.servicios_productos.on(db.servicios_productos.id==db.visitas.servicio_producto),
            db.tipos_visitas.on(db.tipos_visitas.id == db.visitas.tipo_visita),
            db.distritos.on(db.distritos.id == db.visitas.distrito),
            db.personal.on(db.personal.id == db.visitas.personal_visita),
            db.estados_visitas.on(db.visitas.estado_visita == db.estados_visitas.id),
            # db.clientes_direcciones.on(db.clientes_direcciones.direccion_cliente==db.visitas.direccion_cliente),
        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def reportes_tecnicos():
    estado_presupuesto = db.estados_reportes(db.estados_presupuestos.nombre == 'Reporte Tecnico')
    data = db(
        (db.presupuestos.estado_presupuesto == estado_presupuesto)
    ).select(
        db.tickets.id,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.presupuestos.fecha_visitado,
        # db.tipos_visitas.nombre,
        db.personal.persona,
        left=(
            db.tickets.on(db.tickets.id == db.presupuestos.numero_ticket),
            db.clientes.on(db.clientes.id == db.presupuestos.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.tickets.marca_gg),
            # db.visitas.on(db.visitas.numero_ticket==db.tickets.id),
            # db.presupuestos.on(db.presupuestos.numero_ticket == db.tickets.id),
            # db.tipos_visitas.on(db.visitas.tipo_visita==db.tipos_visitas.id),
            db.personal.on(db.personal.id == db.presupuestos.personal_visitado),
            db.estados_presupuestos.on(db.presupuestos.estado_presupuesto == db.estados_presupuestos.id),
        )
    )

    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def presupuestos():
    estado_presupuesto_1 = db.estados_presupuestos(db.estados_presupuestos.nombre == 'Elaboracion')
    estado_presupuesto_2 = db.estados_presupuestos(db.estados_presupuestos.nombre == 'Modificar Presupuesto')
    data = db(
        (db.presupuestos.estado_presupuesto == estado_presupuesto_1) |
        (db.presupuestos.estado_presupuesto == estado_presupuesto_2)
    ).select(
        db.tickets.id,
        db.cotizaciones.numero_cotizacion,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.contactos.contacto_cliente,
        db.servicios_productos.servicio_producto,
        db.cotizaciones.fecha_registro_cotizacion,
        # db.reportes.fecha_envio_reporte,
        db.estados_presupuestos.nombre,
        db.prioridades.nombre,
        # db.estados_negociacion.nombre,
        left=(
            db.tickets.on(db.tickets.id == db.presupuestos.numero_ticket),
            db.cotizaciones.on(db.cotizaciones.id == db.presupuestos.numero_cotizacion),
            db.clientes.on(db.clientes.id == db.presupuestos.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.tickets.marca_gg),
            db.contactos.on(db.contactos.id == db.presupuestos.contacto_cotizacion),
            db.estados_presupuestos.on(db.estados_presupuestos.id == db.presupuestos.estado_presupuesto),
            db.prioridades.on(db.prioridades.id == db.presupuestos.prioridad),
            db.negociaciones.on(db.negociaciones.numero_ticket == db.tickets.id),
            db.estados_negociacion.on(db.estados_negociacion.id == db.negociaciones.estado_negociacion),
            db.reportes.on(db.reportes.numero_ticket == db.tickets.id),
            db.servicios_productos.on(db.servicios_productos.id == db.presupuestos.servicio_producto_cotizacion)
        )
    )
    data = format_table(data)
    return dict(data=data)


####

@auth.requires_login()
def negociacion():
    #    estado_visita_1 = db.estados_visitas(db.estados_visitas.nombre=='Programado')
    #    estado_visita_2 = db.estados_visitas(db.estados_visitas.nombre=='Postergado')
    #    estado_reporte = db.estados_reportes(db.estados_reportes.nombre=='Elaboración')
    estado_negociacion1 = db.estados_negociacion(db.estados_negociacion.nombre == 'Negociacion')
    # estado_negociacion2 = db.estados_negociacion(db.estados_negociacion.nombre=='Nuevo Presupuesto')
    data = db(
        (db.negociaciones.estado_negociacion == estado_negociacion1)
    ).select(
        db.tickets.id,
        db.cotizaciones.numero_cotizacion,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.prioridades.nombre,
        db.negociaciones.descuento,
        db.negociaciones.sub_total_venta,
        db.tiempos_decision.nombre,
        db.intereses.nombre,
        db.negociaciones.fecha_ultima_conversacion,
        left=(
            db.tickets.on(db.tickets.id == db.negociaciones.numero_ticket),
            db.cotizaciones.on(db.cotizaciones.id == db.negociaciones.numero_cotizacion),
            db.clientes.on(db.clientes.id == db.negociaciones.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.tickets.marca_gg),
            db.intereses.on(db.intereses.id == db.negociaciones.interes),
            db.prioridades.on(db.prioridades.id == db.negociaciones.prioridad),
            db.tiempos_decision.on(db.tiempos_decision.id == db.negociaciones.tiempo_decision),
        )
    )
    data = format_table(data)
    return dict(data=data)


def negociacion_avanzada():
    #    estado_visita_1 = db.estados_visitas(db.estados_visitas.nombre=='Programado')
    #    estado_visita_2 = db.estados_visitas(db.estados_visitas.nombre=='Postergado')
    #    estado_reporte = db.estados_reportes(db.estados_reportes.nombre=='Elaboración')
    estado_negociacion1 = db.estados_negociacion(db.estados_negociacion.nombre == 'Negociacion Avanzada')
    # estado_negociacion2 = db.estados_negociacion(db.estados_negociacion.nombre=='Nuevo Presupuesto')
    data = db(
        (db.negociaciones.estado_negociacion == estado_negociacion1)
    ).select(
        db.tickets.id,
        db.cotizaciones.numero_cotizacion,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.prioridades.nombre,
        db.negociaciones.descuento,
        db.negociaciones.sub_total_venta,
        db.tiempos_decision.nombre,
        db.intereses.nombre,
        db.negociaciones.fecha_ultima_conversacion,
        left=(
            db.tickets.on(db.tickets.id == db.negociaciones.numero_ticket),
            db.cotizaciones.on(db.cotizaciones.id == db.negociaciones.numero_cotizacion),
            db.clientes.on(db.clientes.id == db.negociaciones.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.tickets.marca_gg),
            db.prioridades.on(db.prioridades.id == db.negociaciones.prioridad),
            db.intereses.on(db.intereses.id == db.negociaciones.interes),
            db.tiempos_decision.on(db.tiempos_decision.id == db.negociaciones.tiempo_decision),
        )
    )
    data = format_table(data)
    return dict(data=data)


###

@auth.requires_login()
def operaciones():
    estado_operacion_1 = db.estados_operaciones(db.estados_operaciones.nombre == 'Programado')
    estado_operacion_2 = db.estados_operaciones(db.estados_operaciones.nombre == 'Ejecucion')
    estado_operacion_3 = db.estados_operaciones(db.estados_operaciones.nombre == 'Prueba Control')
    estado_operacion_4 = db.estados_operaciones(db.estados_operaciones.nombre == 'Informe')
    estado_operacion_5 = db.estados_operaciones(db.estados_operaciones.nombre == 'Post Servicio')
    estado_operacion_6 = db.estados_operaciones(db.estados_operaciones.nombre == 'Aceptado')
    data = db(
        (db.operaciones.estado_operacion == estado_operacion_1) |
        (db.operaciones.estado_operacion == estado_operacion_2) |
        (db.operaciones.estado_operacion == estado_operacion_3) |
        (db.operaciones.estado_operacion == estado_operacion_4) |
        (db.operaciones.estado_operacion == estado_operacion_5) |
        (db.operaciones.estado_operacion == estado_operacion_6)
    ).select(
        db.tickets.id,
        db.proyectos.numero_proyecto,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.servicios_productos.servicio_producto,
        db.contactos.contacto_cliente,
        #        db.tipos_ejecuciones.nombre,
        #        db.operaciones.fecha_inicio,
        db.personal.persona,
        db.operaciones.avance_estado_proyecto,
        db.estados_operaciones.nombre,
        left=(
            db.tickets.on(db.tickets.id == db.operaciones.numero_ticket),
            db.proyectos.on(db.proyectos.id == db.operaciones.numero_proyecto),
            db.clientes.on(db.clientes.id == db.operaciones.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.tickets.marca_gg),
            db.servicios_productos.on(db.servicios_productos.id == db.operaciones.servicio_producto_cotizacion),
            #            db.cotizaciones.on(db.cotizaciones.id==db.operaciones.numero_cotizacion),
            db.estados_operaciones.on(db.estados_operaciones.id == db.operaciones.estado_operacion),
            #            db.tipos_ejecuciones.on(db.tipos_ejecuciones.id==db.operaciones.tipo_ejecucion),
            db.contactos.on(db.contactos.id == db.operaciones.contacto_proyecto),
            db.personal.on(db.personal.id == db.operaciones.personal_proyecto)
        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def post_venta():
    estado_post_venta = db.estados_post_venta(db.estados_post_venta.nombre == 'Terminado')
    data = db(
        (db.post_venta.estado_post_venta == estado_post_venta)
    ).select(
        db.tickets.id,
        db.proyectos.numero_proyecto,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.servicios_productos.servicio_producto,
        db.contactos.contacto_cliente,
        left=(
            db.tickets.on(db.tickets.id == db.post_venta.numero_ticket),
            db.proyectos.on(db.proyectos.id == db.post_venta.numero_proyecto),
            db.clientes.on(db.clientes.id == db.post_venta.cliente),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.post_venta.marca_gg),
            db.contactos.on(db.contactos.id == db.post_venta.contacto_proyecto),
            db.servicios_productos.on(db.servicios_productos.id == db.post_venta.servicio_producto_cotizacion),
        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def facturacion():
    estado_facturacion = db.estados_facturacion(db.estados_facturacion.nombre == 'Elaboracion')
    data = db(
        (db.facturacion.estado_facturacion == estado_facturacion)
    ).select(
        db.tickets.id,
        db.facturacion.numero_comprobante,
        db.tipos_comprobantes.nombre,
        db.cotizaciones.numero_cotizacion,
        db.clientes.cliente,
        db.tickets.referencia,
        db.marcas_definiciones.marca,
        db.contactos.contacto_cliente,
        db.facturacion.detraccion,
        left=(
            db.tickets.on(db.tickets.id == db.facturacion.numero_ticket),
            db.clientes.on(db.clientes.id == db.facturacion.cliente),
            db.tipos_comprobantes.on(db.tipos_comprobantes.id == db.facturacion.tipo_comprobante),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.tickets.marca_gg),
            db.cotizaciones.on(db.cotizaciones.id == db.facturacion.numero_cotizacion),
            db.contactos.on(db.contactos.id == db.facturacion.contacto_facturacion),
        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def cobranzas_tiempo():
    estado_cuenta_cobrar = db.estados_cuentas_cobrar(db.estados_cuentas_cobrar.nombre == 'Pendiente Saldo Normal')
    # estado_vencimiento = db.estados_vencimientos(db.estados_vencimientos.nombre=='Normal')
    data = db(
        (db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_cuenta_cobrar)
    ).select(
        db.tickets.id,
        db.facturacion.numero_comprobante,
        db.clientes.cliente,
        db.tickets.referencia,
        #        db.razones_zociales_gg.razon_social_gg,
        db.marcas_definiciones.marca,
        db.contactos.contacto_cliente,
        db.cuentas_por_cobrar.saldo_por_cobrar,
        db.cuentas_por_cobrar.fecha_cobranza,
        left=(
            db.tickets.on(db.tickets.id == db.cuentas_por_cobrar.numero_ticket),
            db.facturacion.on(db.facturacion.id == db.cuentas_por_cobrar.numero_comprobante),
            db.clientes.on(db.clientes.id == db.cuentas_por_cobrar.cliente),
            #            db.razones_zociales_gg.on(db.razones_zociales_gg.id==db.cuentas_por_cobrar.razon_social_gg),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.cuentas_por_cobrar.marca_gg),
            db.contactos.on(db.contactos.id == db.cuentas_por_cobrar.contacto_cobranzas),
        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def cobranzas_vencidas():
    estado_cuenta_cobrar = db.estados_cuentas_cobrar(db.estados_cuentas_cobrar.nombre == 'Pendiente Saldo Vencido')
    data = db(
        (db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_cuenta_cobrar)
    ).select(
        db.tickets.id,
        db.facturacion.numero_comprobante,
        db.clientes.cliente,
        db.tickets.referencia,
        #        db.razones_zociales_gg.razon_social_gg,
        db.marcas_definiciones.marca,
        db.contactos.contacto_cliente,
        db.cuentas_por_cobrar.saldo_por_cobrar,
        db.cuentas_por_cobrar.fecha_cobranza,
        left=(
            db.tickets.on(db.tickets.id == db.cuentas_por_cobrar.numero_ticket),
            db.facturacion.on(db.facturacion.id == db.cuentas_por_cobrar.numero_comprobante),
            db.clientes.on(db.clientes.id == db.cuentas_por_cobrar.cliente),
            #            db.razones_zociales_gg.on(db.razones_zociales_gg.id==db.cuentas_por_cobrar.razon_social_gg),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.cuentas_por_cobrar.marca_gg),
            db.contactos.on(db.contactos.id == db.cuentas_por_cobrar.contacto_cobranzas),
        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def cuentas_pagar():
    estado_cuenta_pagar = db.estados_cuentas_pagar(db.estados_cuentas_pagar.nombre == 'Pendiente Saldo Normal')
    data = db(
        (db.cuentas_por_pagar.estado_cuenta_pagar == estado_cuenta_pagar)
    ).select(
        db.gastos_generales.numero_ticket_gasto,
        db.marcas_definiciones.marca,
        db.razones_zociales_gg.razon_social_gg,
        db.cuentas_por_pagar.monto_detraccion,
        db.cuentas_por_pagar.monto_saldo_por_pagar,
        db.cuentas_por_pagar.fecha_de_pago,
        left=(
            db.gastos_generales.on(db.gastos_generales.id == db.cuentas_por_pagar.numero_ticket_gasto),
            db.razones_zociales_gg.on(db.razones_zociales_gg.id == db.cuentas_por_pagar.razon_social_gg),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.cuentas_por_pagar.marca_gg),
        )
    )
    data = format_table(data)
    return dict(data=data)


def cuentas_pagar_vencido():
    estado_cuenta_pagar = db.estados_cuentas_pagar(db.estados_cuentas_pagar.nombre == 'Pendiente Saldo Vencido')
    data = db(
        (db.cuentas_por_pagar.estado_cuenta_pagar == estado_cuenta_pagar)
    ).select(
        db.gastos_generales.numero_ticket_gasto,
        db.marcas_definiciones.marca,
        db.razones_zociales_gg.razon_social_gg,
        db.cuentas_por_pagar.monto_detraccion,
        db.cuentas_por_pagar.monto_saldo_por_pagar,
        db.cuentas_por_pagar.fecha_de_pago,
        left=(
            db.gastos_generales.on(db.gastos_generales.id == db.cuentas_por_pagar.numero_ticket_gasto),
            db.razones_zociales_gg.on(db.razones_zociales_gg.id == db.cuentas_por_pagar.razon_social_gg),
            db.marcas_definiciones.on(db.marcas_definiciones.id == db.cuentas_por_pagar.marca_gg),
        )
    )

    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def cheques_cobrar():
    estado_cheque = db.estados_cheques(db.estados_cheques.nombre == 'Pendiente Cobro')
    data = db(
        (db.movimientos_bancos.estado_cheque == estado_cheque)
    ).select(
        db.movimientos_bancos.numero_cheque,
        db.movimientos_bancos.detalle_movimiento,
        db.movimientos_bancos.egreso,
        #        left=(
        #            db.marcas.on(db.marcas.id==db.cuentas_pagar.marca),
        #            db.proveedores.on(db.proveedores.id==db.cuentas_pagar.proveedor),
        #            db.personal.on(db.personal.id==db.cuentas_pagar.persona),
        #        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def aprobaciones():
    # estado_cheque = db.estados_cheques(db.estados_cheques.nombre=='Pendiente Cobro')
    data = db(
        db.facturacion.id > 0
        # (db.movimientos_bancos.estado_cheque == estado_cheque)
    ).select(
        db.facturacion.sub_total_venta,
        db.facturacion.impuesto_igv,
        db.facturacion.total_venta
        #        left=(
        #            db.marcas.on(db.marcas.id==db.cuentas_pagar.marca),
        #            db.proveedores.on(db.proveedores.id==db.cuentas_pagar.proveedor),
        #            db.personal.on(db.personal.id==db.cuentas_pagar.persona),
        #        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def cancelaciones():
    # estado_cheque = db.estados_cheques(db.estados_cheques.nombre=='Pendiente Cobro')
    data = db(
        db.facturacion.id > 0
        # (db.movimientos_bancos.estado_cheque == estado_cheque)
    ).select(
        db.facturacion.sub_total_venta,
        db.facturacion.impuesto_igv,
        db.facturacion.total_venta
        #        left=(
        #            db.marcas.on(db.marcas.id==db.cuentas_pagar.marca),
        #            db.proveedores.on(db.proveedores.id==db.cuentas_pagar.proveedor),
        #            db.personal.on(db.personal.id==db.cuentas_pagar.persona),
        #        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def gastos_generales():
    # estado_cheque = db.estados_cheques(db.estados_cheques.nombre=='Pendiente Cobro')
    data = db(
        db.facturacion.id > 0
        # (db.movimientos_bancos.estado_cheque == estado_cheque)
    ).select(
        db.facturacion.sub_total_venta,
        db.facturacion.impuesto_igv,
        db.facturacion.total_venta
        #        left=(
        #            db.marcas.on(db.marcas.id==db.cuentas_pagar.marca),
        #            db.proveedores.on(db.proveedores.id==db.cuentas_pagar.proveedor),
        #            db.personal.on(db.personal.id==db.cuentas_pagar.persona),
        #        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def inversiones():
    # estado_cheque = db.estados_cheques(db.estados_cheques.nombre=='Pendiente Cobro')
    data = db(
        db.facturacion.id > 0
        # (db.movimientos_bancos.estado_cheque == estado_cheque)
    ).select(
        db.facturacion.sub_total_venta,
        db.facturacion.impuesto_igv,
        db.facturacion.total_venta
        #        left=(
        #            db.marcas.on(db.marcas.id==db.cuentas_pagar.marca),
        #            db.proveedores.on(db.proveedores.id==db.cuentas_pagar.proveedor),
        #            db.personal.on(db.personal.id==db.cuentas_pagar.persona),
        #        )
    )
    data = format_table(data)
    return dict(data=data)


@auth.requires_login()
def saldos_cuentas_bancos():
    form = SQLFORM.factory(
        Field('cuenta_bancaria', 'reference cuentas_bancarias',
              requires=IS_IN_DB(db, db.cuentas_bancarias, '%(banco)s - %(numero_cuenta)s')),
    )
    monto_ingresos = 0
    monto_egresos = 0
    if form.process().accepted:
        egreso = db.tipos_transacciones(db.tipos_transacciones.nombre == 'Egreso')
        ingreso = db.tipos_transacciones(db.tipos_transacciones.nombre == 'Ingreso')
        cuenta = form.vars.cuenta_bancaria
        # sum = db.log.severity.sum()
        egresos = db((db.transacciones.numero_cuenta == cuenta) & (db.transacciones.tipo_transaccion == egreso)).select(
            db.transacciones.monto_transaccion.sum()
        )
        ingresos = db(
            (db.transacciones.numero_cuenta == cuenta) & (db.transacciones.tipo_transaccion == ingreso)).select(
            db.transacciones.monto_transaccion.sum()
        )
        # data = format_table(data)
        monto_egresos = egresos[0]['_extra']['SUM(transacciones.monto_transaccion)']
        monto_ingresos = ingresos[0]['_extra']['SUM(transacciones.monto_transaccion)']
        if monto_egresos is None:
            monto_egresos = 0
        if monto_ingresos is None:
            monto_ingresos = 0
        monto_egresos = float(monto_egresos)
        monto_ingresos = float(monto_ingresos)
    return dict(form=form, egresos=monto_egresos, ingresos=monto_ingresos)


@auth.requires_login()
def aprobaciones_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        estado_negociacion = db.estados_negociacion(db.estados_negociacion.nombre == 'Aprobado')
        datos = db((db.marcas_definiciones.razon_zocial_gg == marca) &
                   ((db.negociaciones.fecha_aprobacion >= desde) & (db.negociaciones.fecha_aprobacion <= hasta))
                   & (db.negociaciones.estado_negociacion == estado_negociacion)
                   ).select(
            db.marcas_definiciones.marca,
            db.negociaciones.sub_total_venta,
            left=(
                db.marcas_definiciones.on(db.marcas_definiciones.id == db.negociaciones.marca_gg),
            )
        )
        #print datos
        registros = {}
        total_sum = 0
        for element in datos:
            registros.setdefault(element['marcas_definiciones.marca'], 0)
            registros[element['marcas_definiciones.marca']] += element['negociaciones.sub_total_venta']
            total_sum += element['negociaciones.sub_total_venta']
    try:
        data = format_table_aux(registros, total_sum)
    except:
        pass

    return dict(form=form, data=data)


@auth.requires_login()
def cancelaciones_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        estado_negociacion = db.estados_negociacion(db.estados_negociacion.nombre == 'Cancelado')
        # print estado_negociacion
        datos = db((db.marcas_definiciones.razon_zocial_gg == marca) &
                   ((db.negociaciones.fecha_cancelacion >= desde) & (db.negociaciones.fecha_cancelacion <= hasta))
                   & (db.negociaciones.estado_negociacion == estado_negociacion)
                   ).select(
            db.marcas_definiciones.marca,
            db.negociaciones.sub_total_venta,
            left=(
                db.marcas_definiciones.on(db.marcas_definiciones.id == db.negociaciones.marca_gg),
            )
        )
        print datos
        registros = {}
        total_sum = 0
        for element in datos:
            registros.setdefault(element['marcas_definiciones.marca'], 0)
            registros[element['marcas_definiciones.marca']] += element['negociaciones.sub_total_venta']
            total_sum += element['negociaciones.sub_total_venta']
    try:
        data = format_table_aux(registros, total_sum)
    except:
        pass
    return dict(form=form, data=data)


@auth.requires_login()
def ventas_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        datos = db((db.facturacion.razon_social_gg == marca) & (
        (db.facturacion.fecha_facturacion >= desde) & (db.facturacion.fecha_facturacion <= hasta))).select(
            db.marcas_definiciones.marca,
            # db.facturacion.sub_total_venta,
            # db.facturacion.impuesto_igv,
            db.tipos_comprobantes.nombre,
            db.facturacion.total_venta,
            left=(
                #db.negociaciones.on((db.negociaciones.numero_ticket == db.facturacion.numero_ticket)),
                db.marcas_definiciones.on(db.marcas_definiciones.id == db.facturacion.marca_gg),
                db.tipos_comprobantes.on(db.tipos_comprobantes.id==db.facturacion.tipo_comprobante)
            )
        )
        registros = {}
        total_sum = 0
        for element in datos:
            keyw = '{} - {}'.format(element['marcas_definiciones.marca'], element['tipos_comprobantes.nombre'])
            registros.setdefault(keyw, 0)
            amount = element['facturacion.total_venta']
            if amount is None:
                amount = 0
            registros[keyw] += amount
            total_sum += amount
    try:
        data = format_table_aux(registros, total_sum)
    except:
        pass
    # data = format_table(data)
    return dict(form=form, data=data)


@auth.requires_login()
def cuentas_cobrar_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data_cobrar = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        estado_1 = db.estados_cuentas_cobrar(db.estados_cuentas_cobrar.nombre == 'Pendiente Saldo Normal')
        estado_2 = db.estados_cuentas_cobrar(db.estados_cuentas_cobrar.nombre == 'Pendiente Saldo Vencido')

        datos = db(((db.facturacion.razon_social_gg == marca) & ((db.facturacion.fecha_facturacion >= desde) &
                                                                 (db.facturacion.fecha_facturacion <= hasta)) & (
                    (db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_1) | (
                    db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_2)))).select(
            db.marcas_definiciones.marca,
            db.cuentas_por_cobrar.saldo_por_cobrar,
            left=(
                db.cuentas_por_cobrar.on((db.cuentas_por_cobrar.numero_ticket == db.facturacion.numero_ticket) & (
                (db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_1) | (
                db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_2))),
                db.marcas_definiciones.on(db.marcas_definiciones.id == db.facturacion.marca_gg),
            )
        )

        registros = {}
        total_sum = 0
        for element in datos:
            if element not in registros:
                registros.setdefault(element['marcas_definiciones.marca'], 0)
                registros[element['marcas_definiciones.marca']] += float(element['cuentas_por_cobrar.saldo_por_cobrar'])
                total_sum += float(element['cuentas_por_cobrar.saldo_por_cobrar'])

    try:
        data_cobrar = format_table_aux(registros, total_sum)
    except:
        pass
    return dict(form=form, data=data_cobrar)


@auth.requires_login()
def cuentas_pagar_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        estado_1 = db.estados_cuentas_pagar(db.estados_cuentas_pagar.nombre == 'Pendiente Saldo Normal')
        estado_2 = db.estados_cuentas_pagar(db.estados_cuentas_pagar.nombre == 'Pendiente Saldo Vencido')
        datos = db(((db.facturacion.razon_social_gg == marca) & ((db.facturacion.fecha_facturacion >= desde) &
                                                                 (db.facturacion.fecha_facturacion <= hasta)) & (
                        (db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_1) | (
                            db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_2)))).select(
            db.marcas_definiciones.marca,
            db.cuentas_por_pagar.monto_saldo_por_pagar,
            left=(
                db.cuentas_por_pagar.on((db.cuentas_por_pagar.numero_ticket_gasto == db.facturacion.numero_ticket) & (
                    (db.cuentas_por_pagar.estado_cuenta_pagar == estado_1) | (
                        db.cuentas_por_pagar.estado_cuenta_pagar == estado_2))),
                db.marcas_definiciones.on(db.marcas_definiciones.id == db.facturacion.marca_gg),
            )
        )

        registros = {}
        total_sum = 0
        for element in datos:
            if element not in registros:
                if element['cuentas_por_pagar.monto_saldo_por_pagar'] is not None:
                    registros.setdefault(element['marcas_definiciones.marca'], 0)
                    registros[element['marcas_definiciones.marca']] += float(
                        element['cuentas_por_pagar.monto_saldo_por_pagar'])
                    total_sum += float(element['cuentas_por_pagar.monto_saldo_por_pagar'])

    try:
        data = format_table_aux(registros, total_sum)
    except:
        pass
    return dict(form=form, data=data)


@auth.requires_login()
def gastos_generales_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('tipo_centro_costo', 'reference tipos_centros_costos',
              requires=IS_IN_DB(db, db.tipos_centros_costos, '%(nombre)s', zero='---- Elegir Opcion ----')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        centro_costo = form.vars.tipo_centro_costo
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        datos = db((db.gastos_generales.razon_social_gg == marca) &
                   ((db.gastos_generales.fecha_ticket_gasto >= desde) & (
                   db.gastos_generales.fecha_ticket_gasto <= hasta)) &
                   (db.gastos_generales.tipo_centro_costo == centro_costo)
                   ).select(
            db.marcas_definiciones.marca,
            # db.cuentas_por_cobrar.saldo_por_cobrar,
            # db.gastos_generales.monto_sub_total,
            # db.gastos_generales.monto_impuesto,
            db.gastos_generales.monto_total_gasto,
            left=(
                db.marcas_definiciones.on((db.marcas_definiciones.id == db.gastos_generales.marca_gg)),
            )
        )
        registros = {}
        total_sum = 0
        for element in datos:
            if element not in registros:
                if element['gastos_generales.monto_total_gasto'] is not None:
                    registros.setdefault(element['marcas_definiciones.marca'], 0)
                    registros[element['marcas_definiciones.marca']] += float(
                        element['gastos_generales.monto_total_gasto'])
                    total_sum += float(element['gastos_generales.monto_total_gasto'])
    # data = format_table(data)
    try:
        data = format_table_aux(registros, total_sum)
    except:
        pass
    return dict(form=form, data=data)


@auth.requires_login()
def inversiones_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('tipo_inversion', 'reference tipos_inversiones',
              requires=IS_IN_DB(db, db.tipos_inversiones, '%(nombre)s', zero='---- Elegir Opcion ----')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        tipo_inversion = form.vars.tipo_inversion
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        datos = db((db.inversiones.razon_social_gg == marca) &
                   ((db.inversiones.fecha_ticket_inversion >= desde) & (
                   db.inversiones.fecha_ticket_inversion <= hasta)) &
                   (db.inversiones.tipo_inversion == tipo_inversion)
                   ).select(
            db.marcas_definiciones.marca,
            # db.cuentas_por_cobrar.saldo_por_cobrar,
            # db.gastos_generales.monto_sub_total,
            # db.gastos_generales.monto_impuesto,
            db.inversiones.monto_inversion,
            left=(
                db.marcas_definiciones.on((db.marcas_definiciones.id == db.inversiones.marca_gg)),
            )
        )
        registros = {}
        total_sum = 0
        for element in datos:
            if element not in registros:
                if element['inversiones.monto_inversion'] is not None:
                    registros.setdefault(element['marcas_definiciones.marca'], 0)
                    registros[element['marcas_definiciones.marca']] += float(
                        element['inversiones.monto_inversion'])
                    total_sum += float(element['inversiones.monto_inversion'])
                    # data = format_table(data)
    try:
        data = format_table_aux(registros, total_sum)
    except:
        pass
    return dict(form=form, data=data)


@auth.requires_login()
def remuneraciones_personal_fechas():
    form = SQLFORM.factory(
        Field('persona', 'reference personal', requires=IS_IN_DB(db, db.personal, '%(persona)s')),
        #        Field('razon_social_gg', 'reference razones_zociales_gg', requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    new_data = []
    if form.process().accepted:
        persona = form.vars.persona
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        data = db((db.tareos_personal.persona == persona) &
                  ((db.tareos_personal.fecha_dia_trabajo >= desde) & (db.tareos_personal.fecha_dia_trabajo <= hasta))
                  ).select(
            db.tareos_personal.remuneracion,
        )
        new_data = 0
        for elem in data:
            new_data += float(elem['remuneracion'])
    # data = format_table(data)
    return dict(form=form, data=new_data)


# ============================================================================

@auth.requires_login()
def cobranzas_a_tiempo_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        estado_cuenta_cobrar = db.estados_cuentas_cobrar(db.estados_cuentas_cobrar.nombre == 'Pendiente Saldo Normal')
        datos = db((db.cuentas_por_cobrar.razon_social_gg == marca) &
                   ((db.cuentas_por_cobrar.fecha_cobranza >= desde) & (db.cuentas_por_cobrar.fecha_cobranza <= hasta))
                   & (db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_cuenta_cobrar)
                   ).select(
            db.marcas_definiciones.marca,
            # db.facturacion.sub_total_venta,
            # db.facturacion.impuesto_igv,
            db.cuentas_por_cobrar.saldo_por_cobrar,
            left=(

                db.marcas_definiciones.on(db.marcas_definiciones.id == db.cuentas_por_cobrar.marca_gg),

            )
        )
        registros = {}
        total_sum = 0
        for element in datos:
            if element['cuentas_por_cobrar.saldo_por_cobrar'] is not None:
                registros.setdefault(element['marcas_definiciones.marca'], 0)
                registros[element['marcas_definiciones.marca']] += float(element['cuentas_por_cobrar.saldo_por_cobrar'])
                total_sum += float(element['cuentas_por_cobrar.saldo_por_cobrar'])
    try:
        data = format_table_aux(registros, total_sum)
    except:
        pass

    return dict(form=form, data=data)


@auth.requires_login()
def cobranzas_vencidas_fechas():
    form = SQLFORM.factory(
        Field('razon_social_gg', 'reference razones_zociales_gg',
              requires=IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s')),
        Field('desde', 'date'),
        Field('hasta', 'date'),
    )
    data = []
    if form.process().accepted:
        marca = form.vars.razon_social_gg
        desde = form.vars.desde
        hasta = form.vars.hasta
        if desde is None or hasta is None:
            return
        estado_cuenta_cobrar = db.estados_cuentas_cobrar(db.estados_cuentas_cobrar.nombre == 'Pendiente Saldo Vencido')
        datos = db((db.cuentas_por_cobrar.razon_social_gg == marca) &
                   ((db.cuentas_por_cobrar.fecha_cobranza >= desde) & (db.cuentas_por_cobrar.fecha_cobranza <= hasta))
                   & (db.cuentas_por_cobrar.estado_cuenta_cobrar == estado_cuenta_cobrar)
                   ).select(
            db.marcas_definiciones.marca,
            # db.facturacion.sub_total_venta,
            # db.facturacion.impuesto_igv,
            db.cuentas_por_cobrar.saldo_por_cobrar,
            left=(

                db.marcas_definiciones.on(db.marcas_definiciones.id == db.cuentas_por_cobrar.marca_gg),

            )
        )
        registros = {}
        total_sum = 0
        for element in datos:
            if element['cuentas_por_cobrar.saldo_por_cobrar'] is not None:
                registros.setdefault(element['marcas_definiciones.marca'], 0)
                registros[element['marcas_definiciones.marca']] += float(element['cuentas_por_cobrar.saldo_por_cobrar'])
                total_sum += float(element['cuentas_por_cobrar.saldo_por_cobrar'])
    try:
        data = format_table_aux(registros, total_sum)
    except:
        pass

    return dict(form=form, data=data)
