# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def prestamo_bancario():
    response.view = 'investments/default.html'
    gasto = 'Préstamo Bancario'
    item_id = db.tipos_inversiones(db.tipos_inversiones.nombre==gasto)['id']
    db.inversiones.tipo_inversion.default = item_id
    db.inversiones.tipo_inversion.writable = False
    form = SQLFORM.grid(db.inversiones, csv=False, orderby=~db.inversiones.id)
    return dict(form=form,page_name="Préstamo Bancario")

def prestamo_socios():
    response.view = 'investments/default.html'
    gasto = 'Préstamo Socios'
    item_id = db.tipos_inversiones(db.tipos_inversiones.nombre==gasto)['id']
    db.inversiones.tipo_inversion.default = item_id
    db.inversiones.tipo_inversion.writable = False
    form = SQLFORM.grid(db.inversiones, csv=False, orderby=~db.inversiones.id)
    return dict(form=form,page_name="Préstamos Socios")

def capital():
    response.view = 'investments/default.html'
    gasto = 'Capital'
    item_id = db.tipos_inversiones(db.tipos_inversiones.nombre==gasto)['id']
    db.inversiones.tipo_inversion.default = item_id
    db.inversiones.tipo_inversion.writable = False
    form = SQLFORM.grid(db.inversiones, csv=False, orderby=~db.inversiones.id)
    return dict(form=form,page_name="Capitál")