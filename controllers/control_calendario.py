# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())


def download(): return response.download(request, db)


def call(): return service()


### end requires
def index():
    return dict()


def error():
    return dict()


def reporte_diario():
    date_start = now.month
    new_data = []
    data = db(db.reporte_diario.fecha_calendario.month() == date_start).select(db.reporte_diario.id,
                                                                             db.reporte_diario.reporte,
                                                                             db.reporte_diario.fecha_calendario
                                                                             )
    count = 0
    elems = {}
    for row in data:
        fecha_calendario = row.fecha_calendario.isoformat()
        elems.setdefault(fecha_calendario, 0)
        elems[fecha_calendario] += 1
    for row in elems:
        count += 1
        nrow = {'id': count, 'title': '{}'.format(elems[row]),
            'start': row}
        new_data.append(nrow)
    response.view = 'control_calendario/default.html'
    form = SQLFORM.grid(db.reporte_diario, csv=False, orderby=~db.reporte_diario.id)
    return dict(form=form, page_name='REPORTE DIARIO', events=new_data, table='reporte_diario')


def requerimientos():
    date_start = now.month
    new_data = []
    data = db(db.requerimientos.fecha_calendario.month() == date_start).select(db.requerimientos.id,
                                                                             db.requerimientos.referencia,
                                                                             db.requerimientos.fecha_calendario
                                                                             )
    count = 0
    elems = {}
    for row in data:
        fecha_calendario = row.fecha_calendario.isoformat()
        elems.setdefault(fecha_calendario, 0)
        elems[fecha_calendario] += 1
    for row in elems:
        count += 1
        nrow = {'id': count, 'title': '{}'.format(elems[row]),
            'start': row}
        new_data.append(nrow)
    response.view = 'control_calendario/default.html'
    form = SQLFORM.grid(db.requerimientos, csv=False,maxtextlength=200, formstyle='bootstrap',orderby=~db.requerimientos.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Requerimientos', events=new_data, table='requerimientos')


def bitacora():
    date_start = now.month
    new_data = []
    data = db(db.bitacora.fecha_calendario.month() == date_start).select(db.bitacora.id,
                                                                             db.bitacora.detalle_apunte,
                                                                             db.bitacora.fecha_calendario
                                                                             )
    count = 0
    elems = {}
    for row in data:
        fecha_calendario = row.fecha_calendario.isoformat()
        elems.setdefault(fecha_calendario, 0)
        elems[fecha_calendario] += 1
    for row in elems:
        count += 1
        nrow = {'id': count, 'title': '{}'.format(elems[row]),
            'start': row}
        new_data.append(nrow)
    response.view = 'control_calendario/default.html'
    form = SQLFORM.grid(db.bitacora, csv=False, orderby=~db.bitacora.id)
    return dict(form=form, page_name='Bitacora', events=new_data, table='bitacora')


def tareas():
    date_start = now.month
    new_data = []
    data = db(db.tareas.fecha_calendario.month() == date_start).select(db.tareas.id,
                                                                             db.tareas.tarea,
                                                                             db.tareas.fecha_calendario
                                                                             )
    count = 0
    elems = {}
    for row in data:
        fecha_calendario = row.fecha_calendario.isoformat()
        elems.setdefault(fecha_calendario, 0)
        elems[fecha_calendario] += 1
    for row in elems:
        count += 1
        nrow = {'id': count, 'title': '{}'.format(elems[row]),
            'start': row}
        new_data.append(nrow)
    response.view = 'control_calendario/default.html'
    form = SQLFORM.grid(db.tareas, maxtextlength=200, formstyle='bootstrap',csv=False, orderby=~db.tareas.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Tareas', events=new_data, table='tareas')


def cotizaciones_enviadas():
    date_start = now.month
    new_data = []
    data = db(db.cotizaciones_enviadas.fecha_calendario.month() == date_start).select(db.cotizaciones_enviadas.id,
                                                                             db.cotizaciones_enviadas.referencia,
                                                                             db.cotizaciones_enviadas.fecha_calendario
                                                                             )
    count = 0
    elems = {}
    for row in data:
        fecha_calendario = row.fecha_calendario.isoformat()
        elems.setdefault(fecha_calendario, 0)
        elems[fecha_calendario] += 1
    for row in elems:
        count += 1
        nrow = {'id': count, 'title': '{}'.format(elems[row]),
            'start': row}
        new_data.append(nrow)
    response.view = 'control_calendario/default.html'
    form = SQLFORM.grid(db.cotizaciones_enviadas, csv=False,maxtextlength=200, formstyle='bootstrap', orderby=~db.cotizaciones_enviadas.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Cotizaciones Enviadas', events=new_data, table='cotizaciones_enviadas')


def cotizaciones_aprobadas():
    date_start = now.month
    new_data = []
    data = db(db.cotizaciones_aprobadas.fecha_calendario.month() == date_start).select(db.cotizaciones_aprobadas.id,
                                                                             db.cotizaciones_aprobadas.referencia,
                                                                             db.cotizaciones_aprobadas.fecha_calendario
                                                                             )
    count = 0
    elems = {}
    for row in data:
        fecha_calendario = row.fecha_calendario.isoformat()
        elems.setdefault(fecha_calendario, 0)
        elems[fecha_calendario] += 1
    for row in elems:
        count += 1
        nrow = {'id': count, 'title': '{}'.format(elems[row]),
            'start': row}
        new_data.append(nrow)
    response.view = 'control_calendario/default.html'
    form = SQLFORM.grid(db.cotizaciones_aprobadas, csv=False,maxtextlength=200, formstyle='bootstrap', orderby=~db.cotizaciones_aprobadas.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Cotizaciones Aprobadas', events=new_data, table='cotizaciones_aprobadas')


def personal_operaciones():
    date_start = now.month
    new_data = []
    data = db(db.personal_operaciones.fecha_calendario.month() == date_start).select(db.personal_operaciones.id,
                                                                             db.personal_operaciones.nota,
                                                                             db.personal_operaciones.fecha_calendario
                                                                             )
    count = 0
    elems = {}
    for row in data:
        fecha_calendario = row.fecha_calendario.isoformat()
        elems.setdefault(fecha_calendario, 0)
        elems[fecha_calendario] += 1
    for row in elems:
        count += 1
        nrow = {'id': count, 'title': '{}'.format(elems[row]),
            'start': row}
        new_data.append(nrow)
    response.view = 'control_calendario/default.html'
    form = SQLFORM.grid(db.personal_operaciones, csv=False, orderby=~db.personal_operaciones.id)
    return dict(form=form, page_name='Personal Operaciones', events=new_data, table='personal_operaciones')


def proveedor_operaciones():
    date_start = now.month
    new_data = []
    data = db(db.proveedor_operaciones.fecha_calendario.month() == date_start).select(db.proveedor_operaciones.id,
                                                                             db.proveedor_operaciones.nota,
                                                                             db.proveedor_operaciones.fecha_calendario
                                                                             )
    count = 0
    elems = {}
    for row in data:
        fecha_calendario = row.fecha_calendario.isoformat()
        elems.setdefault(fecha_calendario, 0)
        elems[fecha_calendario] += 1
    for row in elems:
        count += 1
        nrow = {'id': count, 'title': '{}'.format(elems[row]),
            'start': row}
        new_data.append(nrow)
    response.view = 'control_calendario/default.html'
    form = SQLFORM.grid(db.personal_operaciones, csv=False, orderby=~db.personal_operaciones.id)
    return dict(form=form, page_name='Proveedor Operaciones', events=new_data, table='proveedor_operaciones')


def auxiliar():
    table = request.vars.table
    fecha = request.vars.fecha
    response.view = 'control_calendario/auxiliar.html'
    db[table].fecha_control = fecha
    query = db[table].fecha_calendario == fecha
    form = SQLFORM.grid(query)
    #if form.process().accepted:
    #    response.flash = 'form accepted'
    #    redirect(URL('{}'.format(table)))
    return dict(form=form, page_name=table.upper())
