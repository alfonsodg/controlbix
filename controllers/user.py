# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires_login()
def manage():
    name = 'Administrar Usuarios'
    response.view = 'user/default.html'
    form = SQLFORM.grid(db.auth_user)
    return dict(form=form, page_name=name)

@auth.requires_login()
def groups():
    name = 'Grupos de Usuarios'
    response.view = 'user/default.html'
    form = SQLFORM.grid(db.auth_group)
    return dict(form=form, page_name=name)

@auth.requires_login()
def memberships():
    name = 'Membresías de Usuarios'
    response.view = 'user/default.html'
    form = SQLFORM.grid(db.auth_membership)
    return dict(form=form, page_name=name)
