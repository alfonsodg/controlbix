# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()


# form_grid = SQLFORM.grid(query,
#        create=False, deletable=False, editable=False,
#        fields=[db.events.register_time,
#                db.events.customer, db.events.sms_campaign,
#                db.events.audience, db.events.start_date,
#                db.events.start_time
#                ]
#        )

def clientes():
    response.view = 'customers/default.html'
    form = SQLFORM.smartgrid(db.clientes,maxtextlength=200, formstyle='bootstrap', linked_tables=[
        'contactos',
        'clientes_direcciones',
        'comercial_cliente',
        'visitas',
#        'reportes',
        'presupuestos',
        'negociaciones',
        'operaciones',
        'post_venta'
    ],
     # fields=[
     #         db.clientes.id,
     #         db.clientes.fecha_registro,
     #         db.clientes.cliente,
     #         db.clientes.tipo_cliente,
     #         db.clientes.razon_social_nombre_completo,
     #         db.clientes.ruc_dni_ce,
     #         db.clientes.fecha_aniversario,
     #         db.clientes.area_negocio,
     #         db.clientes.estado_cliente,
     #         db.clientes.medio_captacion,
     #         db.clientes.telefono_contacto,
     #         db.clientes.email_contacto,
     #         db.clientes.url,
     #         db.clientes.nota,
     #         ],

    csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Clientes')

def contactos():
    response.view = 'customers/default.html'
    form = SQLFORM.grid(db.contactos, maxtextlength=200, formstyle='bootstrap',csv=False, orderby=~db.contactos.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Contactos')

def resultados():
    response.view = 'customers/default.html'
    form = SQLFORM.smartgrid(db.resultados,linked_tables=[
        'registros_ventas',
        'registros_cancelaciones'
    ], csv=False)
    return dict(form=form, page_name='Resultados')

def clientes_direcciones():
    response.view = 'customers/default.html'
    form = SQLFORM.grid(db.clientes_direcciones, maxtextlength=200, formstyle='bootstrap',csv=False, orderby=~db.clientes_direcciones.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Direcciones Cliente')

def cotizaciones():
    response.view = 'customers/default.html'
    form = SQLFORM.smartgrid(db.cotizaciones,maxtextlength=200, formstyle='bootstrap',linked_tables=[
        'propuesta_tecnica',
        'propuesta_economica'
    ],
     fields=[
         db.cotizaciones.id,
         db.cotizaciones.fecha_registro,
         db.cotizaciones.fecha_registro_cotizacion,
         db.cotizaciones.numero_ticket,
         db.cotizaciones.referencia,
         db.cotizaciones.numero_cotizacion,
         db.cotizaciones.reporte_adjunto,
         db.cotizaciones.cliente,
         db.cotizaciones.marca_gg,
         db.cotizaciones.contacto_cotizacion,
         #db.cotizaciones.fecha_cotizacion,
         db.cotizaciones.servicio_producto_cotizacion,
         db.cotizaciones.tipo_trabajo,
         db.cotizaciones.tipo_horario,
         db.cotizaciones.tipo_cotizacion,
         db.cotizaciones.plazo_ejecucion_promedio,
         db.cotizaciones.moneda,
         db.cotizaciones.tipo_comprobante,
         db.cotizaciones.alcances_servicio,
         db.cotizaciones.aplica_adicionales,
         db.cotizaciones.aplica_penalidades,
         db.cotizaciones.plazo_pago,
         db.cotizaciones.calidad_acabado,
         db.cotizaciones.calidad_material,
         db.cotizaciones.calidad_limpieza,
         db.cotizaciones.disposicion_desmonte,
         db.cotizaciones.tiempo_garantia,
         db.cotizaciones.porcentaje_gastos_generales,
         db.cotizaciones.porcentaje_utilidad,
         db.cotizaciones.porcentaje_descuento,
         db.cotizaciones.porcentaje_igv,
         db.cotizaciones.costo_directo,
         db.cotizaciones.gastos_generales,
         db.cotizaciones.utilidad,
         db.cotizaciones.sub_total,
         db.cotizaciones.descuento,
         db.cotizaciones.sub_total_venta,
         db.cotizaciones.impuesto_igv,
         db.cotizaciones.total_venta,
         db.cotizaciones.cotizacion_adjunto,
         db.cotizaciones.observaciones,

         db.cotizaciones.nota,
     ],
     csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Cotizaciones')

def proyectos():
    response.view = 'customers/default.html'
    form = SQLFORM.smartgrid(db.proyectos,maxtextlength=200, formstyle='bootstrap', linked_tables=[
        'mano_de_obra',
        'productos',
        'costo_proyectos'
    ], csv=False)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name='Proyectos')

def tickets():
    name = 'TICKET DE ATENCIÓN'
    response.view = 'config/default.html'
    db.tickets.id.label = "N° Ticket"
    form = SQLFORM.grid(db.tickets,maxtextlength=200, formstyle='bootstrap',csv=False, orderby=~db.tickets.id)
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form, page_name=name)


