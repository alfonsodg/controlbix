# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def contabilidad():
    response.view = 'expenses/default.html'
    gasto = 'Contabilidad'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Contabilidad")

def area_comercial():
    response.view = 'expenses/default.html'
    gasto = 'Area Comercial'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Area Comercial")

def finanzas():
    response.view = 'expenses/default.html'
    gasto = 'Finanzas'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Finanzas")

def publicidad():
    response.view = 'expenses/default.html'
    gasto = 'Publicidad'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Publicidad")

def asesoria_juridica():
    response.view = 'expenses/default.html'
    gasto = 'Asesoría Jurídica'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Asesoría Jurídica")

def personal_administrativo():
    response.view = 'expenses/default.html'
    gasto = 'Personal Administrativo'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Personal Administrativo")

def viaticos():
    response.view = 'expenses/default.html'
    gasto = 'Viáticos'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Viáticos")

def telefonos():
    response.view = 'expenses/default.html'
    gasto = 'Teléfonos'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Teléfonos")

def combustible():
    response.view = 'expenses/default.html'
    gasto = 'Combustible'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Combustible")

def seguros():
    response.view = 'expenses/default.html'
    gasto = 'Seguros'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Seguros")

def alquileres():
    response.view = 'expenses/default.html'
    gasto = 'Alquileres'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Alquileres")

def mantenimiento():
    response.view = 'expenses/default.html'
    gasto = 'Mantenimiento'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Mantenimiento")

def creditos_leasing():
    response.view = 'expenses/default.html'
    gasto = 'Créditos Leasing'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Créditos Leasing")

def otros_gastos():
    response.view = 'expenses/default.html'
    gasto = 'Otros Gastos'
    item_id = db.tipos_gastos(db.tipos_gastos.nombre==gasto)['id']
    db.gastos.tipo_gasto.default = item_id
    db.gastos.tipo_gasto.writable = False
    form = SQLFORM.grid(db.gastos, csv=False, orderby=~db.gastos.id)
    return dict(form=form,page_name="Otros Gastos")
