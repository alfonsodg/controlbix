# -*- coding: utf-8 -*-
import gluon.contrib.simplejson
import types
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def get_items():
    MINCHARS = 2 # characters required to trigger response
    MAXITEMS = 20 # number of items in response
    query = request.vars.q
    base = request.vars.base
    fields = request.vars.content
    format = request.vars.format
    aux = request.vars.aux_base
    relation = request.vars.relation
    join = request.vars.join
    print request.vars
    aux_origin = False
    aux_table = False
    aux_field = False
    #relation_origin = False
    relation_table = False
    relation_fields = False
    relation_format = False
    if len(aux) > 0:
        tmp_aux = aux.split(':')
        aux_origin = tmp_aux[0]
        aux_table = tmp_aux[1]
        aux_field = tmp_aux[2]
    if len(relation) > 0:
        tmp_aux = relation.split(':')
        #relation_origin = tmp_aux[0]
        relation_table = tmp_aux[0]
        relation_fields = tmp_aux[1]
        relation_format = tmp_aux[2]
    id = db[base]['id']
    elements = {}
    if len(query.strip()) > MINCHARS and fields and base:
        data = []
        if len(relation) > 0:
            #main_data_fields = [db[base][field] for field in format.split(',')]
            data_fields = [db[relation_table][field] for field in relation_format.split(',')]
            data_fields.extend([db[base][field] for field in format.split(',')])
            content = [db[relation_table][field] for field in relation_fields.split(',')]
            condition = [field.contains(query.split(' '), all=True) for field in content]
            #tmp = []
            for cond in condition:
                data.extend([ row for row in db(cond).select(id, db[relation_table]['id'], *data_fields,
                                                            left=db[base].on(db[base][join]==db[relation_table]['id']),
                                                             limitby=(0, MAXITEMS)).as_list()])
            elements=[]
            for line in data:
                tmp = []
                for field in relation_format.split(','):
                    content = line[relation_table][field]
                    if content is None:
                        content = ''
                    tmp.append(content)
                elements.append({'uuid':line[base]['id'], 'aux_uid':line[relation_table]['id'], 'value': "-".join(tmp)})
        else:
            data_fields = [db[base][field] for field in format.split(',')]
            content = [db[base][field] for field in fields.split(',')]
            condition = [field.contains(query.split(' '), all=True) for field in content]
            for cond in condition:
                data.extend([ row for row in db(cond).select(id, *data_fields,
                    limitby=(0, MAXITEMS)).as_list()])
            elements=[]
            for line in data:
                tmp=[]
                for field in format.split(','):
                    content = line[field]
                    if field == aux_origin:
                        content = db[aux_table](db[aux_table]['id']==content)[aux_field]
                    tmp.append(content)
                elements.append({'uuid':line['id'], 'value': "-".join(tmp)})
    return gluon.contrib.simplejson.dumps(elements)

def get_data():
    query = request.vars.query #term
    base = request.vars.base #table
    fields = request.vars.content #fields
    if fields == '':
        fields = db[base].fields
    else:
        fields = fields.split(',')
    data_fields = [db[base][field] for field in fields]
    cond = (db[base]['id'] == query)
    data = db(cond).select(*data_fields)
    elements = []
    for line in data:
        elements = {}
        for field in fields:
            if type(line[field]) == datetime.date or type(line[field]) == datetime.time:
                elements[field] = "%s" % line[field]
            else:
                elements[field] = line[field]
    return gluon.contrib.simplejson.dumps(elements)


def grid_data():
    query = request.vars.query #term
    base = request.vars.base #table
    key_field = request.vars.content #fields
    elements = []
    if len(query) <= 0:
        return gluon.contrib.simplejson.dumps(elements)
    print "table:", base
    print "data:", query
    fields = db[base].fields
    data_fields = [db[base][field] for field in fields]
    condition = (db[base]['id'] > 0)
    if key_field is not None:
        condition &= (db[base][key_field] == query)
    data = db(condition).select(*data_fields)
    for row in data:
        tmp = {}
        for field in row:
            if field in fields:
                field_name = field
                if field_name == 'id':
                    field_name = 'recid'
                if type(row[field]) == datetime.date:
                    tmp[field_name] = "%s" % row[field]
                else:
                    tmp[field_name] = row[field]
        elements.append(tmp)
    return gluon.contrib.simplejson.dumps(elements)


def set_data():
    data = gluon.contrib.simplejson.loads(request.body.read())
#    try:
    for table in data:
        fields = db[table].fields
        new_data = {}
        id = data[table]['id']
        for field in data[table]:
            field_name = field
            value = data[table][field_name]
            if field in fields:
                new_data[field_name] = value
            if field.find('---') > 0 and id == '':
                tmp = field.split('---')
                field_name = tmp[0]
                relation = tmp[1]
                if relation == '':
                    new_data[field_name] = value
                else:
                    elements = relation.split(':')
                    add_table = elements[0]
                    add_field = elements[1]
                    extra_value = db[add_table].insert(**{add_field:value})
                    new_data[field_name] = extra_value
        del new_data['id']
        result = db(db[table]._id==id).update(**new_data)
        if result == 0:
            db[table].insert(**new_data)
    status = {'status':'ok'}
#    except Exception, error:
#        status = {'status':error}
    return gluon.contrib.simplejson.dumps(status)


def set_data_grid():
    data = gluon.contrib.simplejson.loads(request.body.read())
    table = data[0]['base']
    reference = data[0]['reference']
    value = data[0]['value']
    for row in data[1:]:
        new_data = {}
        id = False
        for field in row:
            head = field
            content = row[field]
            if type(content) is types.DictionaryType:
                content = content['id']
            if field == 'recid':
                head = 'id'
                id = content
            new_data[head] = content
            new_data[reference] = value

        result = db(db[table]._id==id).update(**new_data)
        if result == 0:
            db[table].insert(**new_data)
    status = {'status':'ok'}
    return gluon.contrib.simplejson.dumps(status)

