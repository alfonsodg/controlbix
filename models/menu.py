response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description

home = [
    (T('INDICE'), URL('default', 'index') == URL(), URL('default', 'index'), []),
]

configuracion = [
    (T('Configuracion'), False, None, [
        (T('General'), False, None, [
            (T('Area de Trabajo'), False, URL('config', 'areas_trabajo'), []),
            (T('Medio Captación'), False, URL('config', 'medios_captacion'), []),
            (T('Tipo de Visita'), False, URL('config', 'tipos_visitas'), []),
            (T('Tipo Personal'), False, URL('config', 'tipos_personal'), []),
            (T('Distrito'), False, URL('config', 'distritos'), []),
            (T('Provincia'), False, URL('config', 'provincias'), []),
            (T('Tipo Cuenta'), False, URL('config', 'tipos_cuentas'), []),
            (T('Tipo de Cliente'), False, URL('config', 'tipos_clientes'), []),
            (T('Tipo de Recibo'), False, URL('config', 'tipos_recibos'), []),
            (T('Tipo de Centro Costo'), False, URL('config', 'tipos_centros_costos'), []),
            (T('Tipo de Pago'), False, URL('config', 'tipos_pagos'), []),
            (T('Tipo de Contacto'), False, URL('config', 'tipos_contacto'), []),
            (T('Prioridades'), False, URL('config', 'prioridades'), []),
            (T('Actividades'), False, URL('config', 'actividades'), []),
            (T('Temporal'), False, URL('config', 't_documentosc_marcas'),[]),
            (T('Temporal2'), False, URL('config', 't_redes_sociales'),[]),
            # No estan en el DIAGRAMA DE VISUALIZACION
            # (T('Marcas'), False, URL('config', 'marcas'),[]),
            # (T('Razones Sociales'), False, URL('config', 'razones_sociales'),[]),
            # (T('Tipos de Servicios'), False, URL('config', 'tipos_servicios'),[]),
            # (T('Redes Sociales'), False, URL('config', 'redes_sociales'),[]),
            # (T('Tipos Documentos'), False, URL('config', 'tipos_documentos'),[]),
            # (T('Tipos de Relaciones'), False, URL('config', 'tipos_relaciones'),[]),
        ]),
        (T('Negocios'), False, None, [
            (T('Area de Negocio'), False, URL('config', 'areas_negocios'), []),
            (T('Razon Social GG'), False, URL('config', 'razones_zociales'), []),
            (T('Tipo de Cotizacion'), False, URL('config', 'tipos_cotizaciones'), []),
            (T('Moneda'), False, URL('config', 'monedas'), []),
            (T('Intereses'), False, URL('config', 'intereses'), []),
            (T('Tiempo de Decisión'), False, URL('config', 'tiempos_decision'), []),
            (T('Alcances del Servicio'), False, URL('config', 'alcances_servicios'), []),
            (T('Calidad de Acabados'), False, URL('config', 'calidades_acabados'), []),
            (T('Calidad de Material'), False, URL('config', 'calidades_materiales'), []),
            (T('Calidade de Limpieza'), False, URL('config', 'calidades_limpieza'), []),
            (T('Disposiciones de Desmonte'), False, URL('config', 'disposiciones_desmonte'), []),
            (T('Tiempo de Garantía'), False, URL('config', 'tiempos_garantia'), []),
            (T('Tipo de Transaccion'), False, URL('config', 'tipos_transacciones'), []),
            (T('Tipo de Trabajo'), False, URL('config', 'tipos_trabajos'), []),
            (T('Tipo Horario'), False, URL('config', 'tipos_horarios'), []),
            (T('Tipo de Inversion'), False, URL('config', 'tipos_inversiones'), []),
            # No estan en el DIAGRAMA DE VISUALIZACION
            # (T('Tipo Herrramientas epp'), False, URL('config', 'epps_herramientas'),[]),

        ]),
        (T('Operaciones'), False, None, [
            (T('Tipo de Comprobante'), False, URL('config', 'tipos_comprobantes'), []),
            (T('Encuesta Satisfacción'), False, URL('config', 'encuestas_satisfaccion'), []),
            (T('Unidades de Medida'), False, URL('config', 'unidades_medidas'), []),
            (T('Posicion / Rol'), False, URL('config', 'posiciones_roles'), []),
            (T('Plazos de Pago'), False, URL('config', 'plazos_pagos'), []),
            (T('Tipo Producto'), False, URL('config', 'tipos_productos'), []),
            (T('Tipo Ejecucion'), False, URL('config', 'tipos_ejecuciones'), []),
            # No estan en el DIAGRAMA DE VISUALIZACION
            # (T('Tipos de Gastos'), False, URL('config', 'tipos_gastos'),[]),

        ]),
        (T('Estados'), False, None,
         [
             (T('Estado Cliente'), False, URL('config', 'estados_clientes'), []),
             (T('Estado Cartas Presentacion'), False, URL('config', 'estados_cartas_presentacion'), []),
             (T('Estado Visita'), False, URL('config', 'estados_visitas'), []),
             (T('Estado Reporte'), False, URL('config', 'estados_reportes'), []),
             (T('Estado Presupuesto'), False, URL('config', 'estados_presupuestos'), []),
             (T('Estado Negociacion'), False, URL('config', 'estados_negociacion'), []),
             (T('Estado Operacion'), False, URL('config', 'estados_operaciones'), []),
             (T('Estado Facturacion'), False, URL('config', 'estados_facturacion'), []),
             (T('Estado Cuentas por Cobrar'), False, URL('config', 'estados_cuentas_cobrar'), []),
             (T('Estado Cuentas por Pagar'), False, URL('config', 'estados_cuentas_pagar'), []),
             (T('Estado Productos'), False, URL('config', 'estados_productos'), []),
             (T('Estado Post venta'), False, URL('config', 'estados_post_venta'), []),
             (T('Estado Cheques'), False, URL('config', 'estados_cheques'), []),
             (T('Estado Personal'), False, URL('config', 'estados_personal'), []),
             (T('Estado Contratos'), False, URL('config', 'estados_contratos'), []),
         ]),
        (T('Usuarios'), False, None, [
            (T('Administrar'), False, URL('user', 'manage'), []),
            (T('Grupos'), False, URL('user', 'groups'), []),
            (T('Membresías'), False, URL('user', 'memberships'), []),
        ]),
    ]),
]

geoperuvian = [
    ('GeoPeruvian Group', False, None, [
        (T('Marcas'), False, URL('corporate', 'marcas'), []),
        (T('Proveedores'), False, URL('corporate', 'proveedores'), []),
        (T('Personal'), False, URL('corporate', 'personal'), []),
#        (T('Control Calendario'), False, URL('corporate', 'control_calendario'), []),
        (T('Inventario de Productos'), False, URL('corporate', 'inventario_productos'), []),
        ('-----------------', False, False, []),
        (T('Marcas'), False, URL('corporate', 'marcas_datagrid'), []),
        (T('Proveedores'), False, URL('corporate', 'proveedores_datagrid'), []),
        (T('Personal'), False, URL('corporate', 'personal_datagrid'), []),
        (T('Inventario de productos'), False, URL('corporate', 'inventario_productos_datagrid'), []),
    ]),
]

control_calendario = [
    ('Control Calendario', False, None, [
        (T('Requerimientos'), False, URL('control_calendario', 'requerimientos'), []),
        (T('Bitacora'), False, URL('control_calendario', 'bitacora'), []),
        (T('Tareas'), False, URL('control_calendario', 'tareas'), []),
        (T('Reporte Diario'), False, URL('control_calendario', 'reporte_diario'), []),
        (T('Cotizaciones Enviadas'), False, URL('control_calendario', 'cotizaciones_enviadas'), []),
        (T('Cotizaciones Aprobadas'), False, URL('control_calendario', 'cotizaciones_aprobadas'), []),
        (T('Personal Operaciones'), False, URL('control_calendario', 'personal_operaciones'), []),
        (T('Proveedor Operaciones'), False, URL('control_calendario', 'proveedor_operaciones'), []),
    ]),
]

clientes = [
    (T('Clientes'), False, None, [
        (T('Clientes'), False, URL('customers', 'clientes'), []),
        (T('Contactos Clientes'), False, URL('customers', 'contactos'), []),
        (T('Direcciones Cliente'), False, URL('customers', 'clientes_direcciones'), []),
        (T('Ticket de Atención'), False, URL('customers', 'tickets'), []),
        (T('Cotizaciones'), False, URL('customers', 'cotizaciones'), []),
        # (T('Resultados'), False, URL('customers', 'resultados'),[]),
        (T('Proyectos'), False, URL('customers', 'proyectos'), []),
        ##('-----------------', False, False, []),
        ##(T('Clientes'), False, URL('corporate', 'cliente_datagrid'), []),
        # (T('Contactos'), False, URL('corporate', 'contactos_datagrid'),[]),
        # (T('Resultados'), False, URL('corporate', 'resultados_datagrid'),[]),
        ##(T('Cotizaciones'), False, URL('corporate', 'cotizacion_datagrid'), []),
        ##(T('Proyectos'), False, URL('corporate', 'proyectos_datagrid'), []),
    ]),
]

procesos = [
    (T('Procesos'), False, None, [
        (T('Comercial'), False, URL('processes', 'comercial'), []),
        (T('Visitas'), False, URL('processes', 'visitas'), []),
        #(T('Reportes'), False, URL('processes', 'reportes'), []),
        (T('Presupuestos'), False, URL('processes', 'presupuestos'), []),
        (T('Negociaciones'), False, URL('processes', 'negociaciones'), []),
        (T('Operaciones'), False, URL('processes', 'operaciones'), []),
        (T('Post Venta'), False, URL('processes', 'post_venta'), []),
    ]),
]

finanzas = [
    (T('Finanzas'), False, None, [
        (T('Facturacion'), False, URL('finances', 'facturacion'), []),
        (T('Cuentas por Cobrar'), False, URL('finances', 'cuentas_por_cobrar'), []),
        (T('Cuentas Bancos'), False, URL('finances', 'cuentas_bancarias'), []),
        (T('Gastos'), False, URL('finances', 'gastos_generales'), []),
        (T('Cuentas por Pagar'), False, URL('finances', 'cuentas_por_pagar'), []),
        (T('Inversiones'), False, URL('finances', 'inversiones'), []),
        ##('-----------------', False, False, []),
        ##(T('Facturacion'), False, URL('corporate', 'facturacion_datagrid'), []),
        #(T('Cuentas por Cobrar'), False, URL('corporate', 'cuentas_por_cobrar_datagrid'), []),
        #(T('Cuentas por Pagar'), False, URL('corporate', 'cuentas_por_pagar_datagrid'), []),
        ##(T('Cuentas Bancos'), False, URL('corporate', 'cuentas_bancos_datagrid'), []),
        #(T('Gastos Generales'), False, URL('corporate', 'gastos_generales_datagrid'), []),
        #(T('Inversiones'), False, URL('corporate', 'inversiones_datagrid'), []),
    ]),
]

# gastos_generales = [
#     (T('Gastos Generales'),False,None,[
#         (T('Contabilidad'), False, URL('expenses', 'contabilidad'),[]),
#         (T('Area Comercial'), False, URL('expenses', 'area_comercial'),[]),
#         (T('Finanzas'), False, URL('expenses', 'finanzas'),[]),
#         (T('Publicidad'), False, URL('expenses', 'publicidad'),[]),
#         (T('Asesoría Jurídica'), False, URL('expenses', 'asesoria_juridica'),[]),
#         (T('Personal Administrativo'), False, URL('expenses', 'personal_administrativo'),[]),
#         (T('Viáticos'), False, URL('expenses', 'viaticos'),[]),
#         (T('Teléfonos'), False, URL('expenses', 'telefonos'),[]),
#         (T('Combustible'), False, URL('expenses', 'combustible'),[]),
#         (T('Seguros'), False, URL('expenses', 'seguros'),[]),
#         (T('Alquileres'), False, URL('expenses', 'alquileres'),[]),
#         (T('Mantenimiento'), False, URL('expenses', 'mantenimiento'),[]),
#         (T('Créditos Leasing'), False, URL('expenses', 'creditos_leasing'),[]),
#         (T('Otros Gastos'), False, URL('expenses', 'otros_gastos'),[]),
#         ('-----------------', False, False,[]),
#         (T('Contabilidad'), False, URL('corporate', 'contabilidad_datagrid'),[]),
#         (T('Area Comercial'), False, URL('corporate', 'area_comercial_datagrid'),[]),
#         (T('Finanzas'), False, URL('corporate', 'finanzas_datagrid'),[]),
#         (T('Publicidad'), False, URL('corporate', 'publicidad_datagrid'),[]),
#         (T('Viaticos'), False, URL('corporate', 'viaticos_datagrid'),[]),
#         (T('Combustible'), False, URL('corporate', 'viaticos_datagrid'),[]),
#         (T('Seguros'), False, URL('corporate', 'viaticos_datagrid'),[]),
#         (T('Alquileres'), False, URL('corporate', 'viaticos_datagrid'),[]),
#         (T('Mantenimiento'), False, URL('corporate', 'mantenimiento_datagrid'),[]),
#         (T('Creditos Leasing'), False, URL('corporate', 'creditos_leasing_datagrid'),[]),
#         (T('Otros Gastos'), False, URL('corporate', 'otros_gastos_datagrid'),[]),
#         #falta Asesoría Jurídica, Personal Administrativo, Teléfonos
#     ]),
# ]

# inversiones = [
#     (T('Inversiones'),False,None,[
#         (T('Préstamo Bancario'), False, URL('investments', 'prestamo_bancario'),[]),
#         (T('Préstamo Socios'), False, URL('investments', 'prestamo_socios'),[]),
#         (T('Capital'), False, URL('investments', 'capital'),[]),
#         ('-----------------', False, False,[]),
#         (T('Prestamo Bancario'), False, URL('corporate', 'prestamo_bancario_datagrid'),[]),
#         (T('Prestamo Socios'), False, URL('corporate', 'prestamo_socios_datagrid'),[]),
#         (T('Capital'), False, URL('corporate', 'capital_datagrid'),[]),
#     ]),
# ]


reportes = [
    (T('Reportes Procesos'), False, None, [
        #        (T('Personal Operaciones'), False, URL('reports', 'personal_operaciones'),[]),
        #        (T('Personal Disponible'), False, URL('reports', 'personal_disponible'),[]),
        (T('Cartas de Presentación'), False, URL('reports', 'cartas_presentacion'), []),
        (T('Citas'), False, URL('reports', 'citas'), []),
        (T('Visitas'), False, URL('reports', 'visitas'), []),
        (T('Reportes Tecnicos'), False, URL('reports', 'reportes_tecnicos'), []),
        (T('Presupuesto'), False, URL('reports', 'presupuestos'), []),
        (T('Negociación'), False, URL('reports', 'negociacion'), []),
        (T('Negociación Avanzada'), False, URL('reports', 'negociacion_avanzada'), []),
        (T('Operaciones'), False, URL('reports', 'operaciones'), []),
        (T('Post Venta'), False, URL('reports', 'post_venta'), []),
        # (T('Aprobaciones Fechas'), False, URL('reports', 'aprobaciones_fechas'),[]),
        # (T('Cancelaciones Fechas'), False, URL('reports', 'cancelaciones_fechas'),[]),
        # (T('Ventas Fechas'), False, URL('reports', 'ventas_fechas'),[]),
        # (T('Ventas'), False, URL('reports', 'aprobaciones'),[]),
        # (T('Cancelaciones'), False, URL('reports', 'cancelaciones'),[]),
        # (T('Gastos Generales'), False, URL('reports', 'gastos_generales'),[]),
        # (T('Inversiones'), False, URL('reports', 'inversiones'),[]),
        # (T('Cheques por Cobrar'), False, URL('reports', 'cheques_cobrar'),[]),
    ]),
]

reportes_finanzas = [
    (T('Reportes Finanzas'), False, None, [
        (T('Facturación'), False, URL('reports', 'facturacion'), []),
        (T('Cobranzas a Tiempo'), False, URL('reports', 'cobranzas_tiempo'), []),
        (T('Cobranzas Vencidas'), False, URL('reports', 'cobranzas_vencidas'), []),
        (T('Cuentas por Pagar'), False, URL('reports', 'cuentas_pagar'), []),
        (T('Cuentas por Pagar Vencidas'), False, URL('reports', 'cuentas_pagar_vencido'), []),
        (T('Saldos Cuentas Bancos'), False, URL('reports', 'saldos_cuentas_bancos'), []),
    ]),
]

reportes_fecha = [
    (T('Reportes Periodos'), False, None, [
        (T('Aprobaciones'), False, URL('reports', 'aprobaciones_fechas'), []),
        (T('Cancelaciones'), False, URL('reports', 'cancelaciones_fechas'), []),
        (T('Ventas'), False, URL('reports', 'ventas_fechas'), []),
#        (T('Cuentas Cobrar'), False, URL('reports', 'cuentas_cobrar_fechas'), []),
        (T('Cobranzas a Tiempo'), False, URL('reports', 'cobranzas_a_tiempo_fechas'), []),
        (T('Cobranzas Vencidas'), False, URL('reports', 'cobranzas_vencidas_fechas'), []),
        #(T('Cuentas Pagar'), False, URL('reports', 'cuentas_pagar_fechas'), []),
        (T('Gastos Generales'), False, URL('reports', 'gastos_generales_fechas'), []),
        (T('Inversiones'), False, URL('reports', 'inversiones_fechas'), []),
        (T('Remuneraciones'), False, URL('reports', 'remuneraciones_personal_fechas'), []),
    ]),
]

datagrid = [
    (T('Data Grid'), False, None, [
        (T('Marcas'), False, URL('corporate', 'marcas_datagrid'), []),
        (T('Proveedores'), False, URL('corporate', 'proveedores_datagrid'), []),
        (T('Personal'), False, URL('corporate', 'personal_datagrid'), []),
        (T('Tarea Personal'), False, URL('corporate', 'tarea_personal_datagrid'), []),
        (T('Almacen'), False, URL('corporate', 'almacen_datagrid'), []),
        (T('Cliente'), False, URL('corporate', 'cliente_datagrid'), []),
        (T('Contactos'), False, URL('corporate', 'contactos_datagrid'), []),
        (T('Direcciones'), False, URL('corporate', 'direcciones_datagrid'), []),
        (T('Cotizacion'), False, URL('corporate', 'cotizacion_datagrid'), []),
        (T('Proyectos'), False, URL('corporate', 'proyectos_datagrid'), []),
        (T('Facturacion'), False, URL('corporate', 'facturacion_datagrid'), []),
        (T('Cuentas por Cobrar'), False, URL('corporate', 'cuentasporcobrar_datagrid'), []),
        (T('Cuentas por Pagar'), False, URL('corporate', 'cuentasporpagar_datagrid'), []),
        (T('Cuentas Bancos'), False, URL('corporate', 'cuentas_bancos_datagrid'), []),
        (T('Costo Proyecto'), False, URL('corporate', 'costo_proyecto_datagrid'), []),
        (T('Contabilidad'), False, URL('corporate', 'contabilidad_datagrid'), []),
        (T('Area Comercial'), False, URL('corporate', 'area_comercial_datagrid'), []),
        (T('Finanzas'), False, URL('corporate', 'finanzas_datagrid'), []),
        (T('Publicidad'), False, URL('corporate', 'publicidad_datagrid'), []),
        (T('Viaticos'), False, URL('corporate', 'viaticos_datagrid'), []),
        (T('Combustible'), False, URL('corporate', 'viaticos_datagrid'), []),
        (T('Seguros'), False, URL('corporate', 'viaticos_datagrid'), []),
        (T('Alquileres'), False, URL('corporate', 'viaticos_datagrid'), []),
        (T('Mantenimiento'), False, URL('corporate', 'mantenimiento_datagrid'), []),
        (T('Creditos Leasing'), False, URL('corporate', 'creditos_leasing_datagrid'), []),
        (T('Otros Gastos'), False, URL('corporate', 'otros_gastos_datagrid'), []),
        (T('Prestamo Bancario'), False, URL('corporate', 'prestamo_bancario_datagrid'), []),
        (T('Prestamo Socios'), False, URL('corporate', 'prestamo_socios_datagrid'), []),
        (T('Capital'), False, URL('corporate', 'capital_datagrid'), []),
    ]),
]

response.menu = home
# if auth.user:
# user = auth.user.id
#    if auth.has_membership(user_id=auth.user.id, role='root'):
response.menu += configuracion
response.menu += geoperuvian
response.menu += control_calendario
response.menu += clientes
response.menu += procesos
response.menu += finanzas
# response.menu += gastos_generales
# response.menu += inversiones


response.menu += reportes
response.menu += reportes_finanzas
response.menu += reportes_fecha


# response.menu += datagrid
# else:
#     response.menu += geo
#     response.menu += personal
#     response.menu += commercial
#     response.menu += negotiation
#     response.menu += operation
#     response.menu += report
