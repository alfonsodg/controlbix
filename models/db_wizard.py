# -*- coding: utf-8 -*-
### we prepend t_ to tablenames and f_ to fieldnames for disambiguity
db.auth_group.update_or_insert(role='root')
db.auth_group.update_or_insert(role='user')
db.auth_group.update_or_insert(role='review')

review_id = db.auth_group(db.auth_group.role == 'review')['id']

auth.settings.everybody_group_id = review_id

db.tipos_gastos.update_or_insert(nombre='Contabilidad')
db.tipos_gastos.update_or_insert(nombre='Area Comercial')
db.tipos_gastos.update_or_insert(nombre='Finanzas')
db.tipos_gastos.update_or_insert(nombre='Publicidad')
db.tipos_gastos.update_or_insert(nombre='Asesoría Jurídica')
db.tipos_gastos.update_or_insert(nombre='Personal Administrativo')
db.tipos_gastos.update_or_insert(nombre='Viáticos')
db.tipos_gastos.update_or_insert(nombre='Teléfonos')
db.tipos_gastos.update_or_insert(nombre='Combustible')
db.tipos_gastos.update_or_insert(nombre='Seguros')
db.tipos_gastos.update_or_insert(nombre='Alquileres')
db.tipos_gastos.update_or_insert(nombre='Mantenimiento')
db.tipos_gastos.update_or_insert(nombre='Créditos Leasing')
db.tipos_gastos.update_or_insert(nombre='Otros Gastos')

db.tipos_inversiones.update_or_insert(nombre='Préstamo Bancario')
db.tipos_inversiones.update_or_insert(nombre='Préstamo Socios')
db.tipos_inversiones.update_or_insert(nombre='Capital')
