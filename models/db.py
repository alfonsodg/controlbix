# -*- coding: utf-8 -*-
import datetime
from gluon.contrib.appconfig import AppConfig
from gluon.tools import Auth, Service, PluginManager
from decimal import Decimal, ROUND_HALF_UP

now = datetime.datetime.now()
#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

## app configuration made easy. Look inside private/appconfig.ini
## once in production, remove reload=True to gain full speed
myconf = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL(myconf.take('db.uri'), pool_size=myconf.take('db.pool_size', cast=int), check_reserved=['all'])
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore+ndb')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## choose a style for forms
response.formstyle = myconf.take('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.take('forms.separator')

## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################


auth = Auth(db)
service = Service()
plugins = PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else myconf.take('smtp.sender')
mail.settings.sender = myconf.take('smtp.sender')
mail.settings.login = myconf.take('smtp.login')

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

mail.settings.server = settings.email_server
mail.settings.sender = settings.email_sender
mail.settings.login = settings.email_login


def redondear(monto, exponente=Decimal('.01')):
    return monto.quantize(exponente, rounding=ROUND_HALF_UP)


def advertencia(cotizacion, total_venta):
    if cotizacion is None or total_venta is None:
        return
    data = db.cotizaciones(db.cotizaciones.id == cotizacion)
    cotizacion_total = float(data['total_venta'])
    factura_total = float(total_venta)
    diferencia = factura_total - cotizacion_total
    message = 'OK'
    if diferencia <> 0:
        message = 'ACTUALIZAR'
    return message


def advertencia_aux(factura, total_venta):
    message = 'OK'
    try:
        data = db.facturacion(db.facturacion.id == factura)
        factura_total = float(data['total_venta'])
        cuenta_total = float(total_venta)
        diferencia = cuenta_total - factura_total
        if diferencia <> 0:
            message = 'ACTUALIZAR'
    except Exception, Error:
        message = 'ACTUALIZAR'
    return message


def cobrar_calculo(total_venta, monto_adelanto_inicial, monto_adelanto_parcial, monto_detraccion):
    if total_venta is None:
        total_venta = 0
    if monto_adelanto_inicial is None or monto_adelanto_inicial == '':
        monto_adelanto_inicial = 0
    if monto_adelanto_parcial is None or monto_adelanto_parcial == '':
        monto_adelanto_parcial = 0
    if monto_detraccion is None:
        monto_detraccion = 0
    calculo = float(total_venta) - (
        float(monto_adelanto_inicial) + float(monto_adelanto_parcial) + float(monto_detraccion))
    return calculo


def name_data(table, id_val, field='name'):
    """
    Content Data Representation
    """
    try:
        value_data = table(id_val)[field]
    except:
        value_data = ''
    return '%s' % value_data


status_options = {
    0: T('---- Elegir Opción ----'),
}

db.define_table('marcas',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('razones_sociales',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('areas_trabajo',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_servicios',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('medios_captacion',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_visitas',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_personal',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('distritos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('provincias',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_cuentas',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_documentos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('areas_negocios',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_cotizaciones',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('monedas',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('intereses',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tiempos_decision',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('alcances_servicios',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('calidades_acabados',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('calidades_materiales',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('calidades_limpieza',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('disposiciones_desmonte',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tiempos_garantia',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_transacciones',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_trabajos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_horarios',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_comprobantes',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_gastos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('encuestas_satisfaccion',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('unidades_medidas',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('posiciones_roles',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('plazos_pagos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_productos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_clientes',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_cartas_presentacion',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_visitas',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_reportes',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_presupuestos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_negociacion',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_operaciones',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_facturacion',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_cuentas_cobrar',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_cuentas_pagar',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_productos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_post_venta',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_cheques',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_personal',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('estados_contratos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_inversiones',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_relaciones',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('epps_herramientas',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_clientes',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_recibos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_centros_costos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_pagos',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('tipos_ejecuciones',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('razones_zociales_gg',
                Field('razon_social_gg', 'string'),
                format='%(razon_social_gg)s')

db.define_table('tipos_contactos',
                Field('nombre', 'string'),
                format='%(nombre)s')

db.define_table('prioridades',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('actividades',
                Field('nombre', 'string'),
                format='%(nombre)s'
                )

db.define_table('marcas_definiciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('marca', 'string', label=T('Marca GG')),
                Field('razon_zocial_gg', 'reference razones_zociales_gg', label=T('Razon Social GG'),
                      requires=IS_EMPTY_OR(
                          IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s', zero='---- Elegir Opcion ----'))),
                Field('ruc', 'string', label=T('RUC GG')),
                Field('direccion_fiscal', 'string', label=T('Dirección Fiscal GG')),
                Field('telefono_contacto', 'string'),
                Field('email_contacto', 'string', label=T('Email Contacto')),
                Field('nota', 'text', label=T('Nota')),
                format='%(marca)s')

db.define_table('proveedores',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('tipo_proveedor', 'reference tipos_personal', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_personal, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('proveedor', 'string'),
                # Field('marca', 'reference marcas_definiciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.marcas_definiciones, '%(marca)s', zero='---- Elegir Opcion ----')),label=T('Marca GG')),
                Field('referencia_busqueda', 'string'),
                Field('razon_social', 'string'),
                Field('ruc', 'string'),
                Field('direccion_fiscal', 'string'),
                Field('telefono_contacto', 'string'),
                Field('email_contacto', 'string'),
                Field('web_url', 'string'),
                Field('nota', 'text'),
                format='%(proveedor)s')

db.define_table('clientes',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('cliente', 'string'),
                Field('creacion', 'date', default=now, label=T('Fecha Actualizacion'),writable=False),
                Field('tipo_cliente', 'reference tipos_clientes', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_clientes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('razon_social_nombre_completo', 'string', label=T('Razon social/ Nombre completo')),
                Field('ruc_dni_ce', 'string', label=T('Ruc / Dni/ Ce')),
                #Field('direccion_fiscal_cliente', label=T('Actividad')),
                Field('actividad', 'reference actividades', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.actividades, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_aniversario', 'date'),
                Field('area_negocio', 'reference areas_negocios', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.areas_negocios, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('estado_cliente', 'reference estados_clientes', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_clientes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('medio_captacion', 'reference medios_captacion', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.medios_captacion, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                # Field('direccion_principal', 'string'),
                Field('telefono_contacto', 'string',label='Telefono Cliente'),
                Field('email_contacto', 'string',label='Email Cliente'),
                Field('url', 'string'),
                Field('nota', 'text'),
                format='%(cliente)s'
                )

db.define_table('tickets',
                Field('creacion', 'date', label='Fecha Registro Ticket', default=now, writable=False),
                Field('cliente', 'reference clientes'),
                Field('referencia', 'string', label='Referencia Cliente'),
                Field('marca_gg', 'reference marcas_definiciones', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.marcas_definiciones, '%(marca)s', zero='---- Elegir Opcion ----'))),
                Field('razon_social_gg', 'reference razones_zociales_gg', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s', zero='---- Elegir Opcion ----'))),
                Field('distrito', 'reference distritos',label='Distrito de Atencion'),
                Field('nota', 'text'),
                format='%(id)s')
                #format=lambda r: "%s-%s-%s" % (db.clientes[r.cliente]['cliente'], r.id, r.referencia))

db.define_table('contactos_marcas',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones'),
                #        Field('proveedor', 'reference proveedores'),
                #        Field('cliente', 'reference clientes'),
                Field('contacto_marca', 'string'),
                # Field('fecha_nacimiento', 'date'),
                Field('area_trabajo', 'reference areas_trabajo', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.areas_trabajo, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('posicion_rol', 'reference posiciones_roles', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.posiciones_roles, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                # Field('telefono_corporativo', 'string'),
                Field('telefono', 'string'),
                # Field('email_corporativo', 'string'),
                Field('email', 'string'),
                format='%(contacto_marca)s')

db.define_table('contactos_proveedores',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('proveedor', 'reference proveedores'),
                Field('contacto_proveedor', 'string'),
                Field('fecha_nacimiento', 'date'),
                Field('area_trabajo', 'reference areas_trabajo', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.areas_trabajo, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('posicion_rol', 'reference posiciones_roles', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.posiciones_roles, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('telefono', 'string'),
                Field('email', 'string'),
                format='%(contacto)s')

db.define_table('contactos',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                # Field('marca_definicion', 'reference marcas_definiciones'),
                # Field('proveedor', 'reference proveedores'),
                Field('fecha_registro', 'date', default=now),
                Field('cliente', 'reference clientes',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.clientes, '%(cliente)s', zero='---- Elegir Opcion ----'))),
                Field('contacto_cliente', 'string'),
                Field('tipo_contacto', 'reference tipos_contactos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_contactos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_nacimiento', 'date'),
                Field('area_trabajo', 'reference areas_trabajo', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.areas_trabajo, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('posicion_rol', 'reference posiciones_roles', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.posiciones_roles, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('telefono_personal', 'string'),
                Field('telefono_corporativo', 'string'),
                Field('email_personal', 'string'),
                Field('email_corporativo', 'string'),
                Field('nota', 'text'),
                format='%(contacto_cliente)s')

db.define_table('redes_sociales',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('red_social', 'string'),
                Field('correo_usuario', 'string'),
                Field('contrasena', 'string'),
                Field('url', 'string'),
                format='%(red_social)s')

db.define_table('suscripciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('red_social', 'string', label=T('Suscripciones')),
                Field('correo_usuario', 'string'),
                Field('contrasena', 'string'),
                Field('url', 'string'),
                Field('nota', 'text'),
                format='%(red_social)s')

db.define_table('convenios_contratos',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('convenio_contrato', 'string'),
                Field('detalle', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(red_social)s')

db.define_table('comercial',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('base_datos', 'string'),
                Field('detalle', 'string'),
                Field('avance_comercial', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(red_social)s')

db.define_table('marketing',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('plan_marketing', 'string'),
                Field('detalle', 'string'),
                Field('avance', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(red_social)s')

db.define_table('dd_negocios',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('cliente_potencial', 'string'),
                Field('contacto_potencial', 'string'),
                Field('telefonos_email_contacto', 'string'),
                Field('detalle_plan', 'string'),
                Field('avance_plan', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(red_social)s')

db.define_table('servicios_productos',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('servicio_producto', 'string'),
                Field('detalles_servicio_producto', 'string'),
                format='%(servicio_producto)s')

db.define_table('documentos_marcas',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('documento', 'string'),
                Field('detalle', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(documento)s')

db.define_table('administracion_documentos',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('documento', 'string'),
                Field('detalle', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(documento)s')


db.define_table('politicas_protocolos',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('marca', 'reference marcas_definiciones', label=T('Marca GG')),
                Field('documento', 'string'),
                Field('detalle', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(red_social)s')

db.define_table('documentos_proveedores',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('proveedor', 'reference proveedores'),
                Field('documento', 'string'),
                Field('documento_adjunto', 'upload'),
                format='%(documento)s')

db.define_table('documentos_clientes',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('tipo', 'reference tipos_relaciones'),
                Field('cliente', 'reference clientes'),
                Field('documento', 'string'),
                Field('archivo_adjunto', 'upload'),
                format='%(documento)s')

db.define_table('cuentas_bancarias_proveedores',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('proveedor', 'reference proveedores'),
                Field('cuenta', 'string'),
                Field('tipo_cuenta', 'reference tipos_cuentas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_cuentas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('entidad_bancaria', 'string'),
                Field('moneda', 'reference monedas',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.monedas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('codigo_interbancario', 'string'),
                format='%(banco)s%(numero_cuenta)s')

db.define_table('clientes_direcciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('cliente', 'reference clientes'),
                Field('direccion_cliente', 'string'),
                Field('tipo_direccion', 'string'),
                Field('distrito', 'reference distritos',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.distritos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('provincia', 'reference provincias',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.provincias, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('referencia', 'string'),
                Field('plano', 'upload'),
                Field('nota', 'text'),
                format='%(direccion_cliente)s')

db.define_table('direcciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('cliente', 'reference clientes'),
                Field('direccion_cliente', 'string'),
                Field('tipo_direccion', 'string'),
                Field('distrito', 'reference distritos',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.distritos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('provincia', 'reference provincias',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.provincias, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('referencia', 'string'),
                Field('mapa_archivo_adjunto', 'upload', label=T('Plano')),
                Field('nota', 'text'),
                format='%(direccion_cliente)s')

db.define_table('personal',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('persona', 'string'),
                # Field('marca', 'reference marcas_definiciones', label=T('Marca GG'),requires=IS_EMPTY_OR(IS_IN_DB(db, db.marcas_definiciones, '%(marca)s', zero='---- Elegir Opcion ----'))),
                Field('referencia_de_busqueda', 'string'),
                Field('estado_personal', 'reference estados_personal', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_personal, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('estado_contrato', 'reference estados_contratos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_contratos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('dni', 'string'),
                Field('tipo_personal', 'reference tipos_personal', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_personal, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                # Field('direccion_fiscal', 'string'),
                Field('fecha_nacimiento', 'date'),
                Field('telefono_celular', 'string'),
                Field('telefono_fijo', 'string'),
                Field('direccion_fiscal', 'string'),
                Field('email_personal', 'string'),
                Field('ocupacion', 'string'),
                Field('grado_estudios', 'string'),
                Field('especialidad', 'string'),
                Field('area_trabajo', 'reference areas_trabajo', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.areas_trabajo, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('posicion_rol', 'reference posiciones_roles', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.posiciones_roles, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('experiencia', 'string'),
                Field('pretension_mensual', 'string'),
                Field('pretension_semanal', 'string'),
                Field('pretencion_diaria', 'string'),
                Field('clave_sol', 'string'),
                Field('contrato', 'string'),
                Field('seguro_scrt', 'string'),
                Field('vencimiento_seguro_scrt', 'date'),
                Field('numero_cuenta_haberes', 'string'),
                Field('entidad_bancaria', 'string'),
                Field('codigo_interbancario', 'string'),
                Field('nota', 'text'),

                format='%(persona)s')

db.define_table('items_productos',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_item', 'string'),
                # Field('producto', 'string'),
                Field('descripcion_item', 'string'),
                Field('unidad_medida', 'string'),
                Field('cantidad', 'float'),
                Field('precio', 'float'),
                Field('parcial', 'float'),
                format='%(id)s')

#    format='%(numero_item)s %(descripcion_item)s')
db.define_table('documentos_personal',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('persona', 'reference personal'),
                Field('documento', 'string'),
                Field('archivo_adjunto', 'upload'),
                format='%(documento)s')

db.define_table('tareos_personal',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('persona', 'reference personal'),
                Field('fecha_dia_trabajo', 'date'),
                Field('tipo_horario', 'reference tipos_horarios', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_horarios, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('bitacora', 'string'),
                Field('tarifa_dia', 'float'),
                Field('tarifa_hora', 'float'),
                Field('puntualidad', 'boolean'),
                Field('descuento_tardanza', 'float'),
                Field('horas_falta', 'float'),
                Field('descuento_total',
                      compute=lambda r: round((r['tarifa_hora'] * r['horas_falta']) + r['descuento_tardanza'], 2)),
                Field('horas_extra', 'float'),
                Field('bonificacion_tipo_horario', 'float'),
                Field('bonificacion_total',
                      compute=lambda r: round((r['tarifa_hora'] * r['horas_extra']) + r['bonificacion_tipo_horario'],
                                              2)),
                Field('remuneracion',
                      compute=lambda r: round((r['tarifa_dia'] + r['bonificacion_total']) - r['descuento_total'], 2)),
                Field('nota', 'text'),
                format='%(id)s')

db.define_table('contactos_clientes',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('contacto_cliente', 'string'),
                Field('fecha_registro', 'date', default=now),
                Field('fecha_nacimiento', 'date'),
                Field('area_trabajo', 'reference areas_trabajo', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.areas_trabajo, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('posicion_rol', 'reference posiciones_roles', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.posiciones_roles, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('telefono_otro', 'string', label=T('Telefono Corporativo')),
                Field('celular_personal', 'string', label=T('Telefono personal')),
                Field('email_otro', 'string', label=T('Email Corporativo')),
                Field('email_personal', 'string', label=T('Email personal')),
                Field('nota', 'text'),
                # Field('tipo', 'reference tipos_relaciones'),
                # Field('marca_definicion', 'reference marcas_definiciones'),
                # Field('proveedor', 'reference proveedores'),
                format='%(contactos_clientes)s')
# Field('creacion', 'date', default=now, writable=False),
# ========================================================
db.define_table('cotizaciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('fecha_registro_cotizacion', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('numero_cotizacion', 'string'),
                Field('reporte_adjunto', 'upload', label='Documento Reporte'),
                Field('cliente', 'reference clientes', writable=False),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('contacto_cotizacion', 'reference contactos', requires=IS_EMPTY_OR(    IS_IN_DB(db, db.contactos, '%(contacto_cliente)s', zero='---- Elegir Opcion ----'))),

                Field('fecha_cotizacion', 'date', default=now),
                Field('servicio_producto_cotizacion', 'reference servicios_productos', requires=IS_EMPTY_OR(IS_IN_DB(db, db.servicios_productos, '%(servicio_producto)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_trabajo', 'reference tipos_trabajos', requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_trabajos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_horario', 'reference tipos_horarios', requires=IS_EMPTY_OR( IS_IN_DB(db, db.tipos_horarios, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_cotizacion', 'reference tipos_cotizaciones', requires=IS_EMPTY_OR(  IS_IN_DB(db, db.tipos_cotizaciones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('plazo_ejecucion_promedio', 'string', default="Null"),
                Field('moneda', 'reference monedas',   requires=IS_EMPTY_OR(IS_IN_DB(db, db.monedas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_comprobante', 'reference tipos_comprobantes', requires=IS_EMPTY_OR(  IS_IN_DB(db, db.tipos_comprobantes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('alcances_servicio', 'reference alcances_servicios', requires=IS_EMPTY_OR( IS_IN_DB(db, db.alcances_servicios, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('aplica_adicionales', 'boolean'),
                Field('aplica_penalidades', 'boolean'),
                Field('plazo_pago', 'reference plazos_pagos', requires=IS_EMPTY_OR(IS_IN_DB(db, db.plazos_pagos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('calidad_acabado', 'reference calidades_acabados', requires=IS_EMPTY_OR(IS_IN_DB(db, db.calidades_acabados, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('calidad_material', 'reference calidades_materiales', requires=IS_EMPTY_OR(IS_IN_DB(db, db.calidades_materiales, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('calidad_limpieza', 'reference calidades_limpieza', requires=IS_EMPTY_OR(IS_IN_DB(db, db.calidades_limpieza, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('disposicion_desmonte', 'reference disposiciones_desmonte', requires=IS_EMPTY_OR(IS_IN_DB(db, db.disposiciones_desmonte, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tiempo_garantia', 'reference tiempos_garantia', requires=IS_EMPTY_OR(IS_IN_DB(db, db.tiempos_garantia, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('porcentaje_gastos_generales', type='decimal(10,2)', default=0),
                Field('porcentaje_utilidad', type='decimal(10,2)', default=0),
                Field('porcentaje_descuento', type='decimal(10,2)', default=0),
                Field('porcentaje_igv', type='decimal(10,2)', default=0),
                Field('costo_directo', type='decimal(10,2)', default=0),
                Field('gastos_generales', compute=lambda r: redondear((r['costo_directo'] * r['porcentaje_gastos_generales'] / 100))),
                Field('utilidad', compute=lambda r: redondear((r['costo_directo'] * r['porcentaje_utilidad'] / 100))),
                Field('sub_total',compute=lambda r: redondear(r['costo_directo'] + r['gastos_generales'] + r['utilidad'])),
                Field('descuento', compute=lambda r: redondear((r['sub_total'] * r['porcentaje_descuento'] / 100))),
                Field('sub_total_venta', compute=lambda r: (r['sub_total'] - r['descuento'])),
                Field('impuesto_igv', compute=lambda r: redondear((r['sub_total_venta'] * r['porcentaje_igv'] / 100))),
                Field('total_venta', compute=lambda r: redondear(r['sub_total_venta'] + r['impuesto_igv'])),
                Field('cotizacion_adjunto', 'upload', label='Documento Cotizacion'),
                Field('observaciones', 'text'),
                Field('nota', 'text'),
                format='%(numero_cotizacion)s')

db.cotizaciones.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('propuesta_tecnica',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_cotizacion', 'reference cotizaciones'),
                Field('titulo_concepto', 'string'),
                Field('descripcion_concepto', 'string'),
                format='%(id)s')

db.define_table('propuesta_economica',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_cotizacion', 'reference cotizaciones'),
                Field('numero_item', 'string'),
                Field('descripcion_item', 'string'),
                Field('unidad_medida', 'reference unidades_medidas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.unidades_medidas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('cantidad', 'float'),
                Field('precio_unitario', 'float'),
                Field('monto_parcial', compute=lambda r: round(r['cantidad'] * r['precio_unitario'])),
                format='%(id)s')

# ========================================================
db.define_table('comercial_cliente',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('numero_ticket', 'reference tickets'),
                Field('cliente', 'reference clientes'),
                Field('marca_gg', 'reference marcas_definiciones'),
                Field('contacto_cliente', 'reference contactos'),
                Field('estado_carta_presentacion', 'reference estados_cartas_presentacion'),
                Field('fecha_envio_carta', 'date'),
                Field('nota', 'text'),
                format='%(id)s')

# format='%(id)s - %(referencia)s')
db.define_table('visitas',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('fecha_registro_ticket', 'date', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('contacto_visita', 'reference contactos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.contactos, '%(contacto_cliente)s', zero='---- Elegir Opcion ----'))),
                # Field('contacto_cliente', 'reference contactos', requires=IS_EMPTY_OR(IS_IN_DB(db, db.contactos, '%(contacto_cliente)s', zero='---- Elegir Opcion ----'))),
                Field('servicio_producto', 'reference servicios_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.servicios_productos, '%(servicio_producto)s', zero='---- Elegir Opcion ----'))),
                Field('estado_visita', 'reference estados_visitas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_visitas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_visita', 'reference tipos_visitas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_visitas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_visita', 'date'),
                Field('hora_visita', 'string'),
                Field('direccion_cliente', 'reference clientes_direcciones', label='Direccion Visita', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.clientes_direcciones, '%(direccion_cliente)s', zero='---- Elegir Opcion ----'))),
                Field('distrito', 'reference distritos', writable=False, label='Distrito de Atencion',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.distritos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('referencia_visita', 'string', writable=False),
                Field('personal_visita', 'reference personal',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.personal, '%(persona)s', zero='---- Elegir Opcion ----'))),
                Field('nota', 'text'),
                format='%(id)s')

db.visitas.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('reportes',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                # Field('servicio_producto', 'reference servicios_productos',requires=IS_EMPTY_OR(IS_IN_DB(db, db.servicios_productos, '%(servicio_producto)s', zero='---- Elegir Opcion ----'))),
                Field('estado_reporte', 'reference estados_reportes', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_reportes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('personal_reporte', 'reference personal',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.personal, '%(persona)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_envio_reporte', 'date'),
                Field('nota', 'text'),
                format='%(id)s')

db.reportes.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('presupuestos',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('numero_cotizacion', 'reference cotizaciones', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('fecha_visitado', 'date', writable=False),
                Field('personal_visitado', 'reference personal', writable=False),
                Field('contacto_cotizacion', 'reference contactos', writable=False),
                Field('servicio_producto_cotizacion', 'reference servicios_productos', writable=False),
                Field('estado_presupuesto', 'reference estados_presupuestos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_presupuestos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_registro_cotizacion', 'date', writable=False),
                Field('fecha_envio_cotizacion', 'date'),
                Field('prioridad', 'reference prioridades'),
                #Field('fecha_envio_modificacion', 'date'),
                Field('nota', 'text'),
                format='%(id)s')

db.presupuestos.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('negociaciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('numero_cotizacion', 'reference cotizaciones', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('servicio_producto_cotizacion', 'reference servicios_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.servicios_productos, '%(servicio_producto)s', zero='---- Elegir Opcion ----')),
                      writable=False),
                Field('contacto_cotizacion', 'reference contactos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.contactos, '%(contacto_cliente)s', zero='---- Elegir Opcion ----')),
                      writable=False),
                Field('estado_negociacion', 'reference estados_negociacion', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_negociacion, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_cotizacion', 'date', label='Fecha envio de Cotizacion', writable=False),
                Field('fecha_ultima_conversacion', 'date'),
                #Field('fecha_cotizacion', 'date'),
                Field('porcentaje_descuento', 'float', default=0, writable=False),
                Field('sub_total', 'float', default=0, writable=False),
                Field('descuento', 'float', default=0, writable=False),
                Field('sub_total_venta', 'float', default=0, writable=False),
                Field('impuesto_igv', 'float', default=0, writable=False),
                Field('total_venta', 'float', default=0, writable=False),
                # Field('tipo_ejecucion', 'reference tipos_ejecuciones'),
                Field('interes', 'reference intereses',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.intereses, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tiempo_decision', 'reference tiempos_decision', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tiempos_decision, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_aprobacion', 'date'),
                Field('fecha_cancelacion', 'date'),
                Field('motivo_aprobacion_cancelacion', 'string'),
                Field.Virtual('validacion',
                              lambda r: advertencia(r.negociaciones.numero_cotizacion, r.negociaciones.total_venta)),
                Field('prioridad', 'reference prioridades'),
                Field('nota', 'text'),
                format='%(id)s')

db.negociaciones.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

# ===============================================================
# ===============================================================

db.define_table('proyectos',
                Field('fecha_registro', 'date', label='Fecha Registro Proyecto', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('numero_proyecto', 'string'),
                Field('numero_cotizacion', 'reference cotizaciones', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('servicio_producto_cotizacion', 'reference servicios_productos', writable=False),
                Field('documento_aprobacion', 'upload'),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('orden_trabajo', 'upload', label='Documento Orden de Trabajo'),
                Field('aplica_indumentaria', 'boolean'),
                Field('aplica_epp', 'boolean'),
                Field('aplica_seguro_scrt', 'boolean'),
                Field('aplica_examen_medico', 'boolean'),
                Field('aplica_guia_remision', 'boolean'),
                Field('documento_entrega', 'upload'),
                Field('informe_proyecto', 'upload', label='Documento Informe Proyecto'),
                Field('documento_certificado_garantia', 'upload'),
                Field('nota', 'string'),
                Field('tipo_ejecucion', 'reference tipos_ejecuciones', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_ejecuciones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_aprobacion', 'date', writable=False),
                Field('fecha_inicio', 'date'),
                Field('fecha_finalizacion', 'date'),
                Field('fecha_prueba_control', 'date'),
                Field('fecha_entrega', 'date', label='Fecha Entrega Proyecto'),
                Field('fecha_envio_informe', 'date'),
                format='%(numero_proyecto)s')

db.proyectos.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('mano_de_obra',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_proyecto', 'reference proyectos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.proyectos, '%(numero_proyecto)s', zero='---- Elegir Opcion ----'))),
                Field('personal', 'reference personal', label='Personal Proyecto',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.personal, '%(persona)s', zero='---- Elegir Opcion ----'))),
                Field('proveedor', 'reference proveedores', label='Proveedor Proyecto',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.proveedores, '%(proveedor)s', zero='---- Elegir Opcion ----'))),
                Field('posicion_rol', 'reference posiciones_roles', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.posiciones_roles, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_horario', 'reference tipos_horarios', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_horarios, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tarifa_dia', 'float'),
                Field('cantidad_dias', 'float'),
                Field('remuneracion', compute=lambda r: round(r['tarifa_dia'] * r['cantidad_dias'], 2)),
                format='%(id)s')

db.define_table('costo_proyectos',
                Field('numero_proyecto', 'reference proyectos'),
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('sumatoria_costo_personal', 'float', label='Sumatoria costo personal y Proveedor'),
                Field('sumatoria_costo_productos', 'float'),
                Field('costo_financiero', 'float'),
                Field('otro_costo', 'float'),
                Field('total_costo_directo_proyectos', compute=lambda r: round(
                    r['sumatoria_costo_personal'] + r['sumatoria_costo_productos'] + r['costo_financiero'] + r[
                        'otro_costo'], 2)),
                format='%(id)s')

# ===============================================================
# ===============================================================

db.define_table('operaciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('numero_proyecto', 'reference proyectos', writable=False),
                Field('numero_cotizacion', 'reference cotizaciones', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('servicio_producto_cotizacion', 'reference servicios_productos', writable=False),
                Field('contacto_proyecto', 'reference contactos'),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('distrito', 'reference distritos', label='Distrito de Atencion', writable=False),
                #                Field('tipo_ejecucion', 'reference tipos_ejecuciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_ejecuciones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('estado_operacion', 'reference estados_operaciones', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_operaciones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('personal_proyecto', 'reference personal',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.personal, '%(persona)s', zero='---- Elegir Opcion ----'))),
                Field('avance_estado_proyecto', 'string', label='Porcentaje Avance Proyecto'),
                Field('coordinaciones', 'text'),
                # Field('fecha_aprobacion', 'date'),
                # Field('fecha_inicio', 'date'),
                # Field('fecha_prueba_control', 'date'),
                # Field('fecha_finalizacion', 'date'),
                # Field('fecha_envio_informe', 'date'),
                Field('personal_proyecto', 'reference personal', writable=False),
                Field('proveedor', 'reference proveedores', label='Proveedor Proyecto',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.proveedores, '%(proveedor)s', zero='---- Elegir Opcion ----'))),
                Field('nota', 'text'),
                format='%(id)s')

db.operaciones.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('post_venta',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('numero_proyecto', 'reference proyectos', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('servicio_producto_cotizacion', 'reference servicios_productos', writable=False),
                Field('contacto_proyecto', 'reference contactos', writable=False),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('estado_post_venta', 'reference estados_post_venta', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_post_venta, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_entrega', 'date', label='Fecha Entrega Proyecto', writable=False),
                Field('fecha_encuesta', 'date'),
                Field('nivel_satisfaccion', 'reference encuestas_satisfaccion', label=T('Nivel de Satisfaccion'),
                      requires=IS_EMPTY_OR(
                          IS_IN_DB(db, db.encuestas_satisfaccion, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('capacidad_solucion_respuesta', 'reference encuestas_satisfaccion',
                      label=T('Capacidad de Solucion y Respuesta'),
                      requires=IS_EMPTY_OR(
                          IS_IN_DB(db, db.encuestas_satisfaccion, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('competencia_tecnica_profesional', 'reference encuestas_satisfaccion',
                      label=T('Competencia Tecnica Profesional'),
                      requires=IS_EMPTY_OR(
                          IS_IN_DB(db, db.encuestas_satisfaccion, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('nivel_comunicacion_cortesia', 'reference encuestas_satisfaccion',
                      label=T('Nivel de Comunicacion y Cortesia'),
                      requires=IS_EMPTY_OR(
                          IS_IN_DB(db, db.encuestas_satisfaccion, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_ultimo_monitoreo', 'date'),
                Field('nota', 'text'),
                format='%(id)s')

db.post_venta.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('facturacion',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('numero_comprobante', 'string'),
                Field('documento_facturacion', 'upload'),
                Field('tipo_comprobante', 'reference tipos_comprobantes', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_comprobantes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('numero_cotizacion', 'reference cotizaciones', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('razon_social_gg', 'reference razones_zociales_gg', writable=False),
                Field('servicio_producto_cotizacion', 'reference servicios_productos', writable=False),
                Field('contacto_facturacion', 'reference contactos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.contactos, '%(contacto_cliente)s', zero='---- Elegir Opcion ----'))),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('fecha_facturacion', 'date'),
                Field('estado_facturacion', 'reference estados_facturacion', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_facturacion, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('moneda', 'reference monedas',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.monedas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                # Field('razon_social_nombre_completo_cliente', 'string'),
                # Field('ruc_dni_ce_cliente', 'string'),
                # Field('direccion_fiscal_cliente', 'string'),
                #Field('periodo_venta', 'string'),
                Field('detraccion', 'boolean'),
                Field('fecha_recepcion', 'date'),
                # Field('sub_total', 'float',writable=False),
                # Field('descuento', 'float',writable=False),
                Field('sub_total_venta', 'float', writable=False),
                Field('impuesto_igv', 'float', writable=False),
                Field('total_venta', type='decimal(10,2)', writable=False),
                Field('porcentaje_detraccion', type='decimal(10,2)', default=0),
                Field('monto_detraccion',
                      compute=lambda r: redondear((r['total_venta'] * r['porcentaje_detraccion']) / 100)),
                Field('nota', 'text'),
                Field.Virtual('validacion',
                              lambda r: advertencia(r.facturacion.numero_cotizacion, r.facturacion.total_venta)),
                format='%(numero_comprobante)s')

db.facturacion.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('facturacion_items',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_comprobante', 'reference facturacion'),
                Field('numero_item', 'string'),
                Field('cantidad', 'float'),
                Field('descripcion', 'string'),
                Field('precio_unitario', 'float'),
                Field('importe', 'float', compute=lambda r: round(r['cantidad'] * r['precio_unitario'], 2)),
                format='%(numero_comprobante)s')

db.define_table('cuentas_por_cobrar_y_pagar',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('marca', 'reference marcas_definiciones'),
                Field('razon_social_gg', 'reference razones_zociales_gg', writable=False),
                Field('saldo_total_cobrar', 'float'),
                Field('saldo_total_pagar', 'float'),
                format=lambda r: r.marca.marca)

db.define_table('gastos_generales',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket_gasto', 'string'),
                Field('personal_gasto', 'reference personal', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.personal, '%(persona)s', zero='---- Elegir Opcion ----'))),
                Field('proveedor_gasto', 'reference proveedores', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.proveedores, '%(proveedor)s', zero='---- Elegir Opcion ----'))),
                Field('razon_social_gg', 'reference razones_zociales_gg', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s', zero='---- Elegir Opcion ----'))),
                Field('marca_gg', 'reference marcas_definiciones', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.marcas_definiciones, '%(marca)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_centro_costo', 'reference tipos_centros_costos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_centros_costos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('numero_centro_costo_proyecto', 'string'),
                Field('fecha_ticket_gasto', 'date'),
                Field('tipo_pago', 'reference tipos_pagos',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_pagos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('moneda', 'reference monedas',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.monedas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('detalle_ticket_gasto', 'string'),
                Field('detraccion', 'boolean'),
                Field('porcentaje_detraccion', 'double'),
                # Field('total_venta', 'double'),
                Field('monto_detraccion', 'double',
                      compute=lambda r: round(r['monto_total_gasto'] * r['porcentaje_detraccion'] / 100, 2)),
                Field('fecha_recepcion', 'date'),
                Field('monto_sub_total', 'double'),
                Field('monto_impuesto', 'double'),
                Field('monto_total_gasto', 'double'),
                Field('nota', 'string'),
                format='%(numero_ticket_gasto)s')

db.define_table('inversiones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket_inversion', 'string'),
                Field('razon_social_gg', 'reference razones_zociales_gg'),
                Field('marca_gg', 'reference marcas_definiciones', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.marcas_definiciones, '%(marca)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_inversion', 'reference tipos_inversiones', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_inversiones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_ticket_inversion', 'date'),
                Field('tipo_pago', 'reference tipos_pagos',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_pagos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('moneda', 'reference monedas',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.monedas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('numero_boucher', 'string'),
                Field('detalle_inversion', 'string'),
                Field('monto_inversion', 'float'),
                Field('nota', 'string'),
                format='%(id)s')

db.define_table('cuentas_por_cobrar',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket', 'reference tickets'),
                Field('numero_comprobante', 'reference facturacion'),
                Field('tipo_comprobante', 'reference tipos_comprobantes', writable=False),
                Field('cliente', 'reference clientes', writable=False),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('razon_social_gg', 'reference razones_zociales_gg',writable=False, readable=False),
                Field('contacto_cobranzas', 'reference contactos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.contactos, '%(contacto_cliente)s', zero='---- Elegir Opcion ----'))),
                Field('estado_cuenta_cobrar', 'reference estados_cuentas_cobrar', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_cuentas_cobrar, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('monto_adelanto_inicial', 'float', default=0),
                Field('monto_adelanto_parcial', 'float', default=0),
                Field('sub_total_venta', 'float', writable=False),
                Field('impuesto_igv', 'float', writable=False),
                Field('monto_detraccion', 'double', writable=False),
                Field('total_venta', 'float', writable=False),
                #                Field('saldo_por_cobrar', compute=lambda r: cobrar_calculo((r['total_venta'] - (r['monto_adelanto_inicial'] + r['monto_adelanto_parcial'] + float(r['monto_detraccion']))))),
                Field('saldo_por_cobrar', compute=lambda r: cobrar_calculo(r['total_venta'],
                                                                           r['monto_adelanto_inicial'],
                                                                           r['monto_adelanto_parcial'],
                                                                           r['monto_detraccion'])),
                Field('tipo_pago', 'reference tipos_pagos',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_pagos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_cobranza', 'date'),
                Field('fecha_cobranza_realizado', 'date'),
                Field('nota', 'string'),
                Field.Virtual('validacion',
                              lambda r: advertencia_aux(r.cuentas_por_cobrar.numero_comprobante,
                                                        r.cuentas_por_cobrar.total_venta)),
                format='%(numero_comprobante)s'
                )

db.cuentas_por_cobrar.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('cuentas_por_pagar',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_ticket_gasto', 'reference gastos_generales', label=T('Numero Ticket Gasto')),
                Field('personal_por_pagar', 'reference personal', writable=False),
                Field('proveedor_por_pagar', 'reference proveedores', writable=False),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('marca_gg', 'reference marcas_definiciones', writable=False),
                Field('razon_social_gg', 'reference razones_zociales_gg', writable=False),
                # Field('numero_comprobante', 'string'),
                Field('monto_detraccion', 'double', default=0, writable=False),
                Field('estado_cuenta_pagar', 'reference estados_cuentas_pagar', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_cuentas_pagar, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('monto_parcial_a_cuenta', 'float', default=0),
                Field('monto_saldo_por_pagar', 'float', default=0),
                Field('tipo_pago', 'reference tipos_pagos',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_pagos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_de_pago', 'date'),
                Field('fecha_pago_realizado', 'date'),
                Field('nota', 'string'),
                format='%(numero_comprobante)s'
                )

db.define_table('cobranzas',
                Field('fecha_registro', 'date', default=now, writable=False),
                # Field('numero_ticket', 'reference tickets'),
                Field('marca', 'reference cuentas_por_cobrar_y_pagar'),
                Field('numero_comprobante', 'reference facturacion', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.facturacion, '%(numero_comprobante)s', zero='---- Elegir Opcion ----'))),
                Field('cliente', 'reference clientes',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.clientes, '%(cliente)s', zero='---- Elegir Opcion ----'))),
                Field('contacto_cliente', 'reference contactos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.contactos, '%(contacto_cliente)s', zero='---- Elegir Opcion ----'))),
                Field('numero_cotizacion', 'reference cotizaciones', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.cotizaciones, '%(numero_cotizacion)s', zero='---- Elegir Opcion ----'))),
                Field('estado_cuenta_cobrar', 'reference estados_cuentas_cobrar', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_cuentas_cobrar, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('sub_total_venta', 'float'),
                Field('impuesto_igv', 'float'),
                Field('total_venta', 'float'),
                Field('monto_detraccion', compute=lambda r: round(r['total_venta'] * 0.10, 2)),
                Field('monto_adelanto_parcial', 'float'),
                Field('monto_saldo_por_cobrar', 'float'),
                Field('estado_producto', 'reference estados_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_productos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_vencimiento_saldo_por_cobrar', 'date'),
                Field('fecha_cobranza', 'date'),
                Field('nota', 'text'),
                format='%(prefijo_comprobante)s %(numero_comprobante)s')

# ===========================================================
# ===========================================================

# db.define_table('gastos_inversiones',
#                 Field('razon_social_gg', 'reference razones_zociales_gg', label=T('Razon Social GG')),
#                 Field('nota', 'text'),
#                 format=lambda r: r.razon_social_gg.razon_social_gg)




# db.define_table('inversione',
#                 Field('razon_social_gg', 'reference gastos_inversiones'),
#                 Field('tipo_inversion', 'reference tipos_inversiones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_inversiones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('numero_boucher', 'string'),
#                 Field('fecha_mes_inversion', 'date'),
#                 Field('detalle_inversion', 'string'),
#                 Field('monto_inversion', 'float'),
#                 Field('nota', 'string'),
#                 format='%(id)s')

# ===========================================================
# ===========================================================

db.define_table('pagos',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('marca', 'reference cuentas_por_cobrar_y_pagar', required=False),
                # Field('numero_recibo', 'reference gastos_generales',requires=IS_EMPTY_OR(IS_IN_DB(db, db.gastos_generales, '%(numero_recibo)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_recibo', 'reference tipos_recibos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_recibos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_centro_costo', 'reference tipos_centros_costos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_centros_costos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('numero_centro_costo', 'string'),
                Field('estado_cuenta_pagar', 'reference estados_cuentas_pagar', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_cuentas_pagar, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_pago', 'reference tipos_pagos',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_pagos, '%(nombre)s', zero='---- Elegir Opcion ----')),
                      label=T('Tipo de Pago')),
                Field('detalle_recibo', 'string'),
                Field('fecha_recibo', 'date'),
                Field('moneda', 'reference monedas',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.monedas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_recepcion_recibo', 'date'),
                Field('detraccion', 'boolean'),
                Field('pago_detraccion', compute=lambda r: round(r['pago_total'] * 0.10, 2)),
                Field('pago_sub_total', 'float'),
                Field('pago_impuesto', 'float'),
                Field('pago_total', 'float'),
                Field('pago_adelanto_parcial_a_cuenta', 'float'),
                Field('saldo_por_pagar', 'float'),
                Field('fecha_maximo_pago', 'date'),
                Field('fecha_pago_total', 'date'),
                Field('nota', 'text'),
                format='%(prefijo_comprobante)s %(numero_comprobante)s')

db.define_table('cuentas_bancarias',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_cuenta', 'string'),
                Field('razon_social_gg', 'reference razones_zociales_gg', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.razones_zociales_gg, '%(razon_social_gg)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_cuenta', 'reference tipos_cuentas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_cuentas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('moneda', 'reference monedas',
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.monedas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('banco', 'string'),
                Field('codigo_interbancario', 'string'),
                Field('sectorista', 'string'),
                Field('telefono', 'string'),
                Field('email', 'string'),
                Field('nota', 'string'),
                format='%(numero_cuenta)s')

db.define_table('transacciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_cuenta', 'reference cuentas_bancarias'),
                Field('fecha_movimiento', 'date'),
                Field('titular_transaccion', 'string'),
                Field('tipo_transaccion', 'reference tipos_transacciones', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_transacciones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('numero_transaccion', 'string'),
                Field('monto_transaccion', 'float'),
                Field('estado_cheque', 'reference estados_cheques', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_cheques, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('detalle_transaccion', 'string'),
                Field('nota', 'text'),
                format='%(id)s')

# db.define_table('cuenta2',
#                 Field('cuenta_bancaria', 'reference cuentas_bancarias'),
#                 Field('fecha_movimiento', 'date'),
#                 Field('tipo_transaccion', 'reference tipos_transacciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_transacciones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('numero_transaccion', 'string'),
#                 Field('estado_cheque', 'reference estados_cheques',requires=IS_EMPTY_OR(IS_IN_DB(db, db.estados_cheques, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('detalle_transaccion', 'string'),
#                 Field('monto_transaccion', 'float'),
#                 Field('nota', 'text'),
#                 format='%(id)s')
#
# db.define_table('cuenta3',
#                 Field('cuenta_bancaria', 'reference cuentas_bancarias'),
#                 Field('fecha_movimiento', 'date'),
#                 Field('tipo_transaccion', 'reference tipos_transacciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_transacciones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('numero_transaccion', 'string'),
#                 Field('estado_cheque', 'reference estados_cheques',requires=IS_EMPTY_OR(IS_IN_DB(db, db.estados_cheques, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('detalle_transaccion', 'string'),
#                 Field('monto_transaccion', 'float'),
#                 Field('nota', 'text'),
#                 format='%(id)s')
#
# db.define_table('cuenta4',
#                 Field('cuenta_bancaria', 'reference cuentas_bancarias'),
#                 Field('fecha_movimiento', 'date'),
#                 Field('tipo_transaccion', 'reference tipos_transacciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_transacciones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('numero_transaccion', 'string'),
#                 Field('estado_cheque', 'reference estados_cheques',requires=IS_EMPTY_OR(IS_IN_DB(db, db.estados_cheques, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('detalle_transaccion', 'string'),
#                 Field('monto_transaccion', 'float'),
#                 Field('nota', 'text'),
#                 format='%(id)s')

# db.define_table('saldos_bancos',
#         Field('cuenta_bancaria', 'reference cuentas_bancarias'),
#         Field('saldo', 'float'),
#     format='%(id)s')



db.define_table('proyectos_mano_obra',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_proyecto', 'reference proyectos'),
                Field('persona', 'reference personal'),
                Field('posicion_rol', 'reference posiciones_roles', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.posiciones_roles, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('tarifa', 'float'),
                Field('tiempo', 'integer'),
                Field('total', 'float'),
                format='%(id)s')

db.define_table('proyectos_materiales_equipos',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_proyecto', 'reference proyectos'),
                Field('item', 'reference items_productos'),
                Field('unidad_medida', 'reference unidades_medidas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.unidades_medidas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('precio', 'float'),
                Field('cantidad', 'float'),
                Field('total', 'float'),
                format='%(id)s')

db.define_table('gastos',
                Field('fecha', 'date'),
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('razon_social', 'reference razones_sociales'),
                Field('proveedor', 'reference proveedores', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.proveedores, '%(proveedor)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_gasto', 'reference tipos_gastos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_gastos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('centro_costo', 'string'),
                Field('tipo_comprobante', 'reference tipos_comprobantes', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_comprobantes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('prefijo_comprobante', 'string'),
                Field('numero_comprobante', 'string'),
                Field('detalle', 'string'),
                Field('monto', 'float'),
                format='%(id)s')

# db.define_table('inversiones',
#                 Field('fecha', 'date'),
#                 Field('razon_social', 'reference razones_sociales'),
#                 Field('tipo_inversion', 'reference tipos_inversiones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_inversiones, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('centro_costo', 'string'),
#                 Field('socio', 'string'),
#                 Field('tipo_comprobante', 'reference tipos_comprobantes',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_comprobantes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('prefijo_comprobante', 'string'),
#                 Field('numero_comprobante', 'string'),
#                 Field('detalle', 'string'),
#                 Field('monto', 'float'),
#                 format='%(id)s')


db.define_table('herramienta_epp',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('persona', 'reference personal'),
                Field('epp_herramienta', 'reference epps_herramientas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.epps_herramientas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('nota', 'text', label=T('Nota')),
                format='%(epp_herramienta)s')

# ===========================================================
db.define_table('inventario_productos',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('producto', 'string'),
                # Field('marca_gg', 'reference marcas_definiciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.marcas_definiciones, '%(marca)s', zero='---- Elegir Opcion ----'))),
                Field('tipo_producto', 'reference tipos_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.tipos_productos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('unidad_medida', 'reference unidades_medidas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.unidades_medidas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('precio_referencial', 'float'),
                # Field('saldo_total', 'float'),
                Field('nota', 'string'),
                format='%(producto)s')

db.define_table('productos',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('numero_proyecto', 'reference proyectos'),
                Field('producto', 'reference inventario_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.inventario_productos, '%(producto)s', zero='---- Elegir Opcion ----'))),
                Field('unidad_medida', 'reference unidades_medidas', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.unidades_medidas, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('cantidad_producto', 'float'),
                Field('precio_unitarios', 'float'),
                Field('costo_productos', compute=lambda r: round(r['cantidad_producto'] * r['precio_unitarios'], 2)),
                format='%(id)s')

db.define_table('asignacion_personal_inventario',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('producto', 'reference inventario_productos'),
                Field('personal', 'reference personal'),
                Field('cantidad_producto', 'float'),
                Field('estado_producto', 'reference estados_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_productos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_asignacion', 'date'),
                format='%(id)s')

db.define_table('proveedor_asignacion',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('producto', 'reference inventario_productos'),
                Field('proveedor', 'reference proveedores', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.proveedores, '%(proveedor)s', zero='---- Elegir Opcion ----'))),
                Field('cantidad_producto', 'float'),
                Field('estado_producto', 'reference estados_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_productos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_asignacion', 'date'),
                format='%(id)s')

db.define_table('almacenes',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('ubicacion_almacen', 'string'),
                Field('producto', 'reference inventario_productos'),
                Field('stock', 'string'),
                Field('estado_producto', 'reference estados_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_productos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_inventario', 'date'),
                format='%(id)s')

# =======================================================
db.define_table('asignacion_proveedor',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('proveedor', 'reference proveedores'),
                Field('producto', 'reference inventario_productos'),
                # Field('tipo_producto', 'reference tipos_productos'),
                # Field('cantidad_producto', 'float'),
                # Field('estado_producto', 'string'),
                Field('estado_producto', 'reference estados_productos', requires=IS_EMPTY_OR(
                    IS_IN_DB(db, db.estados_productos, '%(nombre)s', zero='---- Elegir Opcion ----'))),
                Field('fecha_asignacion', 'date'),
                format='%(id)s')

db.define_table('personal_asignaciones',
                Field('fecha_registro', 'date', default=now, writable=False),
                Field('fecha', 'date', default=now),
                Field('personal', 'reference personal'),
                Field('producto', db.inventario_productos),
                Field('fecha_asignacion', 'date'),
                format='%(id)s')

db.define_table('requerimientos',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
#                Field('fecha', 'date', default=now),
                Field('cliente', 'reference clientes'),
                Field('referencia', 'string', label='Referencia Cliente'),
                Field('area_negocio', 'reference areas_negocios',writable=False),
                Field('contacto', 'reference contactos', label='Contacto Requerimiento'),
                Field('distrito', 'reference distritos', label='Distrito Requerimiento'),
                Field('marca', 'reference marcas_definiciones', label='Marca Requerimiento'),
                Field('requerimiento', 'text'),
                Field('detalle_requerimiento', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('analisis_requerimiento', 'string'),
                Field('ticket_asignado', 'string'),
                Field('nota', 'text'),
                format='%(id)s')

db.define_table('cotizaciones_enviadas',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
#                Field('fecha', 'date', default=now),
                Field('numero_ticket', 'reference tickets'),
                Field('cliente', 'reference clientes', writable=False),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('cotizacion', 'reference cotizaciones', writable=False),
                Field('marca', 'reference marcas_definiciones', writable=False),
                Field('tipo_envio', 'string'),
                Field('nota', 'text'),
                format='%(id)s')

db.cotizaciones_enviadas.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('cotizaciones_aprobadas',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
#                Field('fecha', 'date', default=now),
                Field('numero_ticket', 'reference tickets'),
                Field('cliente', 'reference clientes', writable=False),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('cotizacion', 'reference cotizaciones', writable=False),
                Field('marca', 'reference marcas_definiciones', writable=False),
                Field('nota', 'text'),
                format='%(id)s')

db.cotizaciones_aprobadas.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('proyectos_entregados',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
#                Field('fecha', 'date', default=now),
                Field('numero_ticket', 'reference tickets'),
                Field('cliente', 'reference clientes', writable=False),
                Field('referencia', 'string', label='Referencia Cliente', writable=False),
                Field('cotizacion', 'reference cotizaciones'),
                Field('proyecto', 'reference proyectos'),
                Field('marca', 'reference marcas_definiciones'),
                Field('nota', 'text'),
                format='%(id)s')

db.proyectos_entregados.numero_ticket.widget = SQLFORM.widgets.autocomplete(
    request, db.tickets.id, id_field=db.tickets.id)

db.define_table('personal_operaciones',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
#                Field('fecha', 'date', default=now),
                Field('personal', 'reference personal'),
                Field('nota', 'text'),
                format='%(id)s')

db.define_table('proveedor_operaciones',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
#                Field('fecha', 'date', default=now),
                Field('proveedor', 'reference proveedores'),
                Field('nota', 'text'),
                format='%(id)s')

db.define_table('reporte_diario',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
#                Field('fecha', 'date', default=now),
                Field('personal', 'reference personal'),
                Field('reporte', 'text', label='Reporte Diario'),
                Field('nota', 'string'),
                format='%(id)s')

db.define_table('bitacora',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
                Field('personal', 'reference personal'),
                Field('apunte', 'text'),
                Field('detalle_apunte', 'string'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(id)s')

db.define_table('tareas',
                Field('fecha_registro', 'date', label='Fecha Registro', default=now, writable=False),
                Field('fecha_calendario', 'date', label='Fecha Calendario', default=now),
                Field('personal', 'reference personal'),
                Field('tarea', 'text'),
                #Field('detalle_tarea', 'string'),
                Field('cumplimiento', 'boolean'),
                Field('documento_adjunto', 'upload'),
                Field('nota', 'text'),
                format='%(id)s')



# db.define_table('resultados',
#                 Field('razon_social_gg', 'reference razones_zociales_gg'),
#                 Field('nota', 'string'),
#                 format='%(razon_social_gg)s')
#
#
# db.define_table('registros_ventas',
#                 Field('numero_ticket', 'reference tickets',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tickets, '%(id)s', zero='---- Elegir Opcion ----'))),
#                 Field('razon_social_gg', 'reference resultados'),
#                 Field('numero_cotizacion', 'reference cotizaciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.cotizaciones, '%(numero_cotizacion)s', zero='---- Elegir Opcion ----'))),
#                 Field('fecha_aprobacion', 'date'),
#                 Field('cliente', 'reference clientes',requires=IS_EMPTY_OR(IS_IN_DB(db, db.clientes, '%(cliente)s', zero='---- Elegir Opcion ----'))),
#                 Field('marca_gg', 'reference marcas_definiciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.marcas_definiciones, '%(marca)s', zero='---- Elegir Opcion ----'))),
#                 Field('tipo_cliente', 'reference tipos_clientes',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_clientes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('sub_total_venta', 'float'),
#                 Field('impuesto_igv', 'float'),
#                 Field('total_venta', 'float'),
#                 Field('motivo_aprobacion', 'string'),
#                 Field('nota', 'string'),
#                 format='%(id)s')
#
#
# db.define_table('registros_cancelaciones',
#                 Field('numero_ticket', 'reference tickets',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tickets, '%(id)s', zero='---- Elegir Opcion ----'))),
#                 Field('razon_social_gg', 'reference resultados'),
#                 Field('numero_cotizacion', 'reference cotizaciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.cotizaciones, '%(numero_cotizacion)s', zero='---- Elegir Opcion ----'))),
#                 Field('fecha_cancelacion', 'date'),
#                 Field('cliente', 'reference clientes'),
#                 Field('marca_gg', 'reference marcas_definiciones',requires=IS_EMPTY_OR(IS_IN_DB(db, db.marcas_definiciones, '%(marca)s', zero='---- Elegir Opcion ----'))),
#                 Field('tipo_cliente', 'reference tipos_clientes',requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_clientes, '%(nombre)s', zero='---- Elegir Opcion ----'))),
#                 Field('sub_total_venta', 'float'),
#                 Field('impuesto_igv', 'float'),
#                 Field('total_venta', 'float'),
#                 Field('motivo_cancelacion', 'string'),
#                 Field('nota', 'string'),
#                 format='%(id)s')



db.proyectos.numero_cotizacion.represent = lambda value, row: '' if value is None else name_data(db.cotizaciones, value,
                                                                                                 'numero_cotizacion'
                                                                                                 )
db.presupuestos.numero_cotizacion.represent = lambda value, row: '' if value is None else name_data(db.cotizaciones,
                                                                                                    value,
                                                                                                    'numero_cotizacion'
                                                                                                    )
db.negociaciones.numero_cotizacion.represent = lambda value, row: '' if value is None else name_data(db.cotizaciones,
                                                                                                     value,
                                                                                                     'numero_cotizacion'
                                                                                                     )
db.negociaciones.servicio_producto_cotizacion.represent = lambda value, row: '' if value is None else name_data(db.servicios_productos,
                                                                                                     value,
                                                                                                     'servicio_producto'
                                                                                                     )
db.negociaciones.contacto_cotizacion.represent = lambda value, row: '' if value is None else name_data(db.contactos,
                                                                                                     value,
                                                                                                     'contacto_cliente'
                                                                                                     )
db.negociaciones.estado_negociacion.represent = lambda value, row: '' if value is None else name_data(db.estados_negociacion,
                                                                                                     value,
                                                                                                     'nombre'
                                                                                                     )
db.facturacion.numero_cotizacion.represent = lambda value, row: '' if value is None else name_data(db.cotizaciones,
                                                                                                   value,
                                                                                                   'numero_cotizacion'
                                                                                                   )

#db.tickets._after_insert.append(lambda values, id: insert_tickets(values, id, 'tickets'))
#
db.visitas._after_update.append(lambda identifier, values: data_update(identifier, values, 'visitas'))
db.visitas._after_insert.append(lambda values, id: data_insert(values, id, 'visitas'))
#################################################
db.reportes._after_update.append(lambda identifier, values: data_update(identifier, values, 'reportes'))
db.reportes._after_insert.append(lambda values, id: data_insert(values, id, 'reportes'))
#################################################
db.cotizaciones._after_update.append(lambda identifier, values: data_update(identifier, values, 'cotizaciones'))
db.cotizaciones._after_insert.append(lambda values, id: data_insert(values, id, 'cotizaciones'))
#################################################
db.presupuestos._after_update.append(lambda identifier, values: data_update(identifier, values, 'presupuestos'))
db.presupuestos._after_insert.append(lambda values, id: data_insert(values, id, 'presupuestos'))
#################################################
db.negociaciones._after_update.append(lambda identifier, values: data_update(identifier, values, 'negociaciones'))
db.negociaciones._after_insert.append(lambda values, id: data_insert(values, id, 'negociaciones'))
db.negociaciones._after_insert.append(lambda values, id: data_insert_negociaciones(values, id, 'negociaciones'))
db.negociaciones._after_update.append(
    lambda identifier, values: data_update_negociaciones(identifier, values, 'negociaciones'))
#################################################
db.post_venta._after_update.append(lambda identifier, values: data_update(identifier, values, 'post_venta'))
db.post_venta._after_insert.append(lambda values, id: data_insert(values, id, 'post_venta'))
db.post_venta._after_insert.append(lambda values, id: data_insert_proyecto(values, id, 'post_venta'))
db.post_venta._after_update.append(lambda identifier, values: data_update_proyecto(identifier, values, 'post_venta'))
#################################################
db.facturacion._after_update.append(lambda identifier, values: data_update(identifier, values, 'facturacion'))
db.facturacion._after_insert.append(lambda values, id: data_insert(values, id, 'facturacion'))
# db.facturacion._after_insert.append(lambda values, id: data_insert_cliente(values, id, 'facturacion'))
db.facturacion._after_insert.append(lambda values, id: data_insert_cotizacion_calc(values, id, 'facturacion'))
db.facturacion._after_update.append(
    lambda identifier, values: data_update_cotizacion_calc(identifier, values, 'facturacion'))
db.facturacion._after_insert.append(lambda values, id: data_insert_montos(values, id, 'facturacion'))
db.facturacion._after_update.append(lambda identifier, values: data_insert_montos(identifier, values, 'facturacion'))
#################################################
db.cuentas_por_cobrar._after_update.append(
    lambda identifier, values: data_update(identifier, values, 'cuentas_por_cobrar'))
db.cuentas_por_cobrar._after_insert.append(lambda values, id: data_insert(values, id, 'cuentas_por_cobrar'))
db.cuentas_por_cobrar._after_insert.append(lambda values, id: data_insert_facturacion(values, id, 'cuentas_por_cobrar'))
db.cuentas_por_cobrar._after_update.append(
    lambda identifier, values: data_update_facturacion(identifier, values, 'cuentas_por_cobrar'))
#################################################
db.cuentas_por_pagar._after_update.append(
    lambda identifier, values: data_update_cuentas_x_pagar(identifier, values, 'cuentas_por_pagar'))
db.cuentas_por_pagar._after_insert.append(
    lambda values, id: data_insert_cuentas_x_pagar(values, id, 'cuentas_por_pagar'))
# db.cuentas_por_pagar._after_insert.append(lambda values, id: data_insert_fact(values, id, 'cuentas_por_pagar'))
# db.cuentas_por_pagar._after_update.append(lambda identifier, values: data_update_fact(identifier, values, 'cuentas_por_pagar'))
#################################################
db.presupuestos._after_insert.append(lambda values, id: data_insert_cotizacion(values, id, 'presupuestos'))
db.presupuestos._after_update.append(lambda identifier, values: data_update_cotizaciones(identifier, values, 'presupuestos'))

db.proyectos._after_insert.append(lambda values, id: data_insert(values, id, 'proyectos'))
db.proyectos._after_update.append(lambda identifier, values: data_update(identifier, values, 'proyectos'))

db.proyectos._after_insert.append(lambda values, id: data_insert_cotizacion(values, id, 'proyectos'))
db.proyectos._after_update.append(lambda identifier, values: data_update_cotizaciones(identifier, values, 'proyectos'))

db.proyectos._after_insert.append(lambda values, id: data_insert_neg(values, id, 'proyectos'))
db.proyectos._after_update.append(lambda identifier, values: data_update_neg(identifier, values, 'proyectos'))

db.operaciones._after_insert.append(lambda values, id: data_insert(values, id, 'operaciones'))
db.operaciones._after_update.append(lambda identifier, values: data_update(identifier, values, 'operaciones'))
db.operaciones._after_insert.append(lambda values, id: data_insert_cotizacion_ope(values, id, 'operaciones'))
db.operaciones._after_update.append(
    lambda identifier, values: data_update_cotizaciones_ope(identifier, values, 'operaciones'))
db.operaciones._after_insert.append(lambda values, id: data_insert_proyecto(values, id, 'operaciones'))
db.operaciones._after_update.append(lambda identifier, values: data_update_proyecto(identifier, values, 'operaciones'))

db.cotizaciones_enviadas._after_insert.append(lambda values, id: data_insert_values(values, id, 'cotizaciones_enviadas'))
db.cotizaciones_enviadas._after_update.append(lambda identifier, values: data_update_values(identifier, values, 'cotizaciones_enviadas'))
db.cotizaciones_aprobadas._after_insert.append(lambda values, id: data_insert_values(values, id, 'negociaciones_aprobadas'))
db.cotizaciones_aprobadas._after_update.append(lambda identifier, values: data_update_values(identifier, values, 'negociaciones_aprobadas'))
db.proyectos_entregados._after_insert.append(lambda values, id: data_insert_values(values, id, 'proyectos_entregados'))
db.proyectos_entregados._after_update.append(lambda identifier, values: data_update_values(identifier, values, 'proyectos_entregados'))

db.cotizaciones_aprobadas._after_insert.append(lambda values, id: data_insert_values(values, id, 'cotizaciones_aprobadas'))
db.cotizaciones_aprobadas._after_update.append(lambda identifier, values: data_update_values(identifier, values, 'cotizaciones_aprobadas'))

db.requerimientos._after_insert.append(lambda values, id: data_insert_requerimientos(values, id, 'requerimientos'))
db.requerimientos._after_update.append(lambda identifier, values: data_update_requerimientos(identifier, values, 'requerimientos'))

db.visitas._after_insert.append(lambda values, id: insert_fecha_ticket(values, id, 'visitas'))
db.visitas._after_update.append(lambda identifier, values: update_fecha_ticket(identifier, values, 'visitas'))

db.presupuestos._after_insert.append(lambda values, id: insert_fecha_cotizacion(values, id, 'presupuestos'))
db.presupuestos._after_update.append(lambda identifier, values: update_fecha_cotizacion(identifier, values, 'presupuestos'))

db.operaciones._after_insert.append(lambda values, id: insert_operaciones_distrito(values, id, 'operaciones'))
db.operaciones._after_update.append(lambda identifier, values: update_operaciones_distrito(identifier, values, 'operaciones'))

db.negociaciones._after_insert.append(lambda values, id: insert_fecha_envio_cotizacion(values, id, 'negociaciones'))
db.negociaciones._after_update.append(lambda identifier, values: update_fecha_envio_cotizacion(identifier, values, 'negociaciones'))


#db.visitas._after_insert.append(lambda values, id: insert_direccion_cliente(values, id, 'visitas'))

db.presupuestos._after_insert.append(lambda values, id: insert_fecha_visitado(values, id, 'presupuestos'))

db.visitas._after_insert.append(lambda values, id: insert_referencia_visita(values, id, 'visitas'))
#db.visitas._after_update.append(lambda identifier, values: update_referencia_visita(identifier, values, 'visitas'))
#db.presupuestos._after_update.append(lambda identifier, values: update_fecha_cotizacion(identifier, values, 'presupuestos'))


db.cuentas_por_pagar._after_insert.append(lambda values, id: insert_data_pagar(values, id, 'cuentas_por_pagar'))

db.presupuestos._after_insert.append(lambda values, id: insert_personal_visitado(values, id, 'presupuestos'))

db.visitas._after_insert.append(lambda values, id: insert_visitas_distrito(values, id, 'visitas'))
db.visitas._after_update.append(lambda identifier, values: update_visitas_distrito(identifier, values, 'visitas'))