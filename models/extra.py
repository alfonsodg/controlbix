# -*- coding: utf-8 -*-


def generate_label(text, name):
    if len(text) <= 0:
        label = name.replace('_', ' ')
        label = label.capitalize()
    else:
        label = text
    return label


def select_block(visual, name, table, width_aux=2, width=4, extra='nombre'):
    data = db(db[table]['id']>0).select(db[table]['id'],db[table][extra])
    label = generate_label(visual, name)
    content = []
    for row in data:
        tmp = '<option value="%s">%s</option>' % (row['id'], row[extra])
        content.append(tmp)
    content = """
    <div class="col-xs-%s">%s</div>
    <div class="col-xs-%s">
        <select class="form-control select select-primary" data-toggle="select" id="%s">
            %s
        </select>
    </div>
    """ % (width_aux, label, width, name, ''.join(content))
    return XML(content)


def date_block(visual, name, width_aux=2, width=4):
    label = generate_label(visual, name)
    content = """
    <div class="col-xs-%s">%s</div>
    <div class="col-xs-%s">
        <div class="form-group has-success">
            <input type="text" class="form-control" id="%s"/>
            <span class="input-icon fui-check-inverted"></span>
        </div>
    </div>
    <script>
    $(function() {
        $( "#%s" ).datepicker(
        {
      changeMonth: true,
      changeYear: true
    }
        );
    });
  </script>
    """ % (width_aux, label, width, name, name)
    return XML(content)


def html_block(visual, name, width_aux=2, width=4):
    label = generate_label(visual, name)
    content = """
    <div class="col-xs-%s">%s</div>
    <div class="col-xs-%s">
        <div class="form-group has-success">
            <input type="text" class="form-control" id="%s"/>
            <span class="input-icon fui-check-inverted"></span>
        </div>
    </div>""" % (width_aux, label, width, name)
    return XML(content)


def mult_block(visual, name, width_aux=2, width=4):
    label = generate_label(visual, name)
    content = """
    <div class="col-xs-%s">%s</div>
    <div class="col-xs-%s">
        <div class="form-group has-success">
            <input type="text" class="form-control" id="%s"/>
            <span class="input-icon fui-check-inverted"></span>
        </div>
    </div>""" % (width_aux, label, width, name)
    return XML(content)

def option_block(visual, name, width_aux=2, width=4):
    label = generate_label(visual, name)
    content = """
    <div class="col-xs-%s">%s</div>
    <div class="col-xs-%s">
        <label class="checkbox" for="%s">
        <input type="checkbox" class="form-control" id="%s" data-toggle="checkbox" value="0">
      </label>
    </div>""" % (width_aux, label, width, name, name)
    return XML(content)


def html_void(width_aux=2, width=4):
    content = """
    <div class="col-xs-%s"></div>
    <div class="col-xs-%s">
        <div class="form-group has-success">
            <input type="text" class="form-control"/>
            <span class="input-icon fui-check-inverted"></span>
        </div>
    </div>""" % (width_aux, width)
    return XML(content)

def html_white(width=4):
    content = """
    <div class="col-xs-%s"></div>
    """ % (width)
    return XML(content)


def js_percentage( name, result, field1, field2):
    content = """
    <script>
    $('#%s').change(js_recalc);
    $('#%s').change(js_recalc);

    function %s() {
            var a = $('#%s').val();
            var b = $('#%s').val();
            var total = (a * b)/100;
            $('#%s').val(total);
    }
        </script>
    """ % (field1, field2, name, field1, field2, result)
    return XML(content)

def js_multx( name, result, field1, field2):
    content = """
    <script>
    $('#%s').change(js_recalc);
    $('#%s').change(js_recalc);

    function %s() {
            var a = $('#%s').val();
            var b = $('#%s').val();
            var total = (a * b);
            $('#%s').val(total);
    }
        </script>
    """ % (field1, field2, name, field1, field2, result)
    return XML(content)

def js_sum(name, result, *fields):
    vars = ['a','b','c','d','e','f']
    content = "<script>\n"
    for field in fields:
        content += "$('#%s').change(js_recalc);\n" % (field)
    content += "function %s(){\n" % name
    count = 0
    tmp = []
    for field in fields:
        content += "var %s = parseFloat($('#%s').val());\n" % (vars[count], field)
        tmp.append(vars[count])
        count += 1
    total = '+'.join(tmp)
    content += "total = %s;\n" % total
    content += "$('#%s').val(total);\n" % result
    #content += "js_recalc()\n";
    content += "}\n"
    # content = """
    # <script>
    # $('#%s').change(compute);
    # $('#%s').change(compute);
    #
    # function compute() {
    #         var a = $('#%s').val();
    #         var b = $('#%s').val();
    #         var total = (a * b)/100;
    #         $('#%s').val(total);
    # }
    #     </script>
    # """ % (field1, field2, field1, field2, result)
    content += "</script>"
    return XML(content)

def js_min(name, result, *fields):
    vars = ['a','b','c','d','e','f']
    content = "<script>\n"
    for field in fields:
        content += "$('#%s').change(js_recalc);\n" % (field)
    content += "function %s(){\n" % name
    count = 0
    tmp = []
    for field in fields:
        content += "var %s = parseFloat($('#%s').val());\n" % (vars[count], field)
        tmp.append(vars[count])
        count += 1
    total = '-'.join(tmp)
    content += "total = %s;\n" % total
    content += "$('#%s').val(total);\n" % result
    #content += "js_recalc()\n";
    content += "}\n"
    # content = """
    # <script>
    # $('#%s').change(compute);
    # $('#%s').change(compute);
    #
    # function compute() {
    #         var a = $('#%s').val();
    #         var b = $('#%s').val();
    #         var total = (a * b)/100;
    #         $('#%s').val(total);
    # }
    #     </script>
    # """ % (field1, field2, field1, field2, result)
    content += "</script>"
    return XML(content)


def js_recalc(*fields):
    content = "<script>\n"
    content += "function js_recalc(){\n"
    for field in fields:
        content += "%s();\n" % (field)
    content += "}\n"
    content += "</script>"
    return XML(content)


def grid_headers(table, data_type, relations):
    fields_names = db[table].fields
    #data_ = sizes
    columns_documentos = []
    cnt = 0
    #people = []
    #print relations
    for row in fields_names:
        field = row
        caption = row
        caption = caption.replace('_', ' ').capitalize()
        #caption = row.capitalize()
        w_size = data_type[cnt][0]
        w_type = data_type[cnt][1]
        if field == 'id':
            field = 'recid'
            caption = 'ID'
        tmp = {
            'field': field,
            'caption': caption,
            'size': '%spx' % w_size,
            'sortable': 'true',
            'resizable': 'true',
        }
        if w_type == 'select':
            if field in relations:
                #print data
                aux_data = relations[field].items()[0][0]
                tmp['editable'] = {
                        'type': w_type,
                        'aux': aux_data,
                        #'items': 'people',
                        #'showAll': 'true'
                }
        # elif w_type == 'file':
        #         tmp['editable'] = {
        #                 'type': 'text',
        #                 'render': """function (record, index, column_index) {
        #             var html = '<div>'+ record.field + ' AAAAAAAAAAAAAAAAAAAAAAAA' '</div>';
        #             return html;
        #         }"""
        #         }
        else:
                tmp['editable'] = {
                        'type': w_type,
                }

        columns_documentos.append(tmp)
        cnt += 1
    #print columns_documentos
    return columns_documentos


def aux_table(relations):
    extra = {}
    for field in relations:
        tmp = []
        related = relations[field].items()[0]
        extra_table = related[0]
        related_field = related[1]
        aux = related_field.split('-')
        if len(aux) > 1:
            key_field = aux[0]
            join_table = aux[1]
            name_field = aux[2]
            data = db(db[extra_table].id>0).select(
                db[extra_table]['id'],
                db[join_table][name_field],
                left=db[join_table].on(db[join_table]['id']==db[extra_table][key_field])
            )
            for row in data:
                tmp.append({'id':int(row[extra_table]['id']), 'text':'{}'.format(row[join_table][name_field])})
            extra[extra_table] = tmp
        else:
            aux_field = aux[0]
            data = db(db[extra_table].id>0).select(
                db[extra_table]['id'],
                db[extra_table][aux_field]
            )
            for row in data:
                tmp.append({'id':int(row['id']), 'text':'{}'.format(row[aux_field])})
            extra[extra_table] = tmp
    return extra



# def my_upload():
#
#     import re
#     fileApp=request.vars.pic
#     filename=fileApp.filename.replace(' ', '')
#     result=''
#     http_host=''
#
#     #Prevent special caracters in the file name
#     expression='[*+~$&^#@!;:,|]'
#     regex = re.compile(expression)
#     if regex.search(filename):
#     result="Special caracters NO!!! Nothing to do..."
#     return response.json('<div class="error_wrapper">\
#             <div id="title__error" class="error" style="display: inline-block;">'\
#             +result+\
#             '</div></div>')
#
#
#
#     aux=db.files.file.store(fileApp, filename)
#     db.files.insert(file=aux,title=filename)
#
#
#     if request.env.http_x_forwarded_host:
#       http_host = request.env.http_x_forwarded_host.split(':',1)[0]
#     else:
#       http_host = request.env.http_host
#
#
#     last = db().select(db.files.ALL)[-1]
#     result=T('Successfuly! Here the link: ')
#     result+="<a href=http://"+http_host+'/'+request.application+'/'+request.controller+'/download/'+last.file+">Donwload</a>"
#
#     return response.json('<div class="alert alert-success">'\
#             +result+\
#             '</div>')