# -*- coding: utf-8 -*-

def insert_tickets(values, id, table):
    try:
        cliente = values['cliente']
        cliente_data = db.clientes(db.clientes.id == cliente)
        nombre = cliente_data['razon_social_nombre_completo']
        referencia = "%s-%s" % (id, nombre)
        db[table][id] = dict(referencia=referencia)
    except:
        pass
    return

# def insert_from_tickets(values, id, table, fields):
#     try:
#         ticket = values['numero_ticket']
#         ticket_data = db.tickets(db.tickets.id == ticket)
#         nombre = cliente_data['razon_social_nombre_completo']
#         referencia = "%s-%s" % (id, nombre)
#         db[table][id] = dict(referencia=referencia)
#     except:
#         pass
#     return


def data_insert(values, id, table, fields = ['cliente', 'marca_gg', 'razon_social_gg', 'referencia']):
    #fields = [cliente, marca_gg, razon_social_gg, referencia]
    try:
        ticket = values['numero_ticket']
        ticket_data = db.tickets(db.tickets.id == ticket)
        # cliente = ticket_data['cliente']
        # referencia = ticket_data['referencia']
        # marca_gg = ticket_data['marca_gg']
        # razon_social_gg = ticket_data['razon_social_gg']
        data = {}
        for field in fields:
            data[field] = ticket_data[field]
        #db[table][id] = dict(cliente=cliente, marca_gg=marca_gg, razon_social_gg=razon_social_gg)
        db[table][id] = data
    except:
        pass
    return


def data_update(identifier, values, table):
    try:
        ticket = values['numero_ticket']
    except:
        return
    ticket_data = db.tickets(db.tickets.id == ticket)
    cliente = ticket_data['cliente']
    marca_gg = ticket_data['marca_gg']
    razon_social_gg = ticket_data['razon_social_gg']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(cliente=cliente, marca_gg=marca_gg, razon_social_gg=razon_social_gg)
    except:
        pass


#################################################

def data_insert_cotizacion(values, id, table):
    try:
        ticket_cotizacion = values['numero_ticket']
        data = db.cotizaciones(db.cotizaciones.numero_ticket == ticket_cotizacion)
        numero_cotizacion = data['id']
        contacto_cotizacion = data['contacto_cotizacion']
        servicio_producto_cotizacion = data['servicio_producto_cotizacion']
        fecha_registro_cotizacion = data['fecha_registro_cotizacion']
        db[table][id] = dict(numero_cotizacion=numero_cotizacion, contacto_cotizacion=contacto_cotizacion,
                             servicio_producto_cotizacion=servicio_producto_cotizacion,
                             fecha_registro_cotizacion=fecha_registro_cotizacion)
    except:
        pass
    return


def data_update_cotizaciones(identifier, values, table):
    try:
        ticket_cotizacion = values['numero_ticket']
    except:
        return
    data = db.cotizaciones(db.cotizaciones.numero_ticket == ticket_cotizacion)
    numero_cotizacion = data['id']
    contacto_cotizacion = data['contacto_cotizacion']
    servicio_producto_cotizacion = data['servicio_producto_cotizacion']
    fecha_registro_cotizacion = data['fecha_registro_cotizacion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(numero_cotizacion=numero_cotizacion, contacto_cotizacion=contacto_cotizacion,
                                   servicio_producto_cotizacion=servicio_producto_cotizacion,
                                   fecha_registro_cotizacion=fecha_registro_cotizacion)
    except:
        pass

def data_insert_neg(values, id, table):
    try:
        ticket_cotizacion = values['numero_ticket']
        data = db.negociaciones(db.negociaciones.numero_ticket == ticket_cotizacion)
        fecha_aprobacion = data['fecha_aprobacion']
        db[table][id] = dict(fecha_aprobacion=fecha_aprobacion)
    except:
        pass
    return

def data_update_neg(identifier, values, table):
    try:
        ticket_cotizacion = values['numero_ticket']
    except:
        return
    data = db.negociaciones(db.negociaciones.numero_ticket == ticket_cotizacion)
    fecha_aprobacion = data['fecha_aprobacion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(fecha_aprobacion=fecha_aprobacion)
    except:
        pass


#################################################


def data_insert_proyecto(values, id, table):
    try:
        ticket_proyecto = values['numero_ticket']
        data = db.proyectos(db.proyectos.numero_ticket == ticket_proyecto)
        numero_proyecto = data['id']
        # contacto_proyecto = data['contacto_proyecto']
        fecha_entrega = data['fecha_entrega']
        servicio_producto_cotizacion = data['servicio_producto_cotizacion']
        db[table][id] = dict(numero_proyecto=numero_proyecto,
                             # contacto_proyecto=contacto_proyecto,
                             servicio_producto_cotizacion=servicio_producto_cotizacion,
                             fecha_entrega=fecha_entrega)
    except Exception, error:
        pass
    return


def data_update_proyecto(identifier, values, table):
    try:
        ticket_proyecto = values['numero_ticket']
    except:
        return
    data = db.proyectos(db.proyectos.numero_ticket == ticket_proyecto)
    numero_proyecto = data['id']
    fecha_entrega = data['fecha_entrega']
    # contacto_proyecto = data['contacto_proyecto']
    servicio_producto_cotizacion = data['servicio_producto_cotizacion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(numero_proyecto=numero_proyecto,
                                   # contacto_proyecto=contacto_proyecto,
                                   servicio_producto_cotizacion=servicio_producto_cotizacion,
                                   fecha_entrega=fecha_entrega)
    except:
        pass


#################################################

def data_insert_cotizacion_ope(values, id, table):
    try:
        ticket_cotizacion = values['numero_ticket']
        data = db.cotizaciones(db.cotizaciones.numero_ticket == ticket_cotizacion)
        numero_cotizacion = data['id']
        servicio_producto_cotizacion = data['servicio_producto_cotizacion']
        db[table][id] = dict(numero_cotizacion=numero_cotizacion,
                             servicio_producto_cotizacion=servicio_producto_cotizacion)
    except:
        pass
    return


def data_update_cotizaciones_ope(identifier, values, table):
    try:
        ticket_cotizacion = values['numero_ticket']
    except:
        return
    data = db.cotizaciones(db.cotizaciones.numero_ticket == ticket_cotizacion)
    numero_cotizacion = data['id']
    servicio_producto_cotizacion = data['servicio_producto_cotizacion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(numero_cotizacion=numero_cotizacion,
                                   servicio_producto_cotizacion=servicio_producto_cotizacion)
    except:
        pass


#################################################

def data_insert_negociaciones(values, id, table):
    try:
        ticket_negociacion = values['numero_ticket']
        data = db.cotizaciones(db.cotizaciones.numero_ticket == ticket_negociacion)
        numero_cotizacion = data['id']
        contacto_cotizacion = data['contacto_cotizacion']
        servicio_producto_cotizacion = data['servicio_producto_cotizacion']
        porcentaje_descuento = data['porcentaje_descuento']
        sub_total = data['sub_total']
        descuento = data['descuento']
        sub_total_venta = data['sub_total_venta']
        impuesto_igv = data['impuesto_igv']
        total_venta = data['total_venta']
        fecha_cotizacion = data['fecha_cotizacion']
        db[table][id] = dict(numero_cotizacion=numero_cotizacion,
                             contacto_cotizacion=contacto_cotizacion,
                             servicio_producto_cotizacion=servicio_producto_cotizacion,
                             porcentaje_descuento=porcentaje_descuento,
                             sub_total=sub_total, descuento=descuento,
                             sub_total_venta=sub_total_venta,
                             impuesto_igv=impuesto_igv,
                             total_venta=total_venta,
                             fecha_cotizacion=fecha_cotizacion)
    except:
        pass
    return


def data_update_negociaciones(identifier, values, table):
    try:
        ticket_negociacion = values['numero_ticket']
    except:
        return
    data = db.cotizaciones(db.cotizaciones.numero_ticket == ticket_negociacion)
    numero_cotizacion = data['id']
    contacto_cotizacion = data['contacto_cotizacion']
    servicio_producto_cotizacion = data['servicio_producto_cotizacion']
    porcentaje_descuento = data['porcentaje_descuento']
    sub_total = data['sub_total']
    descuento = data['descuento']
    sub_total_venta = data['sub_total_venta']
    impuesto_igv = data['impuesto_igv']
    total_venta = data['total_venta']
    fecha_cotizacion = data['fecha_cotizacion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(numero_cotizacion=numero_cotizacion,
                                   contacto_cotizacion=contacto_cotizacion,
                                   servicio_producto_cotizacion=servicio_producto_cotizacion,
                                   porcentaje_descuento=porcentaje_descuento,
                                   sub_total=sub_total, descuento=descuento,
                                   sub_total_venta=sub_total_venta,
                                   impuesto_igv=impuesto_igv,
                                   total_venta=total_venta,
                                   fecha_cotizacion=fecha_cotizacion)
    except:
        pass


#################################################


def data_insert_montos(values, id, table):
    try:
        numero_cotizacion = values['numero_cotizacion']
        data = db.cotizaciones(db.cotizaciones.id == numero_cotizacion)
        sub_total_venta = data['sub_total_venta']
        impuesto_igv = data['impuesto_igv']
        total_venta = data['total_venta']
        db[table][id] = dict(
            sub_total_venta=sub_total_venta,
            impuesto_igv=impuesto_igv,
            total_venta=total_venta
        )
    except:
        pass
    return


def data_update_montos(identifier, values, table):
    try:
        numero_cotizacion = values['numero_cotizacion']
    except:
        return
    data = db.cotizaciones(db.cotizaciones.id == numero_cotizacion)
    sub_total_venta = data['sub_total_venta']
    impuesto_igv = data['impuesto_igv']
    total_venta = data['total_venta']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            sub_total_venta=sub_total_venta,
            impuesto_igv=impuesto_igv,
            total_venta=total_venta
        )
    except:
        pass


def data_insert_cotizacion_calc(values, id, table):
    try:
        ticket_cotizacion = values['numero_ticket']
        porcentaje_detraccion = values['porcentaje_detraccion']
        data = db.cotizaciones(db.cotizaciones.numero_ticket == ticket_cotizacion)
        numero_cotizacion = data['id']
        contacto_cotizacion = data['contacto_cotizacion']
        servicio_producto_cotizacion = data['servicio_producto_cotizacion']
        porcentaje_descuento = data['porcentaje_descuento']
        sub_total = data['sub_total']
        descuento = data['descuento']
        sub_total_venta = data['sub_total_venta']
        impuesto_igv = data['impuesto_igv']
        total_venta = data['total_venta']
        monto_detraccion = float(total_venta) * float(porcentaje_detraccion)
        db[table][id] = dict(numero_cotizacion=numero_cotizacion,
                             contacto_cotizacion=contacto_cotizacion,
                             servicio_producto_cotizacion=servicio_producto_cotizacion,
                             porcentaje_descuento=porcentaje_descuento,
                             sub_total=sub_total, descuento=descuento,
                             sub_total_venta=sub_total_venta,
                             impuesto_igv=impuesto_igv,
                             total_venta=total_venta,
                             monto_detraccion=monto_detraccion
                             )
    except:
        pass
    return


def data_update_cotizacion_calc(identifier, values, table):
    try:
        ticket_cotizacion = values['numero_ticket']
    except:
        return
    data = db.cotizaciones(db.cotizaciones.numero_ticket == ticket_cotizacion)
    numero_cotizacion = data['id']
    contacto_cotizacion = data['contacto_cotizacion']
    servicio_producto_cotizacion = data['servicio_producto_cotizacion']
    porcentaje_descuento = data['porcentaje_descuento']
    sub_total = data['sub_total']
    descuento = data['descuento']
    sub_total_venta = data['sub_total_venta']
    impuesto_igv = data['impuesto_igv']
    total_venta = data['total_venta']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(numero_cotizacion=numero_cotizacion,
                                   contacto_cotizacion=contacto_cotizacion,
                                   servicio_producto_cotizacion=servicio_producto_cotizacion,
                                   porcentaje_descuento=porcentaje_descuento,
                                   sub_total=sub_total, descuento=descuento,
                                   sub_total_venta=sub_total_venta,
                                   impuesto_igv=impuesto_igv,
                                   total_venta=total_venta
                                   )
    except:
        pass


#################################################

def data_insert_facturacion(values, id, table):
    try:
        ticket_facturacion = values['numero_ticket']
        data = db.facturacion(db.facturacion.numero_ticket == ticket_facturacion)
        tipo_comprobante = data['tipo_comprobante']
        numero_comprobante = data['id']
        sub_total_venta = data['sub_total_venta']
        impuesto_igv = data['impuesto_igv']
        total_venta = data['total_venta']
        monto_detraccion = data['monto_detraccion']
        db[table][id] = dict(numero_comprobante=numero_comprobante,
                             monto_detraccion=monto_detraccion,
                             sub_total_venta=sub_total_venta,
                             impuesto_igv=impuesto_igv,
                             total_venta=total_venta,
                             tipo_comprobante=tipo_comprobante
                             )
    except:
        return
    return


def data_update_facturacion(identifier, values, table):
    try:
        ticket_facturacion = values['numero_ticket']
    except:
        return
    data = db.facturacion(db.facturacion.numero_ticket == ticket_facturacion)
    tipo_comprobante = data['tipo_comprobante']
    numero_comprobante = data['id']
    sub_total_venta = data['sub_total_venta']
    impuesto_igv = data['impuesto_igv']
    total_venta = data['total_venta']
    monto_detraccion = data['monto_detraccion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(numero_comprobante=numero_comprobante,
                                   monto_detraccion=monto_detraccion,
                                   sub_total_venta=sub_total_venta,
                                   impuesto_igv=impuesto_igv,
                                   total_venta=total_venta,
                                   tipo_comprobante=tipo_comprobante
                                   )
    except Exception, error:
        print error


#################################################

def data_insert_fact(values, id, table):
    try:
        ticket_facturacion = values['numero_ticket']
        data = db.facturacion(db.facturacion.numero_ticket == ticket_facturacion)
        monto_detraccion = data['monto_detraccion']
        db[table][id] = dict(monto_detraccion=monto_detraccion)
    except:
        return
    return


def data_update_fact(identifier, values, table):
    try:
        ticket_facturacion = values['numero_ticket']
    except:
        return
    data = db.facturacion(db.facturacion.numero_ticket == ticket_facturacion)
    monto_detraccion = data['monto_detraccion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(monto_detraccion=monto_detraccion)
    except:
        pass


#################################################

def data_insert_cliente(values, id, table):
    try:
        ticket = values['numero_ticket']
        ticket_data = db.tickets(db.tickets.id == ticket)
        moneda = ticket_data['moneda']
        db[table][id] = dict(moneda=moneda)
    except:
        return
    return


def data_update_cliente(identifier, values, table):
    try:
        ticket = values['numero_ticket']
    except:
        return
    ticket_data = db.tickets(db.tickets.id == ticket)
    cliente = ticket_data['cliente']
    marca_gg = ticket_data['marca_gg']
    razon_social_gg = ticket_data['razon_social_gg']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(cliente=cliente, marca_gg=marca_gg, razon_social_gg=razon_social_gg)
    except:
        pass


#################################################

def data_insert_cuentas_x_pagar(values, id, table):
    try:
        ticket = values['numero_ticket_gasto']
        data = db.gastos_generales(db.gastos_generales.id == ticket)
        marca_gg = data['marca_gg']
        razon_social_gg = data['razon_social_gg']
        monto_detraccion = data['monto_detraccion']
        db[table][id] = dict(marca_gg=marca_gg, razon_social_gg=razon_social_gg, monto_detraccion=monto_detraccion)
    except:
        return
    return


def data_update_cuentas_x_pagar(identifier, values, table):
    try:
        ticket = values['numero_ticket_gasto']
    except:
        return
    data = db.gastos_generales(db.gastos_generales.id == ticket)
    marca_gg = data['marca_gg']
    razon_social_gg = data['razon_social_gg']
    monto_detraccion = data['monto_detraccion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(marca_gg=marca_gg, razon_social_gg=razon_social_gg,
                                   monto_detraccion=monto_detraccion)
    except:
        pass


#################################################
# MONEDA (SE GENERA AUTOMATICAMENTE SELECCIONANDO NUMERO DE TICKET)
# RAZON SOCIAL NOMBRE CLIENTE (SE GENERA AUTOMATICAM. SELECCIONANDO NUMERO DE TICKET)
# RUC /DNI/CE CLIENTE (SE GENERA AUTOMATICAMENTE SELECCIONANDO NUMERO DE TICKET)
# DIRECCION FISCAL CLIENTE (SE GENERA AUTOMATICAMENTE SELECCIONANDO NUMERO DE TICKET)
def data_insert_cli(values, id, table):
    try:
        ticket = values['numero_ticket']
        ticket_data = db.tickets(db.tickets.id == ticket)
        cliente = ticket_data['cliente']
        marca_gg = ticket_data['marca_gg']
        razon_social_gg = ticket_data['razon_social_gg']
        db[table][id] = dict(cliente=cliente, marca_gg=marca_gg, razon_social_gg=razon_social_gg)
    except:
        return
    return


def data_update_cli(identifier, values, table):
    try:
        ticket = values['numero_ticket']
    except:
        return
    ticket_data = db.tickets(db.tickets.id == ticket)
    cliente = ticket_data['cliente']
    marca_gg = ticket_data['marca_gg']
    razon_social_gg = ticket_data['razon_social_gg']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(cliente=cliente, marca_gg=marca_gg, razon_social_gg=razon_social_gg)
    except:
        pass


def data_insert_values(values, id, table):
    try:
        ticket = values['numero_ticket']
        ticket_data = db.tickets(db.tickets.id == ticket)
        marca = ticket_data['marca_gg']
        cliente = ticket_data['cliente']
        referencia = ticket_data['referencia']
        data_cotizacion = db.cotizaciones(db.cotizaciones.numero_ticket == ticket)
        cotizacion = data_cotizacion['id']
        db[table][id] = dict(marca=marca, cotizacion=cotizacion, cliente=cliente, referencia=referencia)
    except Exception, error:
        print error
        pass
    return


def data_update_values(identifier, values, table):
    try:
        ticket = values['numero_ticket']
    except:
        return
    ticket_data = db.tickets(db.tickets.id == ticket)
    marca = ticket_data['marca_gg']
    cliente = ticket_data['cliente']
    referencia = ticket_data['referencia']
    data_cotizacion = db.cotizaciones(db.cotizaciones.numero_ticket == ticket)
    cotizacion = data_cotizacion['id']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = db[table][id] = dict(marca=marca, cotizacion=cotizacion,
                                                   cliente=cliente, referencia=referencia)
    except:
        pass

def data_insert_values_plus(values, id, table):
    try:
        ticket = values['numero_ticket']
        ticket_data = db.tickets(db.tickets.id == ticket)
        marca = ticket_data['marca_gg']
        data_cotizacion = db.cotizaciones(db.cotizaciones.numero_ticket == ticket)
        cotizacion = data_cotizacion['id']
        data_proyecto = db.proyectos(db.proyectos.numero_ticket == ticket)
        proyecto = data_proyecto['id']
        db[table][id] = dict(marca=marca, cotizacion=cotizacion, proyecto=proyecto)
    except:
        pass
    return


def data_update_values_plus(identifier, values, table):
    try:
        ticket = values['numero_ticket']
    except:
        return
    ticket_data = db.tickets(db.tickets.id == ticket)
    marca = ticket_data['marca_gg']
    data_cotizacion = db.cotizaciones(db.cotizaciones.numero_ticket == ticket)
    cotizacion = data_cotizacion['id']
    data_proyecto = db.proyectos(db.proyectos.numero_ticket == ticket)
    proyecto = data_proyecto['id']
    table_id = identifier.select().first().id
    try:
        db[table][id] = dict(marca=marca, cotizacion=cotizacion, proyecto=proyecto)
    except:
        pass

#################################################
def data_insert_requerimientos(values, id, table):
    try:
        cliente = values['cliente']
        data = db.clientes(db.clientes.id == cliente)
        area_negocio = data['area_negocio']
        db[table][id] = dict(
            area_negocio=area_negocio
        )
    except:
        pass
    return


def data_update_requerimientos(identifier, values, table):
    try:
        cliente = values['cliente']
    except:
        return
    data = db.clientes(db.clientes.id == cliente)
    area_negocio = data['area_negocio']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            area_negocio=area_negocio
        )
    except:
        pass

#################################################
def insert_fecha_ticket(values, id, table):
        try:
            ticket = values['numero_ticket']
            ticket_data = db.tickets(db.tickets.id == ticket)
            fecha_registro_ticket = ticket_data['creacion']
            db[table][id] = dict(
                fecha_registro_ticket=fecha_registro_ticket
            )
        except:
            pass
        return


def update_fecha_ticket(identifier, values, table):
    try:
        ticket = values['numero_ticket']
    except:
        return
    ticket_data = db.tickets(db.tickets.id == ticket)
    fecha_registro_ticket = ticket_data['creacion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            fecha_registro_ticket=fecha_registro_ticket
        )
    except:
        pass

#################################################
# def insert_direccion_cliente(values, id, table):
#         try:
#             ticket = values['cliente']
#             cliente_data = db.clientes_direcciones(db.clientes_direcciones.id == cliente)
#             cliente_distrito = cliente_data['distrito']
#             db[table][id] = dict(
#                 cliente_distrito=cliente_distrito
#             )
#         except:
#             pass
#         return
#
#
# def update_direccion_cliente(identifier, values, table):
#     try:
#         ticket = values['numero_ticket']
#     except:
#         return
#     ticket_data = db.tickets(db.tickets.id == ticket)
#     fecha_registro_ticket = ticket_data['creacion']
#     table_id = identifier.select().first().id
#     try:
#         db[table][table_id] = dict(
#             fecha_registro_ticket=fecha_registro_ticket
#         )
#     except:
#         pass

################################################
def insert_fecha_cotizacion(values, id, table):
        try:
            ticket = values['ticket']
            cotizacion_data = db.cotizaciones(db.cotizaciones.id == ticket)
            fecha_cotizacion = cotizacion_data['fecha_registro_cotizacion']
            db[table][id] = dict(
                fecha_cotizacion=fecha_cotizacion
            )
        except:
            pass
        return


def update_fecha_cotizacion(identifier, values, table):
    try:
        ticket = values['ticket']
    except:
        return
    cotizacion_data = db.cotizaciones(db.cotizaciones.id == ticket)
    fecha_cotizacion = cotizacion_data['fecha_registro_cotizacion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            fecha_cotizacion=fecha_cotizacion
        )
    except:
        pass


################################################
def insert_fecha_visitado(values, id, table):
        try:
            numero_ticket = values['numero_ticket']
            #cliente= values['cliente']
            data = db.visitas(db.visitas.numero_ticket == numero_ticket)
            fecha_visitado = data['fecha_visita']
            db[table][id] = dict(
                fecha_visitado=fecha_visitado
            )
            print data
        except:
            pass
        return


def update_fecha_visitado(identifier, values, table):
    try:
        ticket = values['ticket']
    except:
        return
    cotizacion_data = db.cotizaciones(db.cotizaciones.id == ticket)
    fecha_cotizacion = cotizacion_data['fecha_registro_cotizacion']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            fecha_cotizacion=fecha_cotizacion
        )
    except:
        pass

################################################
def insert_fecha_envio_cotizacion(values, id, table):
        try:
            numero_ticket = values['numero_ticket']
            visita_data = db.cotizaciones(db.cotizaciones.id == numero_ticket)
            fecha_cotizacion = visita_data['fecha_cotizacion']
            db[table][id] = dict(
                fecha_cotizacion=fecha_cotizacion
            )
        except:
            pass
        return


def update_fecha_envio_cotizacion(identifier, values, table):
    try:
        numero_ticket = values['numero_ticket']
        visita_data = db.cotizaciones(db.cotizaciones.id == numero_ticket)
        fecha_cotizacion = visita_data['fecha_cotizacion']
        table_id = identifier.select().first().id
        db[table][table_id] = dict(
            fecha_cotizacion=fecha_cotizacion
        )
    except:
        return


################################################
def insert_operaciones_distrito(values, id, table):
        try:
            numero_ticket = values['numero_ticket']
            data = db.tickets(db.tickets.id == numero_ticket)
            distrito = data['distrito']
            db[table][id] = dict(
                distrito=distrito
            )
        except:
            pass
        return


def update_operaciones_distrito(identifier, values, table):
    try:
        numero_ticket = values['numero_ticket']
    except:
        return
    data = db.tickets(db.tickets.id == numero_ticket)
    distrito = data['distrito']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            distrito=distrito
        )
    except:
        pass

################################################
def insert_referencia_visita(values, id, table):
        try:
            cliente = values['cliente']
            data = db.clientes_direcciones(db.clientes_direcciones.id == cliente)
            referencia_visita = data['referencia']
            db[table][id] = dict(
                referencia_visita=referencia_visita
            )
        except:
            pass
        return


def update_referencia_visita(identifier, values, table):
    try:
        cliente = values['cliente']
    except:
        return
    data = db.clientes_direcciones(db.clientes_direcciones.id == cliente)
    referencia_visita = data['referencia']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(referencia_visita=referencia_visita)
    except:
        pass

################################################
def insert_data_pagar(values, id, table):
        try:
            ticket = values['numero_ticket_gasto']
            data = db.gastos_generales(db.gastos_generales.id == ticket)
            personal_por_pagar = data['personal_gasto']
            proveedor_por_pagar = data['proveedor_gasto']
            db[table][id] = dict(
                personal_por_pagar=personal_por_pagar,proveedor_por_pagar=proveedor_por_pagar
            )
        except:
            pass
        return


def update_data_pagar(identifier, values, table):
    try:
        cliente = values['cliente']
    except:
        return
    data = db.clientes_direcciones(db.clientes_direcciones.id == cliente)
    referencia_visita = data['referencia']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            referencia_visita=referencia_visita
        )
    except:
        pass

################################################
def insert_personal_visitado(values, id, table):
        try:
            numero_ticket = values['numero_ticket']
            data = db.visitas(db.visitas.numero_ticket == numero_ticket)
            personal_visitado = data['personal_visita']
            db[table][id] = dict(personal_visitado=personal_visitado)
        except:
            pass
        return
################################################
def insert_visitas_distrito(values, id, table):
        try:
            numero_ticket = values['numero_ticket']
            data = db.tickets(db.tickets.id == numero_ticket)
            distrito = data['distrito']
            db[table][id] = dict(
                distrito=distrito
            )
        except:
            pass
        return


def update_visitas_distrito(identifier, values, table):
    try:
        numero_ticket = values['numero_ticket']
    except:
        return
    data = db.tickets(db.tickets.id == numero_ticket)
    distrito = data['distrito']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            distrito=distrito
        )
    except:
        pass
################################################
def insert_visitas_referencia(values, id, table):
        try:
            numero_ticket = values['numero_ticket']
            data = db.tickets(db.tickets.id == numero_ticket)
            distrito = data['distrito']
            db[table][id] = dict(
                distrito=distrito
            )
        except:
            pass
        return


def update_visitas_referencia(identifier, values, table):
    try:
        numero_ticket = values['numero_ticket']
    except:
        return
    data = db.tickets(db.tickets.id == numero_ticket)
    distrito = data['distrito']
    table_id = identifier.select().first().id
    try:
        db[table][table_id] = dict(
            distrito=distrito
        )
    except:
        pass